/*

BubbleBeam is a pop up toolbar consisting of round buttons for high-level
operations, that can be displayed in any of the four corners of the screen, and
can disappear when the mouse stops moving

*/
(function () {

    var _uuid = 100000;
    var get_uuid = function () { return _uuid++; }

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["core/ui/html/" + name]);
    };


    var bubblebeam = function (opts) {
        var tmp = get_template('bubblebeam');
        var buttons = {};
        for (var i in opts.buttons) {
            var b = opts.buttons[i];
            b.name = 'button' + get_uuid();
            buttons[b.name] = b;
        };
        //console.log("THIS IS BUBBLEBEAM" + JSON.stringify(opts, null, " "));

        var exp = '[data-bubblebeam='+opts.name+']';
        $(exp).remove(); // remove existing

        var ctx = {
            name: opts.name,
            buttons: buttons,
        };

        $('body').append(tmp(ctx));

        if (opts.placement !== 'bottom-left' &&
                opts.placement !== 'bottom-right') {
            throw "Invalid placement";
        }

        // Now set up
        $(exp).each(function () {
            var $e = $(this);
            $e.find(".fixed-action-btn").addClass(opts.placement);
            $e.find('.tooltipped').tooltip({delay: 50});
            $e.find('[data-bubblebeamclick]').each(function () {
                var $this = $(this);
                var button = buttons[$this.attr("data-bubblebeamclick")];
                $this.click(function () {
                    button.action.apply($this, button.args || []);
                    $this.attr('disabled', 'disabled');
                });
            });
        });

        return $(exp);
    };

    var dock_left  = [];
    var dock_right = [];
    var paneldock = {};

    paneldock.register = function (opts) {
        var dock_list = opts.placement === "right" ? dock_right : dock_left;
        dock_list.push(opts);
        return dock_list;
    };

    var prep_button = function (dock, opts) {
        dock._is_visible = false;
        var show_docklist = function () {
            dock.$obj.show();
            dock._is_visible = true;
        };
        return {
            icon_class: 'mdi-action-receipt',
            tooltip: 'Prep buttons',
            action: function () {
                dock._is_visible = !dock._is_visible;
                if (dock._is_visible) {
                    opts.show(show_docklist);
                    dock.$obj.hide();
                } else {
                    dock.$obj.show();
                    opts.hide();
                }
            },
        };
    };

    paneldock.start = function (left_only) {
        dock_left.$obj = bubblebeam({
            placement: 'bottom-left',
            name: 'left_menu',
            buttons: dock_left.map(function (opts) {
                return prep_button(dock_left, opts);
            }),
        });

        if (!left_only) {
            dock_right.$obj = bubblebeam({
                placement: 'bottom-right',
                name: 'right_menu',
                buttons: dock_left.map(function (opts) {
                    return prep_button(dock_right, opts);
                }),
            });
        }
    };

    LE2.provide('bubblebeam', function () { return bubblebeam; });
    LE2.provide('paneldock', function () { return paneldock; });
})();

