/*
Panel allows panels to attach themselves brought up by a bubblebeam
*/
(function () {

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["core/ui/html/" + name]);
    };

    var setup_panel_old = function (opts) {
        // First, set up an appropriate bubblebeam
        var template = get_template('panel');

        var defaults = {
            buttons: false,
            content: false,
            tree: false,
            bottom_content: false,
        };

        var ctx = $.extend(defaults, opts);

        var $elem = $(template(ctx));
        $('body').append($elem);

        $elem.find('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'left', // Choose the horizontal origin
            closeOnClick: false // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });

        var hide = function () {
            $elem.find('.button-collapse').sideNav('hide');
            $elem.hide();
        };

        var show = function () {
            $elem.find('.button-collapse').sideNav('show');
            $elem.show();
        };
        hide();

        $elem.find('.collapse').collapsible();

        var paneldock = LE2.require('paneldock');
        paneldock.register({
            side: opts.placement,
            name: opts.name,
            tooltip: opts.description,
            hide: hide,
            show: show,
        });
    };

    var get_opposite = function (val) {
        if (val === 'left') { return 'right'; }
        if (val === 'right') { return 'left'; }
        return null; // err
    };

    var set_column = function ($div, new_column) {
        $div.removeClass('s12 s11 s10 s9 s8 s7 s6 s5 s4 s3 s2 s1')
            .addClass('col')
            .addClass('s' + new_column);
    };

    var setup_panel_tiled = function (opts) {
        // Custom, inline panel
        var template = get_template('panel');

        var $master_wrapper = $('#master_wrapper');
        var $center_panel = $('#center_panel');
        var $panel_wrapper = $('#' + opts.side + '_panel');
        var $other_panel_wrapper = $('#' + get_opposite(opts.side) + '_panel');

        var defaults = {
            buttons: false,
            content: false,
            tree: false,
            bottom_content: false,
            setup: function () {},
        };

        var ctx = $.extend(defaults, opts);

        var $panel = $(template(ctx));
        $panel_wrapper.append($panel);

        var show_docklist = null;

        var hide = function () {
            $panel_wrapper.hide();
            $panel_wrapper.removeClass('panel-visible');
            if ($other_panel_wrapper.hasClass('panel-visible')) {
                set_column($center_panel, 9);
            } else {
                set_column($center_panel, 12);
            }
            $panel.hide();

            if (show_docklist) { show_docklist(); }
        };

        var show = function (to_show) {
            show_docklist = to_show;

            // Check if other panel is visible
            if ($other_panel_wrapper.hasClass('panel-visible')) {
                set_column($center_panel, 6);
            } else {
                set_column($center_panel, 9);
            }

            // make sure all other panels are collapsed
            $panel_wrapper.find('.uicore-panel').hide();
            set_column($panel_wrapper, 3);
            $panel_wrapper.show();
            $panel_wrapper.addClass('panel-visible');

            // Now make sure our panel is visible
            $panel.show();
        };
        hide();
        $panel.panel_hide = hide;
        $panel.panel_show = show;

        var paneldock = LE2.require('paneldock');
        paneldock.register({
            side: opts.placement,
            name: opts.name,
            tooltip: opts.description,
            hide: hide,
            show: show,
        });
        LE2.require('paneldock').start();
        $panel.find('.uic-close-panel').click(hide);

        setup_pushpin();
        return $panel;
    };

    var setup_pushpin = function () {
        var $left_panel = $('#left_panel');
        var $right_panel = $('#right_panel');
        $left_panel.pushpin();
        $right_panel.pushpin();
        $left_panel.css('left', 0);
        $right_panel.css('right', 0);
    };

    var setup = function () {
    };


    LE2.subscribe('setup', setup);
    LE2.provide('panel', function () { return setup_panel_tiled; });
})()

