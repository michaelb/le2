
/*

Panel allows panels to attach themselves brought up by a bubblebeam

*/
(function () {

    var _uuid = 100000;
    var get_uuid = function () { return _uuid++; }

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["core/ui/html/" + name]);
    };

    var setup_modal_as_panel = function (opts) {
        var template = get_template("navcontent");
        var modal_template = get_template("modal");

        var defaults = {
            buttons: false,
            content: false,
            tree: false,
            bottom_content: false,
        };

        var ctx = $.extend(defaults, opts);
        var modal_content = template(ctx);

        var modal_ctx = { title: opts.title, content: modal_content };

        var $elem = $(modal_template(modal_ctx));
        $('body').append($elem);
        $elem.openModal();

    };

    LE2.provide('modalpanel', function () { return setup_modal_as_panel; });
})()

