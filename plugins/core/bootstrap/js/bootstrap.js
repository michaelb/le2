window.LE2 = (function () {
    var document_head = document.head;
    var events = {};
    var html = {};

    //'html': '<template id="%(id)s">%(content)s</template>',


    /* *************** */
    /* Plugin loading! */
    var get_file = function(path, callback) {
        function onload_event () {
            callback(this.responseText);
        }

        var oReq = new XMLHttpRequest();
        oReq.onload = onload_event;
        oReq.open("get", path, true);
        oReq.send();
    };

    var tag_types = {
        css: function (info, callback) {
            var link = document.createElement('link')
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = info.src;
            document_head.appendChild(link);
            callback();
        },
        js: function (info, callback) {
            var script = document.createElement('script')
            script.src = info.src;
            script.async = true;
            script.type = "text/javascript";
            script.onload = function () {
                callback();
            };
            document_head.appendChild(script);
        },
        html: function (info, callback) {
            var path = info.src.split(".")[0];
            var splitted = path.split("/");
            var src = splitted.slice(splitted.length - 4).join("/");
            get_file(info.src, function (data) {
                html[src] = data;
                callback();
            });
        },
    };

    var load_plugin = function (info, callback) {
        var tag_creator = tag_types[info.type];
        tag_creator(info, callback);
    };

    var load_file_infos = function (file_infos, plugin_name) {
        var loop = function (i) {
            if (i >= file_infos.length) { return plugins_loaded(plugin_name); }
            var info = file_infos[i];
            load_plugin(info, function () { loop(i+1); });
        };
        loop(0);
    };
    /* *************** */

    var plugins_loaded = function (plugin_name) {
        var ipc = require('ipc');

        // lol i have no idea what i am doing
        //window.require = 0; delete window.require; require = 0; delete require;
        //window.module = 0; delete window.module; module = 0; delete module;

        var bootstrap_func = bootstrap_funcs[plugin_name];

        if (!bootstrap_func) {
            console.error("Page is not bootstrapped! "+
                         "Something is very wrong: " + plugin_name);
        } else {
            bootstrap_func(ipc);
        }

        // Set up testing server if necessary
        require('lovelock').try_server(window);
    };

    var trigger_load = function (plugin_name) {
        var ipc = require('ipc');
        ipc.on('loadplugins_reply', function (data) {
            load_file_infos(data, plugin_name);
        });
        ipc.send('loadplugins', plugin_name);
    };

    var bootstrap_funcs = {};
    var register_bootstrap = function (name, launch) {
        bootstrap_funcs[name] = launch;
    };

    var qs = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
            var p=a[i].split('=', 2);
            if (p.length == 1) b[p[0]] = "";
            else b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'));

    return {
        html: html,
        trigger_load: trigger_load,
        register_bootstrap: register_bootstrap,
        instance_settings: qs,
    };
})();

/*!
 * once was based on XXSPubSub
 * Copyright (c) 2012 Denis Ciccale (@tdecs)
 * Released under MIT license (https://raw.github.com/dciccale/xxspubsub/master/LICENSE.txt)
 */
(function (root) {
    // subscribes hash
    var channels = {};

    root.publish = function () {
        var data = Array.prototype.slice.call(arguments);
        var channel = data.shift();

        var subscribes = channels[channel] || [],
                    l = subscribes.length;

        subscribes.forEach(function (sub) {
            sub.apply(root, data || []);
        });
    };

    root.subscribe = function (channel, handler) {
        (channels[channel] = channels[channel] || []).push(handler);
        return [channel, handler];
    };

    root.unsubscribe = function (handle) {
        var subscribes = channels[handle[0]],
            l = subscribes.length;
        while (l--) {
            if (subscribes[l] === handle[1]) {
                subscribes.splice(l, 1);
            }
        }
    };

    /* ***********************
     * Simple require system
     * *********************** */

    var provisions = {};
    var thises = {};
    var list_provs = function () {
        for (prov in provisions) {
            console.error("Provision: " + prov);
        }
    };

    //root.require = function () {
    root.require = function (channel) {
        //var data = Array.prototype.slice.call(arguments);
        //var channel = data.shift();
        var fnc = provisions[channel];
        if (!fnc) {
            //list_provs();
            throw "Provision not found: " + channel;
        }
        var this_ = thises[channel] || null;
        return fnc.apply(this_, []);
    };

    root.provide = function (channel, handler, this_, override) {
        if (provisions[channel] && !override) {
            throw "Attempt to override: " + channel;
        }
        //console.log("Providing -" + channel + '-');
        provisions[channel] =  handler;
        thises[channel] =  this_ || null;
    };

    root.unprovide = function (channel, dontfail) {
        if (dontfail || provisions[channel]) {
            delete provisions[channel];
            delete thises[channel];
        }
    };

})(window.LE2);

