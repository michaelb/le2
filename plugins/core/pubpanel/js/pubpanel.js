/*
   The publishing panel
*/
(function () {

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["core/pubpanel/html/" + name]);
    };

    var $panel = null;

    var setup = function (opts) {
        var defaults = {
        };

        var ctx = $.extend(defaults, opts);


        // Render HTML for panel
        var template = get_template('pubpanel');
        var btemplate = get_template('pubpanel_bottom');
        var content = template(ctx);
        var bcontent = btemplate(ctx);

        //console.log("rendered panel!!");

        var register_panel = LE2.require('panel');
        $panel = register_panel({
            content: content,
            bottom_content: bcontent,
            title: "Export",
            side: "right",
            name: "export",
            tooltip: "Publish your document to a variety of formats",
        });

        // activate any interactive stuff here
        $panel.find('.collapsible').collapsible();
    };

    LE2.subscribe('setup', setup);
})()
