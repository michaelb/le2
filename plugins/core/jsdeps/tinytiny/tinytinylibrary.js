/* * TinyTiny michaelb Copyleft 2013 - LGPL 3.0 * */

/* ********************* */
/* Filters               */
TinyTiny.filters.upper = function (s) { return s.toUpperCase(); };
TinyTiny.filters.lower = function (s) { return s.toLowerCase(); };
TinyTiny.filters.safe = function (s) { var b=new String(s); b.safe=true; return b; };
TinyTiny.filters.add = function (s, a) { return s + a; };
TinyTiny.filters.subtract = function (s, a) { return s - a; };
TinyTiny.filters['default'] = function (s, a) { return s || a; };
TinyTiny.filters.divisibleby = function (s, a) { return ((s*1) % (a*1)) === 0; }
TinyTiny.filters.escapejs = function (s) { return JSON.stringify(s); }
TinyTiny.filters.first = function (s) { return s[0]; }
TinyTiny.filters.last = function (s) { return s[s.length-1]; }
TinyTiny.filters.join = function (s, a) { return s.join(a); }
TinyTiny.filters.length = function (s) { return s.length; }
TinyTiny.filters.pluralize = function (s, a, b) { return a.split(',')[(s===1)*1]; }

/* ********************* */
/* if / else / elif      */

TinyTiny.tags['if'] = function (n, G, a, b, c, d) {
    var ops = ['==', '>', '<', '>=', '<=', '!=', 'not in', 'is not', 'is', 'in', 'not'];
    var ops_o = {
            '!=':'X !== Y',
            '==':'X === Y',
            'is': 'X === Y',
            'is not': 'X !== Y',
            'not': '!(Y)',
            'in': 'typeof Y[X] !== "undefined" || Y.indexOf && Y.indexOf(X) != -1',
        },
        re = RegExp(' ('+ops.join('|')+') ');
    ops_o['not in'] = '!('+ops_o['in']+')';
    a = n.split(re); // injection protection
    d = a.length > 1 ? (ops_o[a[1]] || ("X "+a[1]+" Y")) : 'X';
    d = d.replace(/([XY])/g, function (k, m) { return G.x(a[m==='X'?0:2]); });
    return {
        start: 'if ('+d+'){',
        end: '}'
    };
};

TinyTiny.tags['else'] = function (n, G) {
    return '} else {';
};

TinyTiny.tags.elif = function (n, G) {
    return '} else ' + TinyTiny.tags['if'](n, G).start;
};
/* ********************* */

/* ********************* */
/* For loops             */
TinyTiny.tags['for'] = function (n, G) {
    // Keeps unique arr ids to get over JS's quirky scoping
    var arr = 'arr'+G.s.length,
        split = n.split(' in '),
        vars = split[0].split(','),
        res = 'var '+arr+'='+G.x(split[1])+';';
    res += 'for (var key in '+arr+') {';
    if (vars.length > 1) {
        res += 'x.'+G.w(vars[0])+'=key;';
        vars = vars[1];
    }
    res += 'x.'+G.w(vars)+'='+arr+'[key];';

    return {
        start: res,
        end: '}'
    }
};

TinyTiny.tags.empty = function (n, G) {
    // make not_empty be based on nested-ness of tag stack
    var varname = 'G.forloop_not_empty' + G.s.length;
    var old_end = G.s.pop()[1]; // get rid of dangling for
    return {
        'start': varname+'=true;'
            + old_end + '\nif (!'+varname+') {',
        'end': '}'+varname+'=false;',
        'close': 'endfor'
    }
};
/* ********************* */

