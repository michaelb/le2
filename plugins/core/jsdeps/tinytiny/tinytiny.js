/* * TinyTiny michaelb Copyleft 2013 - LGPL 3.0 * */
(function () {
    var T, th, u, TinyTiny = function (s, o) {
        var G={l:[],s:[]},t=1,i=-1,nl,f,k,n;
        for (k in T) G[k] = (o && o[k]) || T[k];
        n = s.split(RegExp('('+G.mode_tokens.join('|(').replace(/ +/g, ')(.+?)')));
        nl=n.length;
        th = function (m) { throw "TinyTiny: "+m+":"+n[i]+"@"+i; }
        while (i++<nl) n[i] !== u && G.modes[(t=!t)?n[i++]:'text'](n[i], G);
        f = new Function('x,fx,l,G', G.l.join("")+"return l.join('');");
        tt_last_compiled = G.l.join("\n");
        return function (x) { return f(x, G.filters, [], G); }
    };
    T = TinyTiny;

    T.modes = {
        '{%': function (n, G, a, b) {
            a=G.trim(n).split(' ')[0];
            if (a===(G.s[G.s.length-1]||['!'])[0]) {
                G.l.push(G.s.pop()[1]);
            } else {
                b=(G.tags[a]||th("unknown "))(n.slice(a.length+1),G);
                G.l.push(b.start || b);
                b.end && G.s.push([b.close || ('end'+a),b.end]);
            }
        },
        '{#': function (n, G) { },
        '{{': function (n, G) { G.l.push('l.push(G.html('+G.x(n)+'));'); },
        text: function (n, G) {
            (n=G.e(n)) && G.l.push("l.push("+n+");");
        }
    };
    T.mode_tokens = [ '{% %}', '{{ }}', '{# #}' ];
    T.filters = {};
    T.tags = {};
    T.html = function (k) {
        return k.safe ? k : (k+"").replace(/&/g, '&amp;').replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    };
    T.t = T.trim = function (k) { return k.replace(/^\s+|\s+$/g, ""); }
    T.w = function (k) { return (k+'').replace(/[^a-zA-Z0-9$_\.]/g, ''); };
    T.x = T.expression = function (k, b, c, d, e) {
            b=k.split("|");
            d=T.v(b.shift());
            while(c=T.w((e=(b.shift()||'').split(':'))[0]))
                d='fx["'+c+'"]('+d+ (e[1]?','+T.v(e[1]):'')+')';
            return d;
        };
    T.e = T.esc = function (k) { return JSON.stringify(k); };
    T.v = T.value = function (k) {
            k = T.trim(k);
            return k.match(/^('.*'|".*")$/) ? T.e(k.substr(1, k.length-2)) : 
                k.match(/^\d+$/) ? k : 'x.'+T.w(k);
        };
    /*typeof module === 'undefined' ?
        window.TinyTiny = T :
        module.exports = T;*/
    window.TinyTiny = T; // @michaelb 2015
})()
