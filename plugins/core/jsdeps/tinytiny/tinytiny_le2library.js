TinyTiny.tags.cycle = function (n, G) {
    // keep track of cyclers
    if (!G.cycler) { G.cycler = 0; }
    // put cycler in context, since, why not
    var vn = 'x.cyclevar_'+G.cycler++,
        opts = G.trim(n).split(' ');
    return [vn, ' = (', vn, ' || 0) + 1;',
                'l.push([', opts.map(G.e).join(','), ']',
                '[', vn, ' % ', opts.length, ']);'].join('');
};


TinyTiny.tags.comment = function (n, G) {
    return { start: "/*", end: "*/", }
    return { start: "if (false) {", end: "}", }
};


TinyTiny.tags.make_guid = (function () {
    var _guid = 1000;
    var get_guid = function () { return _guid++; }
    return function (n, G) {
        return ["x." + (G.trim(n) || "guid") + " = '" + get_guid() + "';"];
    };
})();
