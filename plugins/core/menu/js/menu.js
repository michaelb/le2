(function () {
    var get_template = function (name) {
        return window.TinyTiny(LE2.html["core/menu/html/" + name]);
    };

    var actions = {
        opentemplate: function (ipc, template_name) {
            ipc.send('open', 'template:'+template_name);
        }
    };

    var hook = function (ipc, $e) {
        $e.find('.tooltipped').tooltip({delay: 50});

        $e.find('[data-action]').each(function () {
            var $this = $(this);
            var action = actions[$this.attr("data-action")];
            var args_str = $this.attr("data-args");
            var args = [ipc].concat(args_str.split(","));
            $this.click(function () {
                action.apply($this, args);
                $this.attr('disabled', 'disabled');
            });
        });

        /*
        LE2.require('bubblebeam')({
            placement: 'bottom-left',
            name: 'globalmenusettings',
            buttons: [
                {
                    icon_class: 'mdi-action-receipt',
                    tooltip: 'test',
                    action: function () {
                        'receipt was clicked!'
                    },
                },
            ],
        });
        */

        LE2.require('panel')({
            placement: 'left',
            name: 'settings',
            title: 'Settings',
            description: 'Settings for scroll editor',
            content: '<strong>test</strong>'
        });

        LE2.require('paneldock').start(true);
    };

    var main = function (ipc) {

        var templates = {
            menu: get_template('menu'),
        };

        var $menu = $('<div id="splashmenu"></div>').appendTo("body");
        ipc.on('menu_ready_reply', function (context) {
            $menu.html(templates.menu(context));
            hook(ipc, $menu);
            var $splash = $('body #splash');
            $splash.fadeOut('fast', function () { $splash.remove(); });
        });

        ipc.send('menu_ready');
    };

    LE2.register_bootstrap('menu', main);
})();


