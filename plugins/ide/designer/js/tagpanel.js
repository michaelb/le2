/*
   The tag editing panel
*/
(function () {

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["ide/designer/html/" + name]);
    };

    var $panel = null;

    var setup = function (opts) {
        var defaults = {
        };

        var ctx = $.extend(defaults, opts);


        // Render HTML for panel
        var template = get_template('tagpanel');
        /*var btemplate = get_template('pubpanel_bottom');*/
        var content = template(ctx);
        /*var bcontent = btemplate(ctx);*/

        //console.log("rendered panel!!");

        var register_panel = LE2.require('panel');
        $panel = register_panel({
            content: content,
            //bottom_content: bcontent,
            title: "Tag Creator",
            side: "left",
            name: "tag creator",
            tooltip: "Design new tags using CSS and HTML",
        });

        // activate any interactive stuff here
        $panel.find('.collapsible').collapsible();
    };

    LE2.subscribe('setup', setup);
})()
