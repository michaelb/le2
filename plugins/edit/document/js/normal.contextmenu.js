

(function () {

    var setup = function () {
    };

    var show_menu = function (editor, selection) {
        //var range  = selObj.getRangeAt(0);
        console.log("range count "+ selection.rangeCount);
        console.log("is collapsed "+ selection.isCollapsed);
        console.log("anchorNode "+ selection.anchorNode);
        console.log("anchorOffset "+ selection.anchorOffset);
        console.log("focusNode "+ selection.focusNode);
        console.log("focusOffset "+ selection.focusOffset);
        console.log("selObj "+ selection);
    };

    LE2.subscribe('normal_mode_rightclick', show_menu);
    LE2.subscribe('setup', setup);

})();

