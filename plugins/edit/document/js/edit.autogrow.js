(function($) {
    var on_change = function () {
        // Catch the current scroll position to stop it from jumping about in
        // some browsers
        var this_scroll = $(window).scrollTop();

        // Clear any existing height settings
        $(this).css('height', 'auto');

        // Set the textarea to scroll so that you can capture its height
        $(this).css('overflow', 'scroll');

        // Set the element height to the current scroll height
        $(this).height($(this).prop('scrollHeight'));

        // Hide the scrollbars
        $(this).css('overflow', 'hidden');

        // Re-apply the scroll position
        $(window).scrollTop(this_scroll);
    };

    $.fn.scrollautogrow = function () {
        $(this).on('change keyup keydown paste', on_change);

        // trigger right away
        on_change();
    };

})(jQuery);


(function () {
    var css_px = function ($e, property) {
        var str = $e.css(property);
        return parseInt(str.replace('px',''));
    };

    var font_size = function ($edit) {
        var font_height = Math.floor(css_px($edit, 'font-size') * 2.0);
        return font_height;
    };

    var setup_textarea = function ($edit) {
        console.log("AUTOGROWING!");
        // Add focus to the new element
        $edit.find('textarea , input').focus();
        $edit.css('overflow', 'hidden');
        var flicker_pad = $edit.scrollautogrow();
        //$edit.css('margin-bottom', '10');
        //$edit.css('margin-bottom', mb);
        //var flicker_pad = font_size($edit);
        //  Now de-flicker
        //var mb = css_px($edit, 'margin-bottom') - flicker_pad;
        //$edit.css('margin-bottom', mb);
        //console.log("MARGIN BOTTOM"+ mb);
    };

    LE2.subscribe('edit_autogrow', function ($textarea, opts) {
        setup_textarea($textarea);
    });

})();

