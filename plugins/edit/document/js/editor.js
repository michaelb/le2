(function () {
    var utils = null;

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["edit/document/html/" + name]);
    };

    var Editor = function (ipc, $editor) {
        this.ipc = ipc;
        this.click_buffer = {};
        this.most_recent_click = null;
        this.$editor = $editor;
    };

    Editor.prototype.start_to_edit_mode = function (element, split_info) {
        var uuid = utils.get_guid()
        var id = 'edit_mode_' + uuid;
        element.id = id;
        this.click_buffer[uuid] = id;
        this.most_recent_click = uuid;

        if (this.$editing) {
            // already in editing mode, switching directly
            var res = this.start_to_normal_mode();
            var html = split_info ? null : utils.outer_html($(element));
            this.ipc.send('normal', {
                    // normal mode data
                    data: res.data, cls: res.cls,

                    // edit mode data
                    switching: true,
                    html: html,
                    uuid: uuid,
                    id: id,
                    splitting: split_info || false,
                });
        } else {
            var html = utils.outer_html($(element));
            this.ipc.send('edit', {
                uuid: uuid,
                html: html
            });
        }
    };

    Editor.prototype.end_to_edit_mode = function (uuid, html, triggers) {
        // Get target
        var $target = $('#' + this.click_buffer[uuid]);

        ///////////////////////////////////
        // Clean up IDs, and clean up click buffer as needed
        $target.attr('id', '');
        delete this.click_buffer[uuid];
        if (uuid != this.most_recent_click) { return; }
        for (var other_uuid in this.click_buffer) {
            $('#' + this.click_buffer[other_uuid]).attr('id', '');
        }
        this.click_buffer = {}; // clear buffer
        this.most_recent_click = null;
        ///////////////////////////////////

        ///////////////////////////////////
        // Swap out textarea (or appropriate editing HTML)
        var $edit_html = $(html);
        utils.copy_style($target, $edit_html);
        $target.replaceWith($edit_html);

        this.$editing = $edit_html;
        // give focus to anything appropriate
        if (this.$editing.is('textarea,input')) {
            this.$editing.focus();
        } else {
            this.$editing.find('textarea,input').first().focus();
        }
        ///////////////////////////////////

        ///////////////////////////////////
        // Send appropriate hooks for plugins
        LE2.publish("editing", $edit_html);
        LE2.publish("edit_mode"); // same, but triggered after without data
        for (var i in triggers) {
            var info = triggers[i];
            info.opts = info.opts || {};
            info.opts.editor = this;
            LE2.publish("edit_" + info.name, $edit_html, info.opts);
        };
        ///////////////////////////////////
    };


    Editor.prototype.start_to_normal_mode = function () {
        var data = this.$editing.val();
        var cls = this.$editing.attr('class');
        var chunk = this.$editing.attr('chunk');
        this.$editing.attr('disabled', 'disabled');
        return {
            data: data,
            cls: cls,
            chunk: chunk,
        };
    };

    Editor.prototype.end_to_normal_mode = function (opts) {
        if (this.$editing === null) {
            console.error("Attempted to go twice into normal mode!");
            return;
        }

        this.$editing.replaceWith(opts.html);
        this.$editing = null;

        LE2.publish('normal_mode', this, opts);

        if (opts.edit) {
            // Go directly into edit mode
            var uuid = opts.edit.uuid;
            var html = opts.edit.html;
            var triggers = opts.edit.triggers;
            this.end_to_edit_mode(uuid, html, triggers);
        }
    };

    Editor.prototype.load_viewframe = function (viewframe) {
        var html = viewframe.data;
        var css_info = viewframe.css;
        this.default_element = viewframe.default_element;
        this.$editor.html("\n");
        this.$editor.append(html);
        LE2.publish('loaded', this);
    };

    Editor.prototype.is_empty = function () {
        // Checks if the editor is empty
        return this.$editor[0].childNodes.length < 2; // there is typically a text node
    };

    Editor.prototype.edit_default = function (initial_text) {
        if (this.most_recent_click !== null) { return };
        var $default_para = $(this.default_element).attr('data', initial_text || '');
        this.$editor.append($default_para);
        this.start_to_edit_mode($default_para[0]);
    };

    Editor.prototype.setup = function ($editor) {
        var ipc = this.ipc;
        var editor = this;

        this.ipc.on('edit_reply', function (arg) {
            console.log("Received edit reply");
            editor.end_to_edit_mode(arg.uuid, arg.html, arg.triggers);
        });

        this.$editor.on('click', 'bk', function (event) {
            // Trickle up to bk
            var $element = $(event.target);
            if (!$element.is('bk')) {
                $element = $element.parents('bk');
            }

            // Alright, $element is the correct element to go into edit mode,
            // and now we'll fetch where exactly we clicked, as best we can
            var selection = window.getSelection();
            utils.insert_selection_caret(selection);

            editor.start_to_edit_mode($element[0]);
        });

        this.ipc.on('normal_reply', function (arg) {
            console.log("Received normal reply");
            editor.end_to_normal_mode(arg);
        });

        $('body').on('contextmenu', function (ev) {
            ev.preventDefault();
            var selection = window.getSelection();
            ipc.send('context_menu', {
                visual: !(selection.isCollapsed),
                edit:   !!(editor.$editing),
                normal: !(editor.$editing),
            })
        });

        // Main "click" event
        $('body').on('click', function (event) {
            var $target = $(event.target);

            if ($target.is('.container')) {
                var cursor_y = event.pageY;
                var $last = editor.$editor; // see if below editor
                var bottom_y = $last.offset().top + $last.outerHeight();
                //console.log("CLICK ON CONTROLLER" + cursor_y  + '-' + bottom_y);
                if (cursor_y > bottom_y) {
                    // is at bottom!
                    return editor.edit_default();
                }
            }

            if (!editor.$editing) { return; }
            if ($target.is('textarea')) { return; }
            if ($target.is(editor.$editing)) { return; }
            if ($target.attr('data-editor-keepmode') === 'keep') { return; }

            // ignore clicks on other editable elements
            if ($target.is('bk') || $target.parents('bk').length > 0) {
                return;
            }

            /*
            if ($target.is(editor.$bottom_click_catcher)) {
                // dont think this ever happens, but worth a shot
                return;
            } 
            */

            // Nope, use as a "defocus" event, get into normal mode
            var res = editor.start_to_normal_mode();
            ipc.send('normal', {data: res.data, cls: res.cls});
        });

        LE2.publish('setup', editor);

        this.ipc.on('editor_ready_reply', function (viewframe) {
            editor.load_viewframe(viewframe);
            var $splash = $('body #splash');
            $splash.fadeOut('fast', function () { $splash.remove(); });
        });

        this.ipc.send('editor_ready');
    };

    var main = function (ipc) {
        utils = LE2.require('utils');

        var editor_template = get_template('editor');
        var ctx = {
            editor_id: "le2d",
        };

        var editor_html = editor_template(ctx);
        $('body').prepend(editor_html);
        var $editor = $('body #le2d');
        var editor = new Editor(ipc, $editor);
        editor.setup();
    };

    LE2.register_bootstrap('editor', main);
})();


