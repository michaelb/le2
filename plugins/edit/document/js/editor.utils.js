
(function () {
    var utils = {};

    var _uuid = 100000;
    utils.get_guid = function () { return _uuid++; }
    utils.outer_html = function ($e) {
        return $('<div>').append($e.clone()).html();
    };

    utils.copy_style = function ($elem, $edit) {
        var c = function (v) { $edit.css(v, $elem.css(v)); };
        var e = function (v) {
                c(v+'-top');
                c(v+'-bottom');
                c(v+'-left');
                c(v+'-right');
        }

        c('font-size'); c('letter-spacing'); c('text-align');
        c('line-height'); c('text-indent'); c('font-family');
        c('font-weight');

        e('margin');
        e('padding');
    };

    utils.get_offset_info = function ($elem, selection, giveup) {
        /*
         * Gives info to enable more precise placement of editing caret
         * Ultimately, it's guesswork, as there is not necessarily a 1 <--> 1
         * correspondance between the markdown and the eidtor HTML
         * representation. However, the  hope is it will make an "educated
         * guess" that will be at least as good as the users for 99% of the
         * cases.
         */

        // TODO: recurse up, through parent elements, from
        // selection.anchor.parent, adding together all offsets

        var count = selection.anchorOffset * 1;

        if ($elem[0] === selection.anchor) {
            // At top level, very common case, e.g no inline values
            return {
                count: selection.anchorOffset * 1,
                word: null,
                word_offset: 0,
            };

        } else {
            // recurse up, up to, say 10 levels, adding together offsets
            //var res = get_offset_info($elem, selection, giveup);
        }

        return null;
    };
    var ELEMENT_NODE = 1;
    var TEXT_NODE = 3;

    utils.insert_at_offset = function (node, node_offset, element_name) {
        var original_node = node;

        // Are we in a text node? (most likely case)
        if (node.nodeType !== TEXT_NODE) {
            return false; // no idea what to do with DOM, atm
        }

        var p_node = node.parentNode;

        // If so, split the node inserting the element

        // split between foo and bar
        var replacement_split = node.splitText(node_offset);

        // creating an empty node
        var caret = document.createElement(element_name);
        caret.appendChild(document.createTextNode(''));

        // adding the span before 'bar'
        p_node.insertBefore(caret, replacement_split);

        return caret;
    };

    utils.insert_selection_caret = function (selection) {
        // Inserts special selection carets into HTML text, so that when its
        // sent to the backend for parsing, we can get proper selection
        // instructions
        var a = null; b = null;
        if (selection.isCollapsed) {
            // simple click event
            a = utils.insert_at_offset(selection.anchorNode, selection.anchorOffset, "caret");
        } else {
            a = utils.insert_at_offset(selection.anchorNode, selection.anchorOffset, "startcaret");
            b = utils.insert_at_offset(selection.focusNode, selection.focusOffset, "endcaret");
        }
        return [a, b];
    };

    LE2.provide('utils', function () { return utils; });

})();
