(function () {
    // todo: do a similar toolbar cache thing to insertbar

    var linebar = null;
    var setup = function (editor) {
        linebar = LE2.require('linebar');
    };

    var $transform_toolbar = null;
    var $style_toolbar = null;

    var remove_toolbars = function () {
        if ($transform_toolbar !== null) { $transform_toolbar.remove(); }
        if ($style_toolbar !== null) { $style_toolbar.remove(); }
    };

    var show = function ($textarea) {
        remove_toolbars();

        /*
        $transform_toolbar = linebar({
            side: "left",
            buttons: [
                { label: "Section" },
                { label: "Paragraph" },
                { label: "Blockquote" }
            ],
        });
        */

        $style_toolbar = linebar({
            side: "right",
            buttons: [
                { label: "Section" },
                { label: "Paragraph" },
                { label: "Blockquote" }
            ],
        });
        $style_toolbar.position($textarea);
        $transform_toolbar.position($textarea);
    };

    var hide = function () {
        remove_toolbars();
    };

    ///////////////////
    // slight debounce to allow rapid clicking to work well
    var $target = null;
    var opts = null;
    var show_or_hide = _.debounce(function () {
        if ($target) { show($target); }
        else { hide(); }
    }, 100);

    LE2.subscribe('setup', setup);
    LE2.subscribe('normal_mode', function () {
        $target = null;
        opts = null;
        show_or_hide();
    });

    LE2.subscribe('edit_editbars', function ($textarea, opts) {
        $target = $textarea;
        opts = opts;
        show_or_hide();
    });
})();

