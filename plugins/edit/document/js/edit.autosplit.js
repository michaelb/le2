(function () {
    // TODO: make sure this works with windows...
    var LINE_RE = {
        1: new RegExp(/\n\r?/g),
        2: new RegExp(/\n\r?\s*\n\r?/g)
    };


    LE2.subscribe('edit_autosplit', function ($textarea, opts) {
        var line_re;
        if (opts.single_line) {
            line_re = LINE_RE[1];
        } else {
            line_re = LINE_RE[2];
        }
        //line_re = LINE_RE[1];

        /*
         * Triggers a "split" event, e.g. splitting while editing at a
         * certain place
         */
        $textarea.on('keyup', function () {
            var val = $textarea.val();
            if (val.match(line_re)) {
                //val = val.replace("\n", "\n\n");
                //$textarea.val(val);
                opts.editor.start_to_edit_mode({}, true);
            }
        });
    });
})();

