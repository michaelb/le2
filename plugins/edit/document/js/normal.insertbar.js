(function () {
    var enabled = false;
    var linebar = null;

    var $toolbar = null;
    var $target = null;
    var hover_toolbar = false;
    var just_hide = _.debounce(function () {
        if (hover_toolbar) {
            return;
        }

        if ($toolbar) {
            $toolbar.remove();
            $toolbar.off()
            $toolbar = null;
        }
    }, 100);

    var show_or_hide = _.debounce(function () {
        // todo: have the toolbar context rendered server side, check if the
        // $old_target is the same as the new for speed up


        if (!$target) {
            // should hide!
            just_hide();
            return;
        }

        // Should show
        if (!$toolbar) {
            // Need to regenerate
            $toolbar = linebar({
                // double check when the mouse leaves the toolbar
                //mouse_out: show_or_hide,

                // so we know if its hovering
                set_hover_state: function (v) { hover_toolbar = v; },

                // draw the line indicating insertion
                inserter_line: true,

                buttons: [
                    { label: "Section" },
                    { label: "Paragraph" },
                    { label: "Blockquote" }
                ]
            });
        }

        linebar.position($toolbar, $target);
    }, 100);

    var setup = function (editor) {
        linebar = LE2.require('linebar');
        enabled = true;

        editor.$editor.on('mouseover mouseout', 'bk', function (e) {
            if (!enabled) { return; } // in edit mode
            if (this !== e.target) { return; } // click on something within

            if (e.type === 'mouseover') {
                $target = $(e.target);
                show_or_hide();
            } else {
                $target = null;
                show_or_hide();
            }
        });
    };

    LE2.subscribe('setup', setup);

    LE2.subscribe('normal_mode', function () {
        $target = null;
        show_or_hide();
        enabled = true;
    });

    LE2.subscribe('edit_mode', function () {
        $target = null;
        show_or_hide();
        enabled = false;
    });
})();

