(function () {

    var is_typeable = function (e) {
        var key = parseInt(e.keyCode || e.charCode || 0);
        if (key >= 32 && key < 127 || key === 8 || key === 13 || key === 15) {
            return String.fromCharCode(key);
        }
        return false;
    };

    var ready_instance = function (editor) {
        /*
        $('body').on('keydown', function (e) {
            var ch = is_typeable(e);
            if (ch) {
                $(this).off(e); // disable
                editor.edit_default(ch);
            }
        });
        */
    };

    LE2.subscribe('loaded', function (editor) {
        if (editor.is_empty()) {
            ready_instance(editor);
        }
    });

})();
