(function () {

    /*
     * Sends a quick save request and entire document data
     */
    var prepared = false;
    var $icon = null;

    var get_template = function (name) {
        return window.TinyTiny(LE2.html["edit/document/html/" + name]);
    };

    var setup = function (editor) {
        $icon = $(get_template("quicksaveicon"));
        $('body').append($icon);
        editor.ipc.on('quicksave_reply', function () {
            $icon.find('.quicksaveicon-saving').hide();
            $icon.find('.quicksaveicon-saved').show();
            $icon.fadeOut('slow', function () {
                $icon.hide();
                setTimeout(function () { $icon.hide() }, 100);
            });
        });

    };

    var send_quicksave = function (editor, normal_mode_info) {
        if (!prepared) {
            prepared = true;
            setup(editor);
        };

        $icon.hide();
        $icon.find('.quicksaveicon-saving').show();
        $icon.find('.quicksaveicon-saved').hide();
        setTimeout(function () {
            $icon.show();
            $icon.fadeIn('slow');
        }, 10);

        /*
        var document_data;
        var chunk = normal_mode_info.chunk;

        // Just an idea how to implement chunks, if I decide I want to:
        // This way, we can even load all chunks at once, but then only save a few
        // Chunks could be something like 10-20 elements (breaks
        // apart at 20 into 2 10 elements, only splits, never
        // deletes a chunk)
        // Not sure if i want to have it only be in memory, e.g.
        // byte references for careful file alterations, or actual
        // files as before (downside, version control less obvious)
        if (chunk) {
            var $chunk = editor.$editor.find('chunk[chunkid='+chunk+']');
            document_data = $chunk.html();
        } else {
            document_data = editor.$editor.html();
        }
        */
        var document_data = editor.$editor.html();

        editor.ipc.send('quicksave', {
            //chunk: normal_mode_info.chunk,
            data: document_data,
        });
    };

    LE2.subscribe('normal_mode', send_quicksave);
})();

