(function () {
    // TODO:
    // How to finis this:
    // 1. In features/editor.js send over wrapping markdown as args
    //    (recall that it gets rendered as a context, verbatim)
    // 2. We'll then get that as an arg to "action", which
    //    (recall that it gets rendered as a context, verbatim)


    // todo: do a similar toolbar cache thing to insertbar
    var linebar = null;
    var setup = function (editor) {
        linebar = LE2.require('linebar');
    };

    var $toolbar = null;

    var remove_toolbars = function () {
        if ($toolbar !== null) { $toolbar.remove(); }
    };

    var show = function ($textarea, opts) {
        remove_toolbars();
        var defaults = {
            side: "left",
            //position: "side",
            position: "after",
            buttons: [
                { label: "Emphasis", data: "emph" },
                { label: "Footnote", data: "emph" },
                { label: "Endnote", data: "emph" },
            ],
            click: function (data) {
                alert(data);
            },
            search: true,
            search_data: [
                { label: "Strong", data: "str", searchstr: "strong,bold" },
                { label: "Mistake", data: "mis", searchstr: "mistake,linethrough,crossout,crossthrough" },
            ]
        };
        $toolbar = linebar(_.extend(defaults, opts));
        linebar.position($toolbar, $textarea);
    };

    var hide = function () {
        remove_toolbars();
    };

    ///////////////////
    // slight debounce to allow rapid clicking to work well
    var $target = null;
    var opts = null;
    var show_or_hide = _.debounce(function () {
        if ($target) { show($target, opts); }
        else { hide(); }
    }, 100);

    LE2.subscribe('setup', setup);
    LE2.subscribe('normal_mode', function () {
        $target = null;
        opts = null;
        show_or_hide();
    });

    LE2.subscribe('edit_stylebar', function ($textarea, opts) {
        $target = $textarea;
        opts = opts;
        show_or_hide();
    });
})();

