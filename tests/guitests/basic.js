var lovelock = require('lovelock');
var path     = require('path');

var DATA_PATH = path.resolve(path.join(__dirname, "testdata"));

process.on('uncaughtException', function (err) {
    console.error("uncaught exception");
    console.error(err);
    console.error(err.stack);
    console.trace();
});


exports.test_load = function (test) {
    // makes sure it loads the given path
    var workspace_path = path.join(DATA_PATH, "bareworkspace");
    lovelock.launch_electron({args: [workspace_path]}, function (window, teardown) {
        test.__teardown_server = teardown;
        test_load_text(test, window);
    });
};

var test_load_text = function (test, window) {
    var next = function () { test_done(test, window); };
    test.ok(window, "window exists");

    // get various properties about the paragraph
    var $para = new (window.$('#main_content').find('.base_para:first'));
    $para.text()._collect('text')._end();
    $para.height()._collect('height')._end();
    $para.width()._collect('width')._end();
    $para.offset()._collect('offset')._end();
    $para._get(function (results) {
        // make sure it loads
        test.equal("A paragraph some inline stuff", results.text);
        test.ok(results.height > 15, "taller than 15")
        test.ok(results.width > 100, "wider than 100");
        test.ok(results.offset.top > 100, "more than 100 from the top");
        test.ok(results.offset.left > 40, "more than 40 from the left");
        next();
    });
};

var test_done = function (test, window) {
    test.__teardown_server(function () {
        test.done();
    });
};

