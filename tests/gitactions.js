// Test high-level Git actions
var fs = require('fs');
var actions = require('../lib/data/actions');
var gitactions = require('../lib/data/gitactions');
var Config = require('../lib/data/config');
var path = require('path');
var os = require('os');
var _ = require('underscore');
_.str = require('underscore.string');
var fsutils = require('../lib/util/fsutils');

var helpers = require('./helpers/test_helpers');

exports.test_core_gitactions = function (test) {
    test.expect(21);
    helpers.with_empty_workspace(test, {}, function (config, workspace, teardown) {
        test_git_init(test, config, workspace, teardown);
    });
};


var test_git_init = function (test, config, workspace, teardown) {
    var next = function () { test_git_commit_and_log(test, config, workspace, teardown); }
    var opts = {silent: true};

    gitactions.gitinit(config, workspace, opts, function () {
        fs.exists(path.join(workspace.working_dir, ".git"), function (fexists) {
            test.ok(fexists, "file copied");
            next();
        });
    });
};

var INITIAL_CONTENT = "some initial content";

var test_git_commit_and_log = function (test, config, workspace, teardown) {
    var next = function () { test_another_commit(test, config, workspace, teardown); }
    var opts = {silent: true};

    // add some initial content
    var doc_path = path.join(workspace.working_dir, 'document.md');
    fs.writeFile(doc_path, INITIAL_CONTENT, function () {
        gitactions.gitstage(config, workspace, opts, function () {
            opts.message = "let's \"test\" together";
            gitactions.gitcommit(config, workspace, opts, function () {
                // Now lets check git log:
                gitactions.gitlog(config, workspace, 0, 20, opts, function (log) {
                    test.equal(log.length, 1);
                    test.equal(log[0].author, config.rc.user.name);
                    test.equal(log[0].message, opts.message);
                    test.equal(log[0].action, null);
                    next();
                });
            });
        });
    });
};

var test_another_commit = function (test, config, workspace, teardown) {
    var next = function () { test_undo_to_hash(test, config, workspace, teardown); }
    var opts = {silent: true};

    // Let's modify a file
    var doc_path = path.join(workspace.working_dir, 'document.md');
    fs.writeFile(doc_path, "some testing content\nanother line", function () {
        gitactions.gitcommitaction(config, workspace, "editted", "paragraph", opts, function () {
            // Now lets check git log:
            gitactions.gitlog(config, workspace, 0, 20, opts, function (log) {
                test.equal(log.length, 2);
                test.equal(log[0].author, config.rc.user.name);
                test.deepEqual(log[0].action, {
                        subject: config.rc.user.name,
                        verb: "editted",
                        object: "paragraph", });
                test.equal(log[1].action, null);
                next();
            });
        });

    });
};

var test_undo_to_hash = function (test, config, workspace, teardown) {
    var next = function () { test_commit_after_revert(test, config, workspace, teardown); }
    var opts = {silent: true};

    gitactions.gitundo(config, workspace, opts, function () {
        var doc_path = path.join(workspace.working_dir, 'document.md');
        fs.readFile(doc_path, function (err, data) {
            if (err) { throw "cant read" + err; }
            test.equal(INITIAL_CONTENT, data + '');
            next();
        });
    });
};

var test_commit_after_revert = function (test, config, workspace, teardown) {
    var next = function () { test_gitpull_nonbare(test, config, workspace, teardown); }

    // Let's modify a file again
    var doc_path = path.join(workspace.working_dir, 'document.md');
    var opts = {silent: true};
    //opts.log = console.log;
    fs.writeFile(doc_path, INITIAL_CONTENT+"\nchanges", function () {
        gitactions.gitcommitaction(config, workspace, "changed", "para", opts, function () {
            // Now lets check git log:
            gitactions.gitlog(config, workspace, 0, 20, opts, function (log) {
                test.equal(4, log.length, "Log length is correct");
                test.equal(log[0].author, config.rc.user.name);
                // The last commit action is as expected
                test.deepEqual(log[0].action, {
                        subject: config.rc.user.name,
                        verb: "changed",
                        object: "para", });

                // Now check the revert is correct
                var revert_action = log[1].action;
                test.ok(revert_action, "revert has action");
                test.ok(revert_action.object, "revert action has object");
                delete revert_action.object;

                test.equal(log[1].author, config.rc.user.name);
                test.deepEqual(revert_action, {
                        subject: config.rc.user.name,
                        verb: "reverted"});

                // Finally, make sure the file itself was reverted,
                // plus the change we made
                fs.readFile(doc_path, function (err, data) {
                    if (err) { throw "cant read" + err; }
                    test.equal(INITIAL_CONTENT+"\nchanges", data + '');
                    next();
                });
            });
        });
    });
};

var test_gitpull_nonbare = function (test, config, workspace, teardown) {
    var next = function () { test_gitsavescroll(test, config, workspace, teardown); }
    var uri = "file://" + workspace.working_dir;
    var opts = {silent: true};
    //opts.log = console.log;
    fs.readFile(path.join(workspace.working_dir, ".scrollid"), function (err, scrollid_data) {
        var correct_scrollid = _.str.trim(scrollid_data + '');
        gitactions.gitpull(config, uri, opts, function (new_dir, scrollid) {
            test.equal(correct_scrollid, scrollid);
            teardown();
            next();
        });
    });
};


var test_gitsavescroll  = function (test, config, workspace, teardown) {
    var next = function () { test_done(test, config, workspace, teardown); }
    var uri = "file://" + workspace.working_dir;
    var opts = {silent: true};
    //opts.log = console.log;

    var scroll_file_path = path.join(workspace.working_dir, "myfile.scroll");
    gitactions.gitsavescroll(config, workspace, scroll_file_path, opts, function () {
        fs.exists(scroll_file_path, function (fexists) {
            test.ok(fexists, "scroll file exsits after creation");
            fsutils.check_scroll_file(scroll_file_path, function (is_valid) {
                test.ok(is_valid, "scroll file has magic prefix");
                next();
            });
        });
    });
};





exports.test_scrollfile = function (test) {
    test.expect(17);
    helpers.with_scrollfile(test, "testfile_00.scroll", {}, function (config, workspace, teardown) {
        test_scrollfile_open(test, config, workspace, teardown);
    });
};


var test_scrollfile_open = function(test, config, workspace, teardown) {
    var opts = {silent: true};
    var next = function () { test_scrollfile_push(test, config, workspace, teardown); }

    // basic assertions
    test.ok(workspace.manifest, "workspace initialized");
    test.ok(workspace.manifest.meta.author, "manifest loaded 1");
    test.equal(workspace.manifest.document.length, 1, "manifest loaded 2");


    var doc_path = path.join(workspace.working_dir, 'document.md');

    // Now lets check git log:
    gitactions.gitlog(config, workspace, 0, 20, opts, function (log) {
        test.equal(4, log.length, "Log length is correct");
        test.equal(log[0].author, "michaelb");
        // The last commit action is as expected
        test.deepEqual(log[0].action, {
                subject: 'michaelb',
                verb: "changed",
                object: "para", });

        // Now check the revert action in the log is correct
        var revert_action = log[1].action;
        test.ok(revert_action, "revert has action");
        test.ok(revert_action.object, "revert action has object");
        delete revert_action.object;

        test.equal(log[1].author, "michaelb");
        test.deepEqual(revert_action, {
                subject: "michaelb",
                verb: "reverted"});

        // now check the doc path is expected
        fs.readFile(doc_path, function (err, data) {
            if (err) { throw "cant read" + err; }
            test.equal(INITIAL_CONTENT+"\nchanges", data + '');
            next();
        });
    });
};

var test_scrollfile_push = function(test, config, workspace, teardown) {
    var next = function () { test_done(test, config, workspace, teardown); }
    var opts = {silent: true};
    //opts.log = console.log;
    //opts.silent = false;

    // cp scrollfile to safe location
    var filepath = workspace.source_uri.slice("scrollfile://".length);
    var newpath = path.join(config._test_helper_tmpdir, "newscrollfile.scroll");
    var NEW_CONTENT = "some\n\ntest\n\ncontent";
    var MANIFEST_DATA = "\n\n[nonexistantheader]\nkey=val\n";

    var do_tests = function (config, workspace, teardown) {
        var finished = _.after(3, next);

        // check log
        gitactions.gitlog(config, workspace, 0, 20, opts, function (log) {
            test.equal(log.length, 5);
            test.equal(log[0].author, config.rc.user.name);
            test.deepEqual(log[0].action, {
                    subject: config.rc.user.name,
                    verb: "editted",
                    object: "paragraph", });
            finished();
        });

        // Check content for changes
        var doc_path = path.join(workspace.working_dir, 'document.md');
        fs.readFile(doc_path, function (err, data) {
            if (err) { throw "cant read" + err; }
            test.equal(NEW_CONTENT, data + '');
            finished();
        });

        // Check manifest data for appended stuff
        var manifest_path = path.join(workspace.working_dir, 'manifest.cfg');
        fs.readFile(manifest_path, function (err, data) {
            if (err) { throw "cant read" + err; }
            data = data + '';
            var end_data = data.slice(data.length-MANIFEST_DATA.length)
            test.equal(MANIFEST_DATA, end_data);
            finished();
        });
    };


    fsutils.copyfile(filepath, newpath, function (err) {
        if (err) { throw err; }

        // Make some new changes in our workspace
        var doc_path = path.join(workspace.working_dir, 'document.md');
        fs.writeFile(doc_path, NEW_CONTENT, function () {

            // test adding data to manifest
            workspace.append_to_manifest(MANIFEST_DATA, function () {
                gitactions.gitcommitaction(config, workspace, "editted", "paragraph", opts, function () {
                    // Push back to the scroll
                    gitactions.gitsavescroll(config, workspace, newpath, opts, function () {
                        fs.exists(newpath, function (fexists) {
                            test.ok(fexists, "scroll file exists after push");

                            // Kick off all tests with new config/workspace
                            helpers.with_scrollfile(test, newpath, opts, do_tests);
                        });
                    });

                });
            });
        });
    });

};

var test_done = function (test, config, workspace, teardown) {
    teardown();
    test.done();
};

