ROOT_PATH="$( cd "$BATS_TEST_DIRNAME" && cd .. && cd .. && pwd )"
scroll="$ROOT_PATH/bin/scroll --config $ROOT_PATH/data/conf/"

setup () {
    cd $(mktemp -d -t "scroll_test_XXXXX")
}

@test "help" {
    result=$($scroll help)
    [[ $result == *"stashlist"* ]]
    result=$($scroll help help 2>&1)
    [[ $result == *"Usage"* ]]
}

@test "new scroll render" {
    $scroll new base basic

    # Ensure a bunch of files exist
    [ -f .scrollid ]
    [ -f document.md ]
    [ -f manifest.cfg ]
    [ -d tags ]
    [ -d tags/base ]
    [ -f tags/base/para.cfg ]

    # Now try adding some text to document.md and make sure we can render  a
    # simple thing properly
    echo -e "Some paragraph text\n\nAnother paragraph" > document.md
    $scroll render < document.md > render_results
    echo -e "<p>Some paragraph text</p><p>Another paragraph</p>" > expected_results
    cmp --silent render_results expected_results
}

