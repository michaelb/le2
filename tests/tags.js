var _tag = require('../lib/markup/tag'),
    TagLoader = _tag.TagLoader,
    Tag       = _tag.Tag;
var customtypes = require('../lib/markup/schematypes');
var fs = require('fs');
var glob = require('glob');
var path = require('path');

process.on('uncaughtException', function(err) {
    console.error(err.stack);
});

var PATH_PREFIX = path.join(__dirname, "testdata", "tags");

var load_all = function (cb) {
    var tag_groups = [{
        "namespace": "testing",
        "paths": glob.sync(path.join(PATH_PREFIX, "*.cfg")),
    }];
    var tl = new TagLoader(tag_groups);
    tl.load(function () { cb(tl); });
};

exports.test_tagloader_basic = function (test) {
    load_all(function (tl) {
        // there are 5 test tags
        test.equal(tl.all_tags.length, 6);
        test.equal(tl.tags.testing.para.containment.tags.length, 3);
        test.equal(tl.tags.testing.section.containment.tags.length, 2);
        test.equal(tl.tags.testing.blockquote.containment.tags.length, 2);
        test.done();
    });
};

exports.test_tagloader_namespace = function (test) {
    var tag_groups = [
        { "namespace": "test_one",
            "paths": [path.join(PATH_PREFIX, "para.cfg")], },
        { "namespace": "test_two",
            "paths": [path.join(PATH_PREFIX, "emphasis.cfg")], },
    ];
    var tl = new TagLoader(tag_groups);
    tl.load(function () {
        // there are 5 test tags
        test.equal(tl.all_tags.length, 2);
        test.equal(tl.tags.test_one.para.containment.tags.length, 1);
        test.equal(tl.tags.test_two.emphasis.containment.tags.length, 1);
        test.done();
    });
};


exports.test_tagloader_render_css = function (test) {
    var normalize = function (s) { return s.split(".").sort().join("."); }
    load_all(function (tl) {
        var result = [
            '.testing_blockquote { display: block; background: gray;} ',
            '.testing_emphasis { text-variation: italic;} ',
            '.testing_para { display: block; padding: 3px;} ',
            '.testing_para > bk { display: inline;} ',
            '.testing_para html { display: none;} ',
            '.testing_section { display: block; font-size: 24pt;} ',
            '.testing_strong { display: block; font-weight: bold;} '].join('');
        test.deepEqual(normalize(result), normalize(tl.render_css()));
        test.done();
    });
};


exports.test_tagmatcher = function (test) {
    load_all(function (tl) {
        var match = function (str, taginfo) {
            return (new customtypes.TagMatcher(str)).match(taginfo);
        };
        // there are 5 test tags
        var para = tl.tags.testing.para;
        var section = tl.tags.testing.section;
        var blockquote = tl.tags.testing.blockquote;
        var emphasis = tl.tags.testing.emphasis;
        var strong = tl.tags.testing.strong;

        test.ok(match("para", para), "Tag name");
        test.ok(!match("para", section), "Tag name (negative)");
        test.ok(match("section para", para), "Tag name, multiple 1");
        test.ok(match("section para", section), "Tag name, multiple 2");
        test.ok(match("class:text", para), "Class name");
        test.ok(!match("class:text", emphasis), "Class name (negative)");
        test.ok(match("exact:testing_blockquote", blockquote), "Exact");
        test.ok(!match("exact:testing_blockquote", emphasis), "Exact (negative)");
        test.ok(match("namespace:testing", emphasis), "Namespace");
        test.ok(!match("namespace:lol", emphasis), "Namespace (negative)");

        // Test reuse:
        var tm = new customtypes.TagMatcher("namespace:nope class:simplestyle "+
                "exact:testing_section class:nope exact:asdf sclass:nope");
        test.ok(tm.match(emphasis), "Multiple 0");
        test.ok(!tm.match(strong),  "Multiple 1");
        test.ok(!tm.match(para),    "Multiple 2");
        test.ok(tm.match(section),  "Multiple 3");
        test.done();
    });
};



