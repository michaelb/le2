// Test high-level actions
var fs = require('fs');
var path = require('path');
var os = require('os');
var userid = require('../lib/util/userid');
var fsutils = require('../lib/util/fsutils');
var _tag = require('../lib/markup/tag'),
    TagLoader = _tag.TagLoader,
    Tag       = _tag.Tag;
var _style = require('../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;
var _ = require('underscore');

var helpers = require('./helpers/test_helpers');

exports.test_user_stuff = function (test) {
    /*
    Tests User  directories, and SCROLL_RCPATH, and SCROLL_HOME features
    */
    test.expect(6);
    helpers.with_tmp_homedir(test, {}, function (teardown, tmpdir) {
        userid.get_conf_dir(function (dir, info, rcpath) {
            test.equal(dir, path.join(tmpdir, ".config", "scroll"));
            test.equal(rcpath, path.join(tmpdir, ".config", "scroll", "scrollrc"));
            var some_user = process.env.USER || process.env.USERNAME;
            test.equal(some_user, info.username, "Detected username");
            test.ok(info.bestname, "Detected some name for user");

            var NEWRC = path.join(tmpdir, "lolrc")
            var NEWCONF = path.join(tmpdir, "scrolly")

            helpers.with_env({
                SCROLL_RCPATH: NEWRC,
                SCROLL_HOME: NEWCONF,
            }, function (teardown2) {
                fsutils.defaultfile(NEWRC, ".", function () {
                    userid.get_conf_dir(function (dir, info, rcpath) {
                        test.equal(dir, NEWCONF);
                        test.equal(rcpath, NEWRC);
                        teardown2();
                        teardown();
                        test.done();
                    });
                });
            });
        });
    });
};


exports.test_autoloader = function (test) {
    test.expect(5);
    // attempts to use the autoloading thing to load tags
    var rootpath  = path.join(helpers.DATA_PATH, "nstags")
    fsutils.autoloader(TagLoader, rootpath, function (tl) {
        // there are 2 test tags, makes sure everything is as expected
        test.equal(tl.all_tags.length, 2);
        test.equal(tl.list.length, 2);
        test.equal(tl.tags.test_one.para.containment.tags.length, 1);
        test.equal(tl.tags.test_two.emphasis.containment.tags.length, 1);
        test.equal(tl.paths_list.length, 2);
        test.done();
    });
};


exports.test_autoloader_empty = function (test) {
    // Ensure loader functions with empty results
    test.expect(2);
    var done = _.after(2, function () { test.done(); });
    var rootpath  = path.join(helpers.DATA_PATH, "empty");
    fsutils.autoloader(TagLoader, rootpath, function (tl) {
        test.equal(tl.list.length, 0);
        done();
    });
    fsutils.autoloader(StyleLoader, rootpath, function (sl) {
        test.equal(sl.list.length, 0);
        done();
    });
};

