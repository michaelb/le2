var utilsorting = require('../lib/util/sorting');

process.on('uncaughtException', function(err) {
    console.error(err.stack);
});

exports.test_toposorting = function (test) {
    //test.expect(1);
    var info_list = [
        {
            name: 'jquery',
            depends: null,
            provides: ["jquery"]
        },
        {
            name: 'jqplug0',
            depends: ["jquery"],
            provides: ["anotherthing"]
        },
        {
            name: 'jqplug1',
            depends: ["jquery", "anotherthing"],
            provides: null
        },
        {
            name: 'unrelated',
            depends: null,
            provides: null,
        },
    ];

    var get_names = function (list) {
        var names = [];
        list.forEach(function (a) { names.push(a.name); });
        return names;
    };

    // Makes sure basic ordering works
    var result = utilsorting.sort_info_list(info_list);
    test.deepEqual(['unrelated', 'jquery', 'jqplug0', 'jqplug1'], get_names(result));

    // Makes sure disconnected graphs work
    info_list = info_list.concat([
        {
            name: 'bs', depends: [], provides: ['bs', 'gui']
        },
        {
            name: 'button', depends: ['gui'], provides: null
        },
        {
            name: 'item', depends: ['bs'], provides: ['gui']
        },
    ]);
    var result = utilsorting.sort_info_list(info_list);
    var expres = [ 'unrelated', 'bs', 'item', 'button', 'jquery', 'jqplug0', 'jqplug1' ];
    test.deepEqual(expres, get_names(result));

    ///////////////////// Again with different ordering

    var info_list = [ { name: 'jqplug2', depends: ["jquery", "anotherthing"],
    provides: null }, { name: 'unrelated', depends: null, provides: null, }, {
    name: 'jqplug0', depends: ["jquery"], provides: ["anotherthing"] }, { name:
    'jqplug1', depends: ["jquery", "anotherthing"], provides: null }, { name:
    'jquery', depends: null, provides: ["jquery"] }, ];

    // Makes sure basic ordering works
    var result = utilsorting.sort_info_list(info_list);
    test.deepEqual(['unrelated', 'jquery', 'jqplug0', 'jqplug1', 'jqplug2'], get_names(result));

    // Makes sure disconnected graphs work
    info_list = info_list.concat([ { name: 'button', depends: ['gui'],
    provides: null }, { name: 'bs', depends: [], provides: ['bs', 'gui'] }, {
    name: 'item', depends: ['bs'], provides: ['gui'] }, ]);
    var result = utilsorting.sort_info_list(info_list);
    var expres = [ 'unrelated', 'bs', 'item', 'button', 'jquery', 'jqplug0', 'jqplug1', 'jqplug2'];
    test.deepEqual(expres, get_names(result));


    test.done();
};


exports.test_toposorting2 = function (test) {
    test.done();
};

/*
exports.test_core_plugins = function (test) {
    pluginloader.get_frontend_files(function (result) {
        console.log(result);
        test.done();
    });
};
*/

