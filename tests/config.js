// Test high-level actions
var fs = require('fs');
var config = require('../lib/data/config');
var path = require('path');
var os = require('os');
var _ = require('underscore');

var helpers = require('./helpers/test_helpers');

exports.test_loading = function (test) {
    helpers.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        test.ok(config.stash.styles, "Style stash loaded");
        test.ok(config.stash.tags, "Tag stash loaded");
        test.ok(config.GIT_TEMPLATE_DIR, "template dir set");
        test.equal(config.stash.styles.list.length, 1);
        test.equal(config.stash.tags.list.length, 5);

        teardown();
        test.done();
    });
};

exports.subprogram = function (test) {
    helpers.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        var git = config.programs["git"];
        var git_output = [];
        git.spawn({
            args: ["--version"],
            on_stdout_data: _.bind(git_output.push, git_output),
        }, function () {
            var E = "git version";
            test.equal(git_output.join('').slice(0, E.length), E);
            teardown();
            test.done();
        });
    });
};
