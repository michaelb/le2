var _ = require('underscore');
_.str = require('underscore.string');
var ScrollMarkdownParser = require('../lib/markup/markdownparser').ScrollMarkdownParser;
var EditorRenderer = require('../lib/markup/renderer').EditorRenderer;
var _tag = require('../lib/markup/tag'),
    TagLoader = _tag.TagLoader,
    Tag       = _tag.Tag;
var glob = require('glob');
var path = require('path');

var helpers = require('./helpers/test_helpers');

exports.test_edit_renderer_core = function (test) {
    helpers.load_edit_renderer({disable_custom_html: true}, function (parser, renderer) {
        var text = [
            "para 1 some -- sy < mb >ols",
            "",
            "para 2",
            "continued nested *inline u{stuff} to see*",
            "",
            "## section",
            "",
            "para 3",
        ].join("\n");

        var expected = ['<bk class="', 'testing_para', '" data="',
            'para 1 some -- sy < mb >ols', '">',
            'para 1 some &mdash; sy &lt; mb &gt;ols', '</bk>', '<bk class="',
            'testing_para', '" data="',
            'para 2 continued nested *inline u{stuff} to see*',
            '">', 'para 2\ncontinued nested ', '<in class="', 'testing_strong',
            '">', 'inline ', '<in class="', 'testing_emphasis', '">', 'stuff',
            '</in>', ' to see', '</in>', '</bk>', '<bk class="',
            'testing_section', '" data="', '## section', '">', ' section',
            '</bk>', '<bk class="', 'testing_para', '" data="', 'para 3',
            '">', 'para 3', '</bk>',].join('')

        renderer.render_to_string(text, parser, function (result) {
            test.equal(result, expected);
            test.done();
        });
    });
};


exports.test_edit_renderer_paragraph_only = function (test) {
    helpers.load_edit_renderer({ONLY_DO_PARA:true}, function (parser, renderer) {
        var text = [
            "para 1 some -- sy < mb >ols",
            "",
            "para 2",
            "continued nested *inline u{stuff} to see*",
            "",
            "## section",
            "",
            "para 3",
        ].join("\n");

        var expected = [
            '<bk class="testing_para', '" data="',
            'para 1 some -- sy < mb >ols', '">',
            'para 1 some -- sy &lt; mb &gt;ols', '</bk>',
            '<bk class="testing_para', '" data="',
            'para 2 continued nested *inline u{stuff} to see*', '">',
            'para 2\ncontinued nested *inline u{stuff} to see*', '</bk>',
            '<bk class="testing_para', '" data="', '## section', '">',
            '## section', '</bk>', '<bk class="testing_para',
            '" data="', 'para 3', '">', 'para 3', '</bk>',
        ].join('');

        renderer.render_to_string(text, parser, function (result) {
            test.equal(result, expected);
            test.done();
        });
    });
};


exports.test_edit_renderer = function (test) {
    helpers.load_edit_renderer({}, function (parser, renderer) {
        var text = [
            "para 1 some -- sy < mb >ols",
            "",
            "para 2",
            "continued nested *inline u{stuff} to see*",
            "",
            "## section",
            "",
            "para 3",
        ].join("\n");

        var res = [
            '<bk class="testing_para" data="para 1 some -- sy < mb >ols">',
            'para 1 some &mdash; sy &lt; mb &gt;ols</bk>',
            '<bk class="testing_para" data="para 2 continued nested *inline u{stuff} to see*">',
            'para 2\ncontinued nested <in class="testing_strong">',
            'inline <in class="testing_emphasis">',
            'stuff</in> ', 'to see</in>', '</bk>',
            '<bk class="testing_section" data="## section">', '<h1>', ' section</h1>', '</bk>',
            '<bk class="testing_para" data="para 3">', 'para 3</bk>',].join("");

        renderer.render_to_string(text, parser, function (result) {
            test.equal(res, result);
            test.done();
        });
    });
};

