/*
Lots of utility functions, sometimes used in only one place, just to keep the
tests clean and free of setup / teardown cruft.
*/

var _ = require('underscore');
_.str = require('underscore.string');
var fs = require('fs');
var glob = require('glob');
var path = require('path');
var schemaconf   = require('schemaconf');
var actions = require('../../lib/data/actions');
var gitactions = require('../../lib/data/gitactions');
var Workspace = require('../../lib/data/workspace');
var Config = require('../../lib/data/config');
var _tag = require('../../lib/markup/tag'),
    TagLoader = _tag.TagLoader,
    Tag       = _tag.Tag;
var fsutils     = require('../../lib/util/fsutils');
var tag_schemas = require('../../lib/markup/schemas');
var Structure   = require('../../lib/markup/structure');
var ScrollMarkdownParser = require('../../lib/markup/markdownparser').ScrollMarkdownParser;
var _renderer = require('../../lib/markup/renderer'),
    EditorRenderer = _renderer.EditorRenderer,
    StyleRenderer = _renderer.StyleRenderer;
var _parser = require('../../lib/markup/parser'),
    TreeParser = _parser.TreeParser,
    StructuredParser = _parser.StructuredParser;
var _style = require('../../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;
var _profile = require('../../lib/export/exportprofile'),
    ProfileLoader = _profile.ProfileLoader,
    Profile       = _profile.Profile;
var get_parser = require('../../lib/markup/markdownparser').get_parser;

process.on('uncaughtException', function (err) {
    console.error("uncaught exception");
    console.error(err);
    console.error(err.stack);
    console.trace();
});

var PATH_PREFIX = path.resolve(path.join(__dirname, "..", "testdata"));
exports.DATA_PATH = PATH_PREFIX;

exports.load_tags = function (cb) {
    var tag_groups = [{
        "namespace": "testing",
        "paths": glob.sync(path.join(PATH_PREFIX, "tags", "*.cfg")),
    }];
    var tl = new TagLoader(tag_groups);
    tl.load(function () { cb(tl); });
};

exports.load_tags = function (cb) {
    var paths = glob(path.join(PATH_PREFIX, "tags", "*.cfg"), function (err, paths){
        var tag_groups = [{
            "namespace": "testing",
            "paths": paths,
        }];
        var tl = new TagLoader(tag_groups);
        tl.load(function () { cb(tl); });
    })
};

exports.load_para = function (cb) {
    var tag_groups = [{
        "namespace": "testing",
        "paths": [path.join(PATH_PREFIX, "tags", "para.cfg")],
    }];
    var tl = new TagLoader(tag_groups);
    tl.load(function () { cb(tl); });
};




exports.load_structure = function (cb, opts) {
    var opts = opts || {};
    exports.load_tags(function (tl) {
        var structure_schema = new schemaconf.ConfSchema(tag_schemas.structure);
        fs.readFile(path.join(PATH_PREFIX, "structure",
                opts.STRUCTFILENAME || "structure2.cfg"), function (err, data) {
            if (err) { throw new Error("READ ERR: " + err)};
            data = schemaconf.parse(data + '');
            var vdata = structure_schema.validate(data);
            var st = new Structure(vdata);
            st.prepare(function () {
                cb(tl, st);
            });
        });
    });
};



exports.in_tmp_dir = function (test, callback) {
    /*
    var old_done = _.bind(test.done, test);
    var tmp_dir_location = null;
    test.done = function () {
        console.log("messy tmp dir", tmp_dir_location);
        old_done();
    };
    */

    fsutils.mkdirtmp(function (path) {
        //console.log( "Making tmp dir1:" + path);
        process.chdir(path);
        //tmp_dir_location = path;
        callback(path);
    });
};


exports.load_parser = function (cb, opts) {
    var func = (opts.ONLY_DO_PARA ? exports.load_para : exports.load_tags);
    func(function (tl) {
        var parser = new ScrollMarkdownParser(tl, opts);
        parser.compile(function () {
            cb(parser);
        });
    });
};

exports.load_parser2 = function (cb, opts) {
    var opts = opts || {};
    var func = (opts.ONLY_DO_PARA ? exports.load_para : exports.load_tags);
    func(function (tl) {
        var parser = get_parser("md", tl, opts);
        parser.compile(function () {
            cb(parser);
        });
    });
};

exports.load_parser = exports.load_parser2;

exports.load_edit_renderer = function (opts, cb) {
    var popts = {emit_source: true};
    if (opts.ONLY_DO_PARA) {
        popts.ONLY_DO_PARA = true;
    }
    exports.load_parser(function (parser) {
        var renderer = new EditorRenderer(parser.tagloader, opts);
        renderer.compile(function () {
            cb(parser, renderer);
        });
    }, popts);
};


exports.load_style_renderer = function (opts, cb) {
    var style = opts.style;
    exports.load_tree_parser(function (parser) {
        var renderer = new StyleRenderer(parser.tagloader, style, opts);
        renderer.compile(function () {
            cb(parser, renderer);
        });
    }, opts);
};



exports.load_structure_parser = function (cb, opts) {
    exports.load_structure(function (tl, st) {
        var parser = new StructuredParser(tl, st, opts);
        parser.compile(function () {
            cb(parser);
        });
    }, opts);
};


exports.load_tree_parser = function (cb, opts) {
    exports.load_structure(function (tl, st) {
        var parser = new TreeParser(tl, st, opts);
        parser.compile(function () {
            cb(parser);
        });
    }, opts);
};

exports.tokens_side_by_side = function (list1, list2) {
    for (var i = 0; i < Math.max(list1.length, list2.length); i++) {
        var ch = '';
        if (_.isEqual(list1[i], list2[i])) {
            ch = " | ";
        } else {
            ch = " X ";
        }
        console.log(i, ch, list1[i], list2[i]);
    }
};

exports.pp_html = function (s1) {
    var html = require('html');
    console.log(html.prettyPrint(s1));
};

exports.html_diff = function (s1, s2) {
    var html = require('html');
    var s = function (v) { return html.prettyPrint(v).split("\n"); };
    return exports.tokens_side_by_side(s(s1), s(s2));
};


exports.load_style = function (cb) {
    var paths = glob(path.join(PATH_PREFIX, "styles", "*.cfg"), function (err, paths){
        var style_groups = [{
            "namespace": "testing",
            "paths": paths,
        }];
        var sl = new StyleLoader(style_groups);
        sl.load(function () { cb(sl); });
    })
};


exports.load_profiles = function (cb) {
    var paths = glob(path.join(PATH_PREFIX, "profiles", "*.cfg"), function (err, paths){
        var style_groups = [{
            "namespace": "testing",
            "paths": paths,
        }];
        var sl = new ProfileLoader(style_groups);
        sl.load(function () { cb(sl); });
    })
};


exports.ast_strip_tags = function (o) {
    // Cleans up the AST,  removing unneeded info, so we can do deep compares
    if (o.tag && o.tag.name) { o.tag = o.tag.name; }
    if (o.parent && o.parent.tag) { o.parent = o.parent.tag; }
    o.children.forEach(exports.ast_strip_tags);
    o.head.forEach(exports.ast_strip_tags);
    if (o.children.length < 1) { delete o.children }
    if (o.head.length < 1) { delete o.head }
    if (o.text === null) { delete o.text }
    if (o.tag === null) { delete o.tag }
    // clean up other non-essential fields
    delete o.is_unranked;
    delete o.rank;
};



exports.with_env = function (env, callback) {
    var old_env = _.extend({}, process.env);
    _.extend(process.env, env);
    var teardown = function () {
        // restore old values
        for (var key in process.env) {
            if (!(key in old_env)) {
                delete process.env[key];
            } else if (process.env[key] !== old_env[key]) {
                process.env[key] = old_env[key];
            }
        }
    };
    callback(teardown);
};


exports.with_tmp_homedir = function (test, opts, callback) {
    exports.in_tmp_dir(test, function (tmppath) {
        var env = _.extend({
            "HOME": tmppath,
            "XDG_CONFIG_HOME": path.join(tmppath, ".config"),
        }, opts.env);
        exports.with_env(env, function (teardown) {
            callback(teardown, tmppath);
        });
    });
};



exports.with_init_conf = function (test, opts, callback) {
    exports.with_tmp_homedir(test, {}, function (teardown, tmpdir) {
        actions.init_conf(function () {
            callback(teardown, tmpdir);
        }, function () {
            console.error("error initting conf " + tmpdir);
        }, { datadir: path.join(PATH_PREFIX, "config"), });
    });
};

exports.with_testconfig = function (test, opts, callback) {
    // Sets up a fake homedir in tmp, inits a config obj
    exports.with_init_conf(test, {}, function (teardown, tmpdir) {
        var config = new Config(opts);
        config._test_helper_tmpdir = tmpdir;
        config.load(function () {
            callback(config, tmpdir, teardown);
        });
    });
};

exports.test_list_existence = function (test, list, prefix, callback) {
    var count = list.length;
    if (!callback) { prefix = callback; }

    var done = _.after(count, function () {
        callback();
    });

    list.forEach(function (p) {
        var name = path.basename(p);
        if (prefix) { p = path.join(prefix, p); }

        fs.exists(p,  function (fexists) {
                test.ok(fexists, "checking if " + name + " exists");
                done();
            });
    });
};

exports.with_empty_workspace = function (test, opts, callback) {
    exports.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        process.chdir(tmpdir);
        actions.init(".", function () {
            var workspace = new Workspace(tmpdir);
            workspace.prepare(function () {
                callback(config, workspace, teardown);
            });
        }, function () {
            console.ok(false, "error initting " + tmp_dir);
            test.done()
        });
    });
};


exports.with_scrollfile = function (test, filepath, opts, callback) {
    // Not an absolute path, assume it's a filename
    if (!filepath.match('^/')) {
        filepath = path.join(PATH_PREFIX, "files", filepath);
    }

    exports.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        var filedir = path.join(tmpdir, "opened-file");
        var gitopts = { dir: filedir, silent: !opts.noisy };
        var uri = "scrollfile://" + filepath;
        gitactions.gitpull(config, uri, gitopts, function () {
            process.chdir(filedir);
            var workspace = new Workspace(filedir, uri);
            workspace.prepare(function () {
                callback(config, workspace, teardown);
            });
        });
    });
};


exports.with_workspace = function (test, filepath, opts, callback) {
    // Not an absolute path, assume it's a filename
    if (!filepath.match('^/')) {
        filepath = path.join(PATH_PREFIX, "workspaces", filepath);
    }

    exports.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        process.chdir(filepath);
        var workspace = new Workspace(filepath);
        workspace.prepare(function () {
            callback(config, workspace, teardown);
        });
    });
};


