ROOT_PATH="$( cd "$BATS_TEST_DIRNAME" && cd .. && cd .. && pwd )"
scroll="$ROOT_PATH/bin/scroll --config $ROOT_PATH/data/conf/"

setup () {
    cd $(mktemp -d -t "scroll_datatest_XXXXX")
}


@test "freeform template" {
    $scroll new base basic

    # Ensure a bunch of files exist
    [ -d tags/base ]
    [ -f tags/base/para.cfg ]
    [ -f tags/base/emphasis.cfg ]

    # Now try adding some text to document.md and make sure we can render  a
    # simple thing properly
    echo "
##A section

A paragraph *with a variety* of ~~mistakes~~ and **various** styling.

<base_blockquote>
Paragraph in quote 1

Paragraph in quote 2
</base_blockquote>" > document.md

    { echo "
<h1>A section</h1>
<p>A paragraph <em>with a variety</em> of <del>mistakes</del> and <strong>various</strong> styling.</p>
<blockquote>
<p>Paragraph in quote 1</p>
<p>Paragraph in quote 2</p>
</blockquote>
" | tr -d '\n'; echo ; } > expected_results

    $scroll render < document.md > render_results
    echo "--RENDERED RESULTS--"
    cat render_results 1>&2
    echo "------"
    echo "--EXPECTED RESULTS--"
    cat expected_results 1>&2
    echo "------"
    cmp render_results expected_results
}

