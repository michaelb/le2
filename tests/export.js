//// Basic test for loading and searching a style object
var export_actions = require('../lib/export/actions');
var fsutils = require('../lib/util/fsutils');
var fs = require('fs');
var glob = require('glob');
var path = require('path');
var helpers = require('./helpers/test_helpers');
var _ = require('underscore');
_.str = require('underscore.string');


var arg_coerce = function (arg) {
    return Array.prototype.slice.call(arg);
};

var _scrub = function (s) {
    return s.replace(/[\n\r\s ]+/g, '')
            .replace(/<[^>]+>/g, '');
};

var ExportProfile = require('../lib/export/exportprofile').ExportProfile;

exports.test_actions_basic_render = function (test) {
    helpers.with_workspace(test, "basic_ws", {}, function (config, workspace, teardown) {
        fsutils.mkdirtmp(function (tmpdir) {
            export_actions.render_workspace(config, workspace, tmpdir, {}, function () {
                //console.log("this is output dir", tmpdir);
                helpers.test_list_existence(test, ['document.html'], tmpdir, function () {
                    teardown();
                    test.done();
                });
            });
        });
    });
};

var process_one_file = function (test, ep, workspace, asset, callback) {
    var events = [];

    var setup_event = function (name) {
        ep.on(name, function (a, b, c, d, e) {
            var a = arg_coerce(arguments);
            a.unshift(name);
            events.push(a);
        });
    };

    setup_event('write');
    setup_event('copy');
    setup_event('move');
    ep.on('done', function () {
        callback(events);
    });

    ep.handle_file(workspace, asset);
};

exports.test_blank_exportprofile = function (test) {
    var test_done = _.after(2, function () { test.done() });
    helpers.with_workspace(test, "basic_ws", {}, function (config, workspace, teardown) {
        var ep = new ExportProfile('testns', 'test', {});
        process_one_file(test, ep, workspace, { path: "document.md" }, function (events) {
            test.equal(events.length, 1);
            test.equal(events[0][0], 'write');
            test.equal(events[0][1], 'document.html');
            test.equal(_scrub(events[0][2]),
                    _scrub(['Test document',
                        'A paragraph some inline stuff',
                        'Some more data, looking good',
                        'Another section',
                        'More paragraphs',
                        'Lorem ipsum',].join('')));
            test_done();
        });

        var ep2 = new ExportProfile('testns', 'test', {});
        process_one_file(test, ep2, workspace, { path: "images/myimage.jpg" }, function (events) {
            test.deepEqual(events, [['copy', 'images/myimage.jpg', 'images/myimage.jpg']]);
            test_done();
        });
    });
};

