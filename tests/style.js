//// Basic test for loading and searching a style object
var _style = require('../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;
var fs = require('fs');
var glob = require('glob');
var path = require('path');
var helpers = require('./helpers/test_helpers.js');

var EMPTY_STYLE = new Style('testing', 'empty', { template: [] });

var trim = function (a) { return a.replace(/\s*/g, ''); };
var TEXT = [
    'p0',
    // dont have have 2 newlines after blockquote, to prevent extraneous newlines
    '<testing_blockquote>\na quote\n</testing_blockquote>p1',
    '## doc',
        'p2',
        'some *formatted* text',
    '## doc',
        'p4',
        // dont have full two newlines after blockquote, to prevent extraneous newlines
        '<testing_blockquote>\n\n</testing_blockquote>p5',
            'p6',
    ].join("\n\n");

var EXPECTED_NO_STYLE = [
        '<div class="testing_para">', 'p0</div>',
        '<blockquote>', '<div class="testing_para">', 'a quote</div>', '</blockquote>',
        '<div class="testing_para">', 'p1</div>',
        '<h1>', ' doc</h1>',
        '<div class="testing_para">', 'p2</div>',
        '<div class="testing_para">', 'some <span class="testing_strong">',
            'formatted</span>', ' text</div>',
        '<h1>', ' doc</h1>',
        '<div class="testing_para">', 'p4</div>',
        '<blockquote>', '</blockquote>',
        '<div class="testing_para">', 'p5</div>',
        '<div class="testing_para">', 'p6</div>',].join('')

exports.test_empty_style = function (test) {
    var opts = { style: EMPTY_STYLE, STRUCTFILENAME: "structure2.cfg" };
    helpers.load_style_renderer(opts, function (parser, renderer) {
        var text = TEXT;
        var expected = EXPECTED_NO_STYLE;
        renderer.render_to_string(text, parser, function (result) {
            //console.log("THIS IS RESULT", result.split(">").join(">',\n"));
            test.equal(expected, result);
            //helpers.html_diff(expected, result);
            //helpers.pp_html(result);
            test.done();
        });
    });
};

var EXPECTED_WITH_STYLE = [
    '<p>p0</p>', '<div class="quotation">', '<blockquote>The ',
    'quote:', '<p>a quote</p>', '</blockquote>', '<div',
    'class="discussion">', '<p>p1</p>', '</div>', '</div>', '<h1 ',
    'class="test"> doc</h1>', '', '<p>p2</p>', '<p>some ',
    '<strong>formatted</strong>', 'text</p>', '<hr />', '<h1 ',
    'class="test"> doc</h1>', '<p>p4</p>', '<div class="quotation">',
    '<blockquote>No quote.</blockquote>', '<div class="discussion">',
    '<p>p5</p>', '<p>p6</p>', '</div>', '</div>', '<hr />',
].join('');


exports.test_style_basic = function (test) {
    helpers.load_style(function (sl) {
        var style = sl.all_styles[0];
        test.equal(sl.all_styles.length, 1);
        var opts = { style: style, };
        helpers.load_style_renderer(opts, function (parser, renderer) {
            var text = TEXT;
            var expected = EXPECTED_WITH_STYLE;
            renderer.render_to_string(text, parser, function (result) {
                test.equal(trim(expected), trim(result));
                test.done();
            });
        });
    });
};


