#!/bin/bash

########################
# CD into up dir
MY_PATH="`dirname \"$0\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"
cd $MY_PATH
cd ..
########################

# Add BATS to path
export PATH="$MY_PATH/helpers/bats/bin:$PATH"
########################

# die on err
set -e

#source ~/.nvm/nvm.sh ;nvm use 0.10 ;npm install

#if [ "$1" = "JUNIT" ];
#then
#    NUFLAGS=--arr
#fi

#NODEUNIT="/usr/bin/env node --use_strict ./node_modules/.bin/nodeunit"
#NODEUNIT="/usr/bin/env node --harmony_proxies ./node_modules/.bin/nodeunit"
NODEUNIT="/usr/bin/env node ./node_modules/.bin/nodeunit"
NODEVERSION=`/usr/bin/env node --version`
#--harmony_proxies ./node_modules/.bin/nodeunit"

bats_cli_all () {
    echo "--- CLI tests (BATS)"
    bats ./tests/clitests/*.sh
}


bats_data_all () {
    echo "--- Data tests (BATS)"
    bats ./tests/datatests/*.sh
}

nodeunit_one () {
    $NODEUNIT $1 $2 $3 $4 $5 $6 $7
}

nodeunit_all () {
    echo "--- Unit tests (NodeUnit) - node $NODEVERSION"
    $NODEUNIT ./tests/
}

nodeunit_gui_all () {
    echo "--- GUI end-to-end tests (NodeUnit) - node $NODEVERSION"
    #$NODEUNIT ./tests/guitests/
    xvfb-run $NODEUNIT ./tests/guitests/
}

do_all () {
    nodeunit_all
    bats_cli_all
    bats_data_all # disabled for now
}

if [ "$1" = "cli" ];
then
    bats_cli_all
elif [ "$1" = "data" ];
then
    bats_data_all
elif [ "$1" = "unit" ];
then
    nodeunit_all
elif [ "$1" = "gui" ];
then
    nodeunit_gui_all
elif [ "$1" = "all" ];
then
    do_all
    nodeunit_gui_all
elif [ -n "$1" ];
then
    nodeunit_one "./tests/$1.js" $2 $3 $4 $5 $6 $7
else
    do_all
fi

