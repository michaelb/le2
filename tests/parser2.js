var _ = require('underscore');
_.str = require('underscore.string');
var helpers = require('./helpers/test_helpers');

var REV = {
    1: "TEXT",
    2: "TAG",
    3: "OPEN_TAG",
    4: "CLOSE_TAG",
};

var load_parser = helpers.load_parser2;


exports.test_parser_trim_whitespace = function (test) {
    // the parser should trim whitespace by default
    load_parser(function (parser) {
        var text = [
            "",
            "## section",
            "",
            "   a paragraph  ",
            "",
        ].join("\n");
        var result = [
            ["OPEN_TAG", "section"],
                ["TEXT", " section"],
            ["CLOSE_TAG", "section"],
            ["OPEN_TAG", "para"],
                ["TEXT", "a paragraph"],
            ["CLOSE_TAG", "para"],
        ];

        var contents = [];
        parser.parse(text, function (type, tag, value) {
            contents.push([REV[type], tag ? tag.name : value]);
        }, function () {
            test.deepEqual(result, contents);
            test.done();
        });
    });
};


exports.test_basic_parsing = function (test) {
    // more complicated example
    load_parser(function (parser) {
        var text = [
            "## section",
            "",
            "para 1 some -- sy < mb >ols",
            "",
            "para 2",
            "continued nested *inline u{stuff} to see*",
            "",
            "## section",
            "",
            "para 3",
        ].join("\n");

        var result = [
            ["OPEN_TAG", "section"],
                ["TEXT", " section"],
            ["CLOSE_TAG", "section"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 1 some "],
                    ["OPEN_TAG", "emdash"],
                    ["CLOSE_TAG", "emdash"],
                ["TEXT", " sy < mb >ols"],
            ["CLOSE_TAG", "para"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 2\ncontinued nested "],
                    ["OPEN_TAG", "strong"],
                        ["TEXT", "inline "],
                        ["OPEN_TAG", "emphasis"],
                            ["TEXT", "stuff"],
                        ["CLOSE_TAG", "emphasis"],
                        ["TEXT", " to see"],
                    ["CLOSE_TAG", "strong"],
            ["CLOSE_TAG", "para"],
            ["OPEN_TAG", "section"],
                ["TEXT", " section"],
            ["CLOSE_TAG", "section"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 3"],
            ["CLOSE_TAG", "para"],
        ];

        var contents = [];
        parser.parse(text, function (type, tag, value) {
            contents.push([REV[type], tag ? tag.name : value]);
        }, function () {
            test.deepEqual(result, contents);
            test.done();
        });

    });
};


exports.test_xml_parsing = function (test) {
    // more complicated example with XML style tags
    load_parser(function (parser) {
        var text = [
            "<section>",
            " section",
            "</section><testing_para>",
            "para 1 some -- sy < mb >ols",
            "</testing_para>"+
            "para 2",
            "continued nested "+
            "<strong>",
            "inline "+
            "<emphasis>",
            "stuff",
            "</emphasis> to see",
            "</strong>",
            "",
            "## section",
            "",
            "para 3",
        ].join("\n");

        var result = [
            ["OPEN_TAG", "section"],
                ["TEXT", " section"],
            ["CLOSE_TAG", "section"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 1 some "],
                    ["OPEN_TAG", "emdash"],
                    ["CLOSE_TAG", "emdash"],
                ["TEXT", " sy < mb >ols"],
            ["CLOSE_TAG", "para"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 2\ncontinued nested "],
                    ["OPEN_TAG", "strong"],
                        ["TEXT", "inline "],
                        ["OPEN_TAG", "emphasis"],
                            ["TEXT", "stuff"],
                        ["CLOSE_TAG", "emphasis"],
                        ["TEXT", " to see"],
                    ["CLOSE_TAG", "strong"],
            ["CLOSE_TAG", "para"],
            ["OPEN_TAG", "section"],
                ["TEXT", " section"],
            ["CLOSE_TAG", "section"],
            ["OPEN_TAG", "para"],
                ["TEXT", "para 3"],
            ["CLOSE_TAG", "para"],
        ];

        var contents = [];
        parser.parse(text, function (type, tag, value) {
            contents.push([REV[type], tag ? tag.name : value]);
        }, function () {
            //helpers.tokens_side_by_side(result, contents);
            test.deepEqual(result, contents);
            test.done();
        });

    });
};

