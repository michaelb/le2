// Test high-level actions
var fs = require('fs');
var actions = require('../lib/data/actions');
var Config = require('../lib/data/config');
var path = require('path');
var os = require('os');
var _ = require('underscore');

var helpers = require('./helpers/test_helpers');

exports.test_init = function (test) {
    var COUNT = 3;
    test.expect(COUNT);
    var done = _.after(COUNT, function () {
        test.done();
    });

    helpers.in_tmp_dir(test, function (tmp_dir) {
        actions.init(".", function () {
            var exists = function (path, fexists) {
                test.ok(fexists, "checking if " + path + " exists");
                done();
            };
            fs.exists("./manifest.cfg",  _.partial(exists, "manifest.cfg"));
            //fs.exists("./paragraph.cfg", _.partial(exists, "paragraph.cfg"));
            fs.exists("./document.md",   _.partial(exists, "document.cfg"));
            fs.exists("./.scrollid",     _.partial(exists, ".scrollid"));
        }, function () {
            console.error("error initting " + tmp_dir);
            test.done()
        });
    });
};


exports.test_init_conf = function (test) {
    var check_existence = function (teardown, tmpdir) {
        var TOTAL = 3;
        var done = _.after(TOTAL, function () {
            check_config(teardown, tmpdir);
        });

        actions.init_conf(function () {
            var exists = function (path, fexists) {
                test.ok(fexists, "checking if " + path + " exists: " + tmpdir);
                done();
            };

            fs.exists(".config/scroll/scrollrc",  _.partial(exists, "scrollrc"));
            fs.exists(".config/scroll/stash/templates/nstest/standarddoc.cfg",  _.partial(exists, "standarddoc.cfg"));
            fs.exists(".config/scroll/stash/tags/testing/blockquote.cfg",  _.partial(exists, "blockquote.cfg"));
        }, function () {
            console.error("error initting conf " + tmp_dir);
            test.done()
        }, { datadir: path.join(helpers.DATA_PATH, "config"), });
    };

    var check_config = function (teardown, tmpdir) {
        var config = new Config();
        config.load(function () {
            // ensure that at least git was found
            test.ok(config.rc.user.name, "rc has username");
            test.ok(_.find(config.rc.program, {name: "git"}),
                    "system has git, and was detected");
            teardown();
            test.done();
        });
    };

    helpers.with_tmp_homedir(test, {}, check_existence);
};

/* **************************************** */
/* ************** STASH ******************* */
exports.test_stash_list = function (test) {
    var stripinfo = function (o) {
        var allowed = ['filename', 'namespace', 'name'];
        return _.pick(o, function (value, key) {
            return _.contains(allowed, key);
        });
    };
    var normlist = function (l) {
        var data = l.map(stripinfo).map(JSON.stringify);
        data.sort(); // ensures in a particular order
        return data.map(JSON.parse);
    };

    var EXPECTED_TAGS = [
        { filename: 'blockquote', namespace: 'testing', name: 'Blockquote' },
        { filename: 'emphasis', namespace: 'testing', name: 'Emphasis' },
        { filename: 'para', namespace: 'testing', name: 'Para' },
        { filename: 'section', namespace: 'testing', name: 'Section' },
        { filename: 'strong', namespace: 'testing', name: 'Strong' },
    ];

    var EXPECTED_STYLE = [
        { filename: 'basic', namespace: 'testing', name: 'test' },
    ];

    var EXPECTED_TEMPLATES = [
        { filename: 'standarddoc', namespace: 'nstest', name: 'Basic' },
    ];

    test.expect(6);
    helpers.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        actions.stash_list(config, function (o) {
            JSON.stringify(o, null)
            test.equal(o.styles.testing.length,       1);
            test.equal(o.tags.testing.length,         5);
            test.equal(o.templates.nstest.length,     1);
            test.deepEqual(normlist(o.tags.testing),     EXPECTED_TAGS);
            test.deepEqual(normlist(o.styles.testing),   EXPECTED_STYLE);
            test.deepEqual(normlist(o.templates.nstest), EXPECTED_TEMPLATES);
            test.done()
        }, function () {
            console.error("stash error" + tmp_dir);
            test.done()
        });
    });
};

exports.test_stash_load = function (test) {
    test.expect(6);
    var onlog = console.log;
    var onlog = null;
    var onerror = function (err) { test.ok(false, err); test.done() };
    var opts = {
        error: onerror,
        debug: onlog,
    };

    var from_empty = function () {
        helpers.with_empty_workspace(test, {}, function (config, workspace, teardown) {
            actions.stash_load(config, workspace, "tag", "testing", "para", function (new_workspace) {
                test.ok(new_workspace, "new workspace loaded");
                fs.exists(path.join(workspace.working_dir, "tags", "testing", "para.cfg"), function (fexists) {
                    test.ok(fexists, "file copied");
                    second_time(config, new_workspace, teardown);
                });
            }, opts);
        });
    };

    var second_time = function (config, workspace, teardown) {
        actions.stash_load(config, workspace, "tag", "testing", "emphasis", function (new_workspace) {
            fs.exists(path.join(workspace.working_dir, "tags", "testing", "emphasis.cfg"), function (fexists) {
                test.ok(fexists, "file copied");
                test.equal(new_workspace.loaders.tag.list.length, 2, "2 tags total")
                simultaneous(config, workspace, teardown);
            });
        }, opts);
    };

    var simultaneous = function (config, workspace, teardown) {
        actions.stash_load(config, workspace, "tag", "testing",
                    ["blockquote", "strong", "section"], function (new_workspace) {
            test.equal(new_workspace.loaders.tag.list.length, 5, "5 tags total")
            entire_ns(config, workspace, teardown);
        }, opts);
    };

    var entire_ns = function (config, workspace, teardown) {
        config.stash.tags.by_namespace.newns = 
            config.stash.tags.by_namespace.testing;
        config.stash.tags.paths.newns = 
            config.stash.tags.paths.testing;
        actions.stash_load(config, workspace, "tag", "newns", null, function (new_workspace) {
            test.equal(new_workspace.loaders.tag.list.length, 10, "10 tags total")
            test.done();
            teardown();
        }, opts);
    };

    from_empty();
};


/* **************************************** */
/* ************** TEMPLATE **************** */
exports.test_new = function (test) {
    test.expect(2);
    var onlog = console.log;
    var onlog = null;
    var onerror = function (err) { test.ok(false, "err: "+err); test.done() };

    var opts = {
        error: onerror,
        debug: onlog,
    };

    helpers.with_empty_workspace(test, {}, function (config, workspace, teardown) {
        // get one of our parsed templates
        var template = config.stash.templates.by_namespace.nstest.standarddoc;
        // load into the given workspace
        actions.template_load(config, workspace, template, function (new_workspace) {
            test.equal(new_workspace.loaders.tag.list.length, 5, "5 tags total");
            test.equal(new_workspace.loaders.style.list.length, 1, "1 style total");
            teardown();
            test.done();
        }, opts);
    });
};



exports.test_template_init = function (test) {
    test.expect(2);
    var onlog = console.log;
    var onlog = null;
    var onerror = function (err) { test.ok(false, "err: "+err); test.done() };

    var opts = {
        error: onerror,
        debug: onlog,
    };

    helpers.with_testconfig(test, {}, function (config, tmpdir, teardown) {
        actions.template_init(config, ".", "nstest", "standarddoc", function (new_workspace) {
            test.equal(new_workspace.loaders.tag.list.length, 5, "5 tags total");
            test.equal(new_workspace.loaders.style.list.length, 1, "1 style total");
            teardown();
            test.done();
        }, opts);
    });
};



