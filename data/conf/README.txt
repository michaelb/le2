Hey there, scroller!

Here's where we put stuff:

- scrollrc

    Global configuration file

- documents/

    New files and recently opened files go here. This is where the
    Scroll Editor "auto-saves", and on close the Scroll Editor will push
    the file to the source (e.g., either a .scroll file, or the git
    source)

- stash/

    Stash for tags, styles, and templates

