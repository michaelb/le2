var _HARDHACK = function (list) {
    // return list;
    // XXX Because the sorting mechanism is broken, this is hardhack work
    // around that puts jquery dependency at the top
    var result = [];
    for (var i in list) {
        if (list[i].provides && list[i].provides.indexOf('jquery') !== -1) {
            result.unshift(list[i]);
        } else {
            result.push(list[i]);
        }
    }
    return result;
};

var path = require('path');
var glob = require('glob');
var fs = require('fs');
var schemaconf = require('schemaconf');
var sortingutil = require('../util/sorting');
var _ = require('underscore');
_.str = require('underscore.string');
var BUILTIN_PLUGINS  = path.resolve(__dirname, '..', '..', "plugins");

var comma_or_newline_split = function (s) {
    return s ? (s.split((s.indexOf("\n") !== -1) ? "\n" : ",")
                 .map(function (s) { return _.str.clean(s); }))
             : null;
};


var features_loaded = {};

var get_parsed_plugins = function (editor, callback) {
    // loads and parses all plugins, and arranges them in sorted order
    list_available_plugins(editor, function (list) {
        // Read all plugins
        var results = [];
        for (var i in list) {
            var plugin_path = list[i];
            // read in file
            var data = fs.readFileSync(plugin_path);
            var info = null;

            if (data.length > 1) {
                // Load as cfg
                info = schemaconf.parse(''+data);
            }

            // Parse PROVIDES and DEPENDS
            var meta = info && info.meta && info.meta[0];
            var depends = comma_or_newline_split(meta && meta.depends);
            var provides = comma_or_newline_split(meta && meta.provides);

            // parse needed FEATURES
            // note: will need to separate this out when we get more things
            // loading at once
            var features = comma_or_newline_split(meta && meta.features);
            if (features) {
                for (var i in features) {
                    var module_name = features[i];
                    var modpath = '../features/' + module_name;
                    if (!features_loaded[module_name]) {
                        // an UNLOADED feature discovered!
                        features_loaded[module_name] = true;
                        var module = require(modpath);
                        module.setup(editor); // perform setup functions
                        features_loaded[module_name] = module;
                    }
                }
            }

            results.push({
                path: plugin_path,
                info: info,
                depends: depends || null,
                provides: provides || null
            });
        }

        // Now, we sort them with topographical sorting
        var sorted_results = sortingutil.sort_info_list(results);
        sorted_results = _HARDHACK(sorted_results);

        // And return the parsed versions!
        //console.log("sorted results", sorted_results);
        callback(sorted_results);
    });
};

var get_frontend_files = function (editor, callback) {
    // first list available plugins
    get_parsed_plugins(editor, function (list) {
        var results = [];

        var add_one = function (src, type) {
            if (type === 'html') {
                var relpath = path.relative(BUILTIN_PLUGINS, src);
                var id = relpath.replace(
                        new RegExp(path.sep, 'g'), "_").split('.')[0];
            } else {
                var id = null;
            }
            results.push({ type: type, src: src, id: id });
        };

        var add_all = function (plugin_dir_path, type) {
            var glob_str = path.join(plugin_dir_path, type, "*."+type);
            var array = glob.sync(glob_str);
            for (var i in array) {
                add_one(array[i], type);
            }
        };

        var add_glob = function (plugin_dir_path, glob_s, type) {
            var glob_str = path.join(plugin_dir_path, glob_s);
            var array = glob.sync(glob_str);
            for (var i in array) {
                add_one(array[i], type);
                //results.push({ type: type, src: array[i] });
            }
        };

        // Now add all JS and CSS files from that list
        for (var i in list) {
            var plugin_path = list[i].path;
            var plugin_dir_path = path.dirname(plugin_path);
            var info = list[i].info;

            // read in file
            var manifest = (info.manifest && info.manifest[0]) || null;

            /* Load manifests */
            var add_conditional = function (submanifest, type) {
                if (submanifest) {
                    submanifest = comma_or_newline_split(submanifest);
                    //console.log("add_glob", submanifest);
                    // custom, sequenced load
                    for (var i in submanifest) {
                        add_glob(plugin_dir_path, submanifest[i], type);
                    }
                } else {
                    //console.log("add_all", submanifest);
                    // unconditional, unsequenced load
                    add_all(plugin_dir_path, type);
                }
            };
            add_conditional(manifest && manifest.css, 'css')
            add_conditional(manifest && manifest.js, 'js')
            add_conditional(manifest && manifest.html, 'html')

        }

        // Finally, remove dupes
        results = sortingutil.remove_dupes(results);
        callback(results);
    });
};

var PLUGINSETS = {
    "editor": {
        deny: ["core/menu"]
    },
    "menu": {
        allow: [
            "core/menu",
            "core/themebase",
            "core/jsdeps",
            "core/ui",
        ]
    }
};
var PLUGINSETS = {}; // disabled for now

var list_available_plugins = function (editor, callback, filename) {
    // will generate a list of all available plugins, in all
    // locations on the filesystem, including embedded in a file

    filename = filename || "plugin.cfg";

    var pluginset_strategy = PLUGINSETS[editor.pluginloader_pluginset_name] || {};
    var regexp = null;
    var lst = pluginset_strategy.allow || pluginset_strategy.deny || null;
    var regexp_is_whitelist = !!(pluginset_strategy.allow);
    if (lst) {
        lst = lst.map(function (s) { return s + "/" + filename; });
        regexp = new RegExp('(' + lst.join("|") + ')$', 'i');
    }

    var glob_str = path.join(BUILTIN_PLUGINS, "**", filename);
    glob(glob_str, function (err, files) {
        if (err) { throw "plugin issue:"+err; }

        if (regexp) {
            files = files.filter(function (path) {
                var result = path.match(regexp);
                return regexp_is_whitelist ? result : !result;
            });
        }
        callback(files);
    });
};

var setup = function (editor) {
    var load_plugins_hook = function (event, pluginset_name) {
        editor.pluginloader_pluginset_name = pluginset_name;
        get_frontend_files(editor, function (file_infos) {
            //console.log("loadplugins_reply", file_infos);
            event.sender.send("loadplugins_reply", file_infos);
        });
    };

    /*
    // Let main.js hook this for us
    var ipc = require('ipc');
    ipc.on('loadplugins', load_plugins_hook);
    */
    return load_plugins_hook;
};


module.exports.setup = setup;
module.exports.get_parsed_plugins = get_parsed_plugins;
module.exports.get_frontend_files = get_frontend_files;
module.exports.list_available_plugins = list_available_plugins;


