// remember the main?
var path = require('path');
var querystring = require('querystring');
var Buff = require('../data/buff');
var PLUGIN_PREFIX = 'file://' + path.resolve(__dirname, '..', '..', 'plugins');
var HTML_PATH = PLUGIN_PREFIX + '/core/bootstrap/index.html';

var window_prefs = {
    width: 800,
    height: 600,
    "min-width": 200,
    "min-height": 100,
    "web-preferences": {
        "text-areas-are-resizable": false,
    }
};

var main = function (cli, args, options) {
    // Bootstraps the main window, either to display and edit a file, or show
    // the splash menu
    var file_path = opts.filepath;
    var instance = { "load": (file_path ? "editor" : "menu") };

    var app = require('app');  // Module to control application life.
    var BrowserWindow = require('browser-window');

    // first, hook "loadplugins", because of the race condition
    // with loading a file taking longer than bootstraping the
    // editor window
    var ipc = require('ipc');
    var loadplugins_call = null;
    var load_plugins_hook = null;
    ipc.on('loadplugins', function (event, arg) {
        loadplugins_call = [event, arg];
        if (load_plugins_hook) {
            // browser window slower than backend
            // (likely case for empty or simple files)
            load_plugins_hook(event, arg);
            load_plugins_hook = null;
            loadplugins_call = null;
        }
    });

    // load all plugins
    var setup = function () {

        var pluginloader = require('../gui/pluginloader');
        console.log("Entering editor, loading plugin-loader");
        var buff = new Buff(file_path);
        buff.prepare(function () {
            load_plugins_hook = pluginloader.setup(buff); // listen for loadplugins
            console.log("SPECIALCODE:ALLREADY");

            if (loadplugins_call !== null) {
                // backend slower than browser window
                // (likely case for larger files or inefficient
                // copy/revision cmds)
                load_plugins_hook.apply(null, loadplugins_call);
                load_plugins_hook = null;
                loadplugins_call = null;
            }
        });

        //Buff.load(window, path, function (buff) {
        //    pluginloader.setup(buff); // listen for loadplugins
        //});
    }

    // Set up the actual window
    var main_window = null;

    app.on('window-all-closed', function() {
        if (process.platform != 'darwin') { app.quit(); }
    });

    app.on('ready', function() {
        main_window = new BrowserWindow(window_prefs);
        main_window.loadUrl(HTML_PATH + '?' +
                    querystring.stringify(instance));
        main_window.on('closed', function() { main_window = null; });
        setup();
    });

};

var launch = function () {
    var args = trim_args(process.argv);
    // remove debug argument if its there
    args = args.filter(function (arg) { return !arg.match(/^--debug/); });

    // for now, just assume first argument
    if (args[0] === '-h' || args[0] === '--help' || args.length > 1) {
        console.log("Specify exactly one document to load, or specify none "+
                    "to open with default document.");
    } else if (args.length === 1) {
        main({
            filepath: args[0],
            instance: { load: "editor", }
        });
    } else {
        main({
            filepath: '!global', // loads global conf
            instance: { load: "menu", }
        });
    }
};

module.exports.main = main;

