// Editor has some high level bootstrapping functions, and is also passed
// around as a sort of global variable

var Workspace = require('../data/workspace');
var Config = require('../data/config');
var actions = require('../data/actions');

var _scrollid_regexp = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-'+
                             '[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$', 'i');

var Editor = function () {
    this.config = null;
};

Editor.prototype.load = function (path, callback) {
    // First, read in global conf info
    var _this = this;
    actions.get_or_init_conf(function (config) {
        _this.config = config;

        // Check file path:
        var file_path;
        if (path.match(_scrollid_regexp)) {
            // is a scroll id, dir is in config
            file_path = config.path_for("documents", path);
        } else {
            // just assume is an absolute path
            file_path = path;
        }

        // Now we read in file path, and set up workspace
        _this.workspace = new Workspace(file_path, null);
        _this.workspace.prepare(function () {
            callback();
        });
    }, function (err) {
        cli.status("could not init config: " + err, "fatal");
    });
};


module.exports = Editor;

