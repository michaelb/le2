/*
Platform specific user code, for finding home dirs, user info, and the like


*/

var path = require('path');
var fs = require('fs');
var passwd_user = require('passwd-user');
var info = require('./bakedplatforminfo');
var _ = require('../util/uscoreenh');


exports.get_user_info = function (callback) {
    // This attempts to get user information for Linux, OSX, and
    // Windows, by trying a variety of sources (passwd and ENV)
    passwd_user(process.getuid(), function (err, info) {
        if (err) { throw new Error("user info error:" + err); }
        info = info || {}; // default value
        // try some attempts at guessing it based on ENV variables for windows
        var E = process.env;
        info.homedir = _.enh.firstof(info.homedir, E.HOMEPATH, E.USERPROFILE, E.HOME);
        info.username = _.enh.firstof(info.username, E.USERNAME, E.USER);
        info.bestname = _.enh.firstof(info.fullname,
                                    info.username,
                                    path.basename(info.homedir),
                                    '');

        // TODO: sniff if windows, and use _ instead of . as prefix
        // for hidden
        info.confighome = _.enh.firstof(E.XDG_CONFIG_HOME,
                                        path.join(info.homedir, '.config'));
        callback(info);
    });
};

var notexists = function (p, callback) {
    callback(false);
};

exports.get_conf_dir = function (callback) {
    /*
    1. Gets the user's conf directory in a platform-specific manner.
    2. Searches for the user's scrollrc file
    */
    exports.get_user_info(function (info) {

        // linux, use XDG
        var default_conf_dir = path.join(info.confighome, "scroll");
        var dir = process.env.SCROLL_HOME || default_conf_dir; 
        var rcpath0 = process.env.SCROLL_RCPATH || null;
        var rcpath1 = path.join(info.homedir, ".scrollrc"); 
        var rcpath2 = path.join(dir, "scrollrc"); 
        (rcpath0 ? fs.exists : notexists)(rcpath0, function (exists) {
            if (exists) { return callback(dir, info, rcpath0); }
            fs.exists(rcpath1, function (exists) {
                if (exists) { return callback(dir, info, rcpath1); }
                // fall back on rcpath 2
                callback(dir, info, rcpath2);
            });
        });
    });
};

/*
{
username: 'sindresorhus',
password: '*',
uid: 501,
gid: 20,
fullname: 'Sindre Sorhus',
homedir: '/home/sindresorhus',
shell: '/bin/zsh'
}
*/

