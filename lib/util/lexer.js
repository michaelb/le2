var TEXT_NODE = 1;
var TAG_NODE  = 2;

var escape_for_regex = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

// Streaming tokenizer based on tags and state
var RuleSet = function () {
    this.states = {};
    this.state_edges = {};
    this.regexps = null;
};

RuleSet.prototype.compile = function () {
    for (var state_name in this.states) {
        var patterns = [];
        this.states[state_name].forEach(function (token_name) {
            patterns.push(escape_for_regexp(token_name));
        });

        // Sort patterns by length, so that longer is first
        // (prevents lexing "****" as "*", "*", "*", "*", not "**", "**")
        patterns.sort(function (a, b) { return b.length - a.length; });

        // create the regex for this state context
        var exp = '(' + patterns.join('|') + ')';
        var regexp = new RegExp(exp);
        this.regexps[state_name] = regexp;
    }
};

RuleSet.prototype.add = function (token, state, new_state) {
    this.regexps = null;
    if (!this.states[state]) { this.states[state] = []; }
    if (!this.state_edges[state]) { this.state_edges[state] = {}; }

    this.states[state].push(token);
    this.state_edges[state][token] = new_state;
};

/*
RuleSet.prototype.add_tag = function (tag_name, enter, exit, within_states, payload) {
    // add to parent states
    for (var i in within_states) {
        var parent_state = within_state[i];
        this.add(enter, parent_state, "tag_"+tag_name);
    }

    this.add(exit, "tag_"+tag_name, null); // null designates state pop
};
*/

RuleSet.prototype.new_lexer = function (state, func) {
    if (this.regexps === null) {
        this.compile();
    }

    return new Lexer(this, func, state);
};


var Lexer = function (ruleset, emit, default_state) {
    this.ruleset = ruleset;
    this.emit = emit;
    this.default_state = default_state;
    this.stack = [];
    this.TEXT_NODE = TEXT_NODE;
    this.TAG_NODE  = TAG_NODE;
};


/*
Feed text into the lexer
*/
Lexer.prototype.write = function (text) {
    var length = this.stack.length;
    var state = length > 0 ? this.stack[length-1] : this.default_state;
    var regexp = this.ruleset.regexps[state];

    var match = text.match(regex);

    if (match === -1) {
        // entirely text match
        this.emit(this.TEXT_NODE, text);
        return;
    }

    // Found a match, figure out what we matched
    var index = match.index;
    var token = match[0];

    // Split text based on token
    var initial_text = text.substring(0, index);
    var remaining_text = text.substring(index + token.length, text.length);

    // Emit the prefix text, and the token
    this.emit(this.TEXT_NODE, initial_text);
    this.emit(this.TAG_NODE, token);

    // Perform state transition
    var next_state = this.ruleset.states[state_name];
    if (next_state === null) {
        this.stack.pop();
    } else {
        this.stack.push(next_state);
    }

    // Finally, recurse on the remaining text
    this.write(remaining_text);
};

exports.TAG_NODE = TAG_NODE;
exports.TEXT_NODE = TEXT_NODE;

