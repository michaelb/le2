// Small utility functions, attached to underscore
var _ = require('underscore');
_.str = require('underscore.string');

_.enh = {};

_.enh.truthy = function (val) {
    // weaker definition of truth, a la Steven Colbert, RIP
    if (!val) { return false; }
    if (val === NaN) { return false; }
    if (_.isString(val) || _.isArray(val)) {
        return val.length > 0;
    }
    if (_.isObject(val)) {
        return _.keys(val).length > 0;
    }
    return true;
};

_.enh.falsy = function (val) { return !_.enh.truthy(val); };

_.enh.firstof = function () {
    var arr = Array.prototype.slice.call(arguments), val;
    do {} while (_.enh.falsy(val = arr.shift()));
    return val;
};


module.exports = _;
