var toposort = require('toposort');

/*
 * Sort items based on "provides" and "depends" lists. E.g. objects like this
 * can get sorted into proper initialization order:
 *    [ { name: 'jquery', depends: null, provides: ["jquery"] },
 *      { name: 'jqplug0', depends: ["jquery"], provides: ["anotherthing"] },
 *      { name: 'jqplug1', depends: ["jquery", "anotherthing"], provides: null }, ]
 */
module.exports.sort_info_list = function (info_list) {
    // First pass, generate IDs for all items
    var ephemeral_plugin_id = 100;
    var by_id = {};

    info_list.forEach(function (item) {
        ephemeral_plugin_id++;
        by_id[ephemeral_plugin_id] = item;
        item.eid = ephemeral_plugin_id;
    });

    // Now loop through IDs and generate graph edges. "dependencies" are nodes,
    // "provides" are nodes, and finally "items" are nodes
    var graph_edges = [];
    var sorted_plugin_infos = [];
    info_list.forEach(function (item) {
        if (!item.provides && !item.depends) {
            // Add right away to "sorted", since it is disconnected from the graph
            sorted_plugin_infos.push(item);
            return;
        }

        // Add in "provides"
        (item.provides || []).forEach(function (provision) {
            graph_edges.push([provision, item.eid]);
        });

        // Add in "depends"
        (item.depends || []).forEach(function (depends) {
            graph_edges.push([item.eid, depends]);
        });
    });

    // Now, the graph edges have been constructed, run the toposort algorithm
    var sorted = toposort(graph_edges);
    sorted = sorted.reverse(); // since its in the opposite order

    // Finally, only the "number" type are the true nodes, loop through and
    // pull them out and put them into the sorted_plugin_infos
    sorted.forEach(function (item) {
        if (typeof item === "number") {
            var info = by_id[item];
            delete info.eid;
            sorted_plugin_infos.push(info);
        }
    });

    return sorted_plugin_infos;
};


/*
 * Removes dupes from a list of items, based on "src"
 */
module.exports.remove_dupes = function (file_list) {
    var result = [];
    var seen = {};
    file_list.forEach(function (item) {
        if (!seen[item.src]) {
            seen[item.src] = true;
            result.push(item);
        }
    });
    return result;
};
