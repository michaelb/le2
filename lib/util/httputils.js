var http = require('http'); // pull in http module

exports.get = function (uri, callback) {
    var opts = {};
    var path_suffix = uri.slice('http://'.length);
    var _host = path_suffix.split('/')[0];
    opts.port = _host.split(':')[1] || 80;
    opts.host = _host.split(':')[0];
    http.request(opts, function (response) {
        var result = []

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            result.push(chunk + '');
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            callback(result.join(''));
        });
    }).end();
}

