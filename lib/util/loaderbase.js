var _ = require('underscore');
_.str = require('underscore.string');
var path       = require('path');
var fs         = require('fs');
var schemaconf  = require('schemaconf');
var fsutils    = require('../util/fsutils');


var LoaderBase = function (opts) {//groups) {
    // {"paths": ["obj.cfg"], "namespace": "base"}
    // What to load
    this.opts = _.extend({}, opts);
    this.Class = this.opts.Class;
    this.groups = this.opts.groups;
    this.schema = new schemaconf.ConfSchema(
                this.opts.schema, {"no_exceptions": true});
    this.by_namespace = {};
    this.list = [];
    this.paths_list = [];
    this.paths = {};
    this[this.opts.plural_name] = this.by_namespace;
    this["all_" + this.opts.plural_name] = this.list;
};

LoaderBase.prototype.load = function (callback) {
    var this_ = this;

    /* ***************************************
     * Step 1, load and parse all objs
     * *************************************** */
    var start_load = function (groups) {
        // Prepare next step to be called after we're done
        var next_step = _.after(groups.length, finish_load);

        // Now, load all objs, plural objs specifications
        for (var i in groups) {
            var group = groups[i];
            var sub_next_step = _.after(group.paths.length, next_step);

            for (var i in group.paths) {
                var path = group.paths[i];
                this_.load_obj(group.namespace, path, sub_next_step);
            }
        }
    };

    /* ***************************************
     * Step 2, set up containment and post-tag processing
     * *************************************** */
    var finish_load = function () {
        if (this_.opts.finish_load) {
            this_.opts.finish_load(callback);
        } else {
            callback();
        }
    };

    if (this.groups.length < 1) { finish_load(); }
    else { start_load(this.groups); }
};

/* *********************
 * Loads a single obj from the resource path specified
 */
LoaderBase.prototype.load_obj = function (namespace, fullpath, callback) {
    var name = path.basename(fullpath).split('.')[0];
    fs.readFile(fullpath, _.bind(function (err, data_bytes) {
        if (err) { throw fullpath + " LoaderBase error: " + err; }

        // Parse it, validate it, and apply defaults
        var data_parsed = schemaconf.parse(data_bytes.toString());
        var data_validated = this.schema.validate(data_parsed);
        this.schema.apply_defaults(data_validated);

        // Create new obj based on validated data
        var obj = new this.Class(namespace, name, data_validated);

        if (!this.by_namespace[namespace]) {
            this.by_namespace[namespace] = {};
            this.paths[namespace] = {};
        }
        if (!obj[this.opts.compile_method]) {
            throw "LoaderBase not found " + this.opts.compile_method;
        }
        var compile = _.bind(obj[this.opts.compile_method], obj);

        compile(_.bind(function () {
            // obj is prepared, add to our list and return
            this.by_namespace[namespace][name] = obj;
            this.list.push(obj);
            this.paths[namespace][name] = fullpath;
            this.paths_list.push(fullpath);
            callback();
        }, this));
    }, this));
};

LoaderBase.prototype.copy_to = function (namespace, name, target_dir, callback) {
    var source_path = this.paths[namespace][name];
    var dest_path = path.join(target_dir, name + ".cfg");
    fsutils.defaultfilecopy(dest_path, source_path, function (err, was_copied) {
        callback(err, was_copied);
    });
};

module.exports = LoaderBase;

