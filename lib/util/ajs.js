/*
 * #########################################
 * AJS --- Advanced JavaScript Tk. Some handy classes 
  (copied from another project)
 * #########################################
 */


var ajs = {'data':{}};

ajs.inherits = function (klass, parent) {
    var d = {}, p = (klass.prototype = new parent());
    klass.method('uber', function uber(name) {
        if (!(name in d)) {
            d[name] = 0;
        }
        var f, r, t = d[name], v = parent.prototype;
        if (t) {
            while (t) {
                v = v.constructor.prototype;
                t -= 1;
            }
            f = v[name];
        } else {
            f = p[name];
            if (f == klass[name]) {
                f = v[name];
            }
        }
        d[name] += 1;
        r = f.apply(klass, Array.prototype.slice.apply(arguments, [1]));
        d[name] -= 1;
        return r;
    });
    return klass;
};



ajs.Set = function (list) {
    this.dict = {};
    for (var i in list) {
        this.dict[list[i]] = true;
    }
};


ajs.Set.prototype.iter = function () {
    return this.dict;
};

ajs.Set.prototype.add = function (item) {
    this.dict[item] = true;
};

ajs.Set.prototype.repr = function (item) {
    var list = "";
    for(var k in this.dict) list += k + ", ";

    return "ajs.Set(["+ list +"])";
};

ajs.Set.prototype.remove = function (item) {
    delete this.dict[item];
};

ajs.Set.prototype.contains = function (item) {
    return typeof this.dict[item] !== 'undefined';
};


ajs.Set.prototype.union = function (set) {
    var dict = {};
    for (var item in this.dict) {
        dict[item] = true;
    }

    for (var item in set.dict) {
        dict[item] = true;
    }

    var keys = [];
    for (var k in dict) keys.push(k);

    return new ajs.Set(keys);
};

ajs.Set.prototype.difference = function (set) {
    var dict = {};
    for (var item in this.dict) {
        dict[item] = true;
    }
    for (var item in set.dict) {
        delete dict[item];
    }

    var keys = [];
    for(var k in dict) keys.push(k);

    return new ajs.Set(keys);
};

ajs.Set.prototype.join = function (sep, prefix) {
    var s = '';
    var is_first = true;
    if (!prefix) prefix = '';
    for (var item in this.dict) {
        if (is_first) {
            s += prefix+item;
        } else {
            s += sep + prefix+item;
        }
        is_first = false;
    }
    return s;
};


ajs.data.ChunkIter = function (array, chunk_size, a, c) {
    this.pointer = 0;
    this.array = array;
    this.chunk_size = chunk_size;
    if (this.array.size) {
        // jQuery-esque
        this.length = this.array.size();
    } else {
        // Normal array
        this.length = this.array.length;
    }

    this.aggregate = a ? a : false;
    this.cut = c ? c : false;
    // An extremely large number, should never be reached, just in case
    // something goes wrong so their browser doesn't get hung;
    this.failsafe = 10000000;
};

ajs.data.ChunkIter.prototype.next = function (peak) {
    // The aggregating mode
    if (!this.has_next()) { return []; }
    if (this.aggregate) {
        var chunk = [];
        var original = this.pointer;
        while (chunk.length < this.chunk_size && this.has_next()) {
            var group = [];
            while (this.has_next()) {
                var elem = this.array.slice(this.pointer, this.pointer+1);
                this.pointer++;
                if (this.cut(elem)) break;
                group.push(elem);
            };
            if (group.length > 0) {
                chunk.push(this.aggregate(group));
            }
        }

        if (peak) {
            this.pointer = original;
        }

        return chunk;

    // The normal mode
    } else {
        var start = this.pointer;
        var end = start + this.chunk_size;

        if (!peak) {
            this.pointer = Math.min(end, this.length);
        }

        return this.array.slice(start, this.pointer);
    }
};

ajs.data.ChunkIter.prototype.progress = function () {
    return this.pointer / this.length;
};

ajs.data.ChunkIter.prototype.has_next = function () {
    return this.pointer < this.length && this.failsafe--;
};

ajs.Str = function (val) {
    this.prototype = (new String(val)).prototype;
};

ajs.Str.prototype.format = function (values) {
    // BORKEN
    var result = new String(this);
    // Add support for values

    for (var key in values) {
        // Add support for %i and %s
        // ALSO, prevent recursive replace, this is an incorrect
        // algorithm right now as written
        result = result.replace('%('+key+')s', values[key]);
    }
    return new ajs.Str(result);
};

ajs.typing = {
    is_array: function (object) {
        // Naive duck-typing for array
        return object != null && typeof object === "object" &&
            'splice' in object && 'join' in object;
    }
};



// This iterator loops through lines.
ajs.data.StringChunkIter = function (text, chunk_size, delim, opts) {
    this.text_remaining = text;
    this.original_text = text;
    this.length = this.original_text.length;
    this.pointer = 0;
    this.failsafe = 100000000;
    this.delim = delim;
    if (opts && opts.split) {
        // Split by newline instead of consume directly 
        // (not sure which is faster / less intense)
        this.split_text = this.original_text.split(this.delim);
    } else {
        this.split_text = null;
    }
};

ajs.data.StringChunkIter.prototype.next = function (peak) {
    // Depth first search
    if (!this.has_next()) { return []; }

    var chunk = [];
    var original = this.pointer;
    if (this.split_text) {
        // We already split by newline, just slice
        chunk = this.split_text.slice(this.pointer, 
            this.pointer+this.chunk_size);
        this.pointer += this.chunksize;
    } else {

        while (chunk.length < this.chunk_size && this.has_next()) {
            var next = this.text_remaining.indexOf(this.delim);
            // TODO not fiinished
            chunk.push(this.text_remaining.slice(0, next));
            chunk_length++;
            this.pointer += next;
        };
    }

    if (peak) {
        // If we're just peaking, rewind
        this.pointer = original;
    }

    return chunk;
};

ajs.data.StringChunkIter.prototype.has_next = function () {
    return this.pointer < this.length && this.failsafe--;
};

ajs.data.StringChunkIter.prototype.progress = function () {
    return this.pointer / this.length;
};

ajs.util = {};

ajs.util.MarkupLexer = function(patterns){
    var exp = '(';
    if (patterns.length < 1) {
        throw "MarkupLexer: patterns not defined.";
    }
    for (var i in patterns) {
        var val = patterns[i];
        if (val.length < 1) {
            console.error("Blank pattern.");
            return;
        }
        exp = exp + (i==0?'':'|') + val;
    }
    var regex = new RegExp(exp + ')', 'i');

    var Instance = function(text){
        // Is also an iter! :)
        this.text = text;
        this.last_token = '';
        this.length = text.length;
        this.DEBUG_MAX = 10000000000;
        this.next = function() {
            var index = this.text.search(regex);
            if (index != -1) {
                this.last_token = this.text.match(regex)[0];
                var result = this.text.substring(0, index);
                this.text = this.text.substring(
                    index+this.last_token.length, this.text.length);
            } else {
                var result = this.text;
                this.text = '';
                this.last_token = '';
            }
            return result;
        };
        this.has_text_left = function() {
            if (this.DEBUG_MAX-- < 0) {
                throw "REACHED DEBUG MAX";
                return false;
            }
            return this.text !== '';
        };
        this.has_next = function () {
            return this.has_text_left();
        };
        this.progress = function () {
            return this.text.length / this.length;
        };
    };

    this.TEXT_NODE = 1;
    this.lex = function(text) {
        var p = new Instance(text);
        var lexed = [];
        while (p.has_text_left()) {
            var txt = p.next();
            if (txt) {
                lexed.push([this.TEXT_NODE, txt]);
            }
            if (p.last_token) {
                lexed.push([p.last_token, p.last_token]);
            }
        };
        return lexed;
    };
};


module.exports = ajs;

