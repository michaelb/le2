var EventEmitter = require('events').EventEmitter;
var util = require('util');
var path = require('path');
var fs   = require('fs');

var TreeParser = require('../../lib/markup/parser').TreeParser;
var Structure  = require('../../lib/markup/structure');
var _renderer  = require('../../lib/markup/renderer'),
    EditorRenderer = _renderer.EditorRenderer,
    StyleRenderer = _renderer.StyleRenderer;
var _style      = require('../../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;

var LoaderBase  = require('../util/loaderbase');
var _ = require('underscore');
_.str = require('underscore.string');

var SCHEMA = require('./schemas').exportprofile;

var ExportProfile = function (namespace, name, validated_info) {
    this.namespace = namespace;
    this.name = name;
    this.info = validated_info;
};

util.inherits(ExportProfile, EventEmitter);

ExportProfile.prototype.prepare = function (callback) {
    this.on('file', this.handle_file);
    callback();
};


var _get_rel_path = function (workspace, in_path) {
    var path_suffix;
    if (in_path.indexOf(workspace.working_dir) === 0) {
        // prefixed with working directory
        path_suffix = path.relative(workspace.working_dir, in_path);
    } else if (in_path.charAt(0) !== '/') {
        // relative to working dir
        path_suffix = in_path;
    } else {
        throw new Error("Invalid asset path, abs specified not " +
                        "in working dir: " + in_path);
    }

    return path_suffix;
};

var _replace_extension = function (full_path, extension) {
    var split_at = full_path.length;

    var base_name = path.basename(full_path);
    var r_index = base_name.lastIndexOf('.');
    if (r_index !== -1) { // has an extension, adjust the split location
        split_at -= (base_name.length - r_index);
    }

    // split and add new extension
    return full_path.slice(0, split_at) + '.' + extension;
};

var _get_empty_style = function (callback) {
    var empty_style = new Style('fake', 'empty', { template: [] });
    var empty_structure = new Structure({structure: {ordering: {}, hierarchy: {}, classes: {}}});
    empty_style.compile(function () {
        empty_structure.prepare(function () {
            callback(empty_style, empty_structure);
        });
    });
};

ExportProfile.prototype.get_parser_and_renderer = function (asset, callback) {
    var tagloader = this.workspace.tagloader;

    var renderer_opts = { target: "default" };
    var get_style = _get_empty_style;
    // todo: based on logic, have a different getter

    var parser, renderer;
    get_style(function (style, structure) {
        var parser = new TreeParser(tagloader, structure);
        var renderer = new StyleRenderer(tagloader, style, renderer_opts);
        var done = _.after(2, function () { callback(parser, renderer); });
        parser.compile(done);
        renderer.compile(done);
    });
};

ExportProfile.prototype.handle_file = function (workspace, asset) {
    this.workspace = workspace; // there can only be one workspace per export profile

    // Do logic based on full_in_path
    var full_in_path = asset.path;

    // First get relative path
    var rel_sub_path = _get_rel_path(workspace, full_in_path);

    if (full_in_path.match(/\.md$/i)) {
        // by default, render markdown files
        var dest_path = _replace_extension(rel_sub_path, 'html');
        this.render_file(full_in_path, _.bind(function (data) {
            this.emit('write', dest_path, data);
            this.emit('done', asset);
        }, this));
    } else if (false) {
        // EXAMPLE: 
        make_some_cool_thumbnails(source, dest);
        this.emit('move', thumb_1, dest_thumb_1);
        this.emit('move', thumb_2, dest_thumb_1);
        this.emit('done', asset);
        // etc
    } else {
        // copy, keep extension
        this.emit('copy', rel_sub_path, full_in_path);
        this.emit('done', asset);
    }
};


ExportProfile.prototype.render_file = function (full_in_path, callback) {
    // First, gets the renderer
    this.get_parser_and_renderer(full_in_path, function (parser, renderer) {
        // Then, reads in the file
        fs.readFile(full_in_path, function (err, data) {
            if (err) { throw new Error("Could not read doc: " + err); }
            // Finally, parses and renders the file to a string
            renderer.render_to_string(data.toString(), parser, callback);
        });
    });
};


var ExportProfileLoader = function (export_groups) {
    ExportProfileBase.call(this, {
        groups: export_groups,
        Class: ExportProfile,
        schema: SCHEMA,
        plural_name: 'exportprofiles',
        compile_method: 'prepare',
        /*
        finish_load: function (callback) {
            // Step 2, set up containment and post-tag processing
            Tag.prepare_containment(this_.all_tags, callback);
        },
        */
    });
};

ExportProfileLoader.prototype = new LoaderBase;


module.exports.ExportProfileLoader = ExportProfileLoader;
module.exports.ExportProfile = ExportProfile;
