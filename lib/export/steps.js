/*
Built-in Export Step type functions
*/

var StepData = function (path, data) {
    this.path = path || null;
    this.data = data || null;
};

StepData.prototype.get_data = function (callback) {
    if (this.data) {
        callback(this.data);
    } else {
        fs.readFile(this.path, function (err, data) {
            if (err) { throw "StepData err (in " + this.path + "): " + err; }
            callback(data + '');
        });
    }
};


exports.render = function (workspace, stepinfo, source, callback) {
    // Unstructured rendering of input
    source.get_data(function (data) {
        new StepData(null, result);
    });
};

exports.stylerender = function () {
};

exports.convert = function () {
};

