var Types = require('schemaconf').Types;

module.exports.exportprofile = {
    "profile": {
        singular: true,
        required: true,
        values: {
            name: true,
            description: false,
        },
    },

    "rule": {
        singular: false,
        required: false,
        values: {
            asset: {
                // wildcard match for asset
                type: Types.wordlist,
                required: true,
            },
            description: false,
            action: {
                required: false,
                type: Types.choice,
                choices: "render stylerender convert",
            },
        },
    },

    /*
    // this is how we code to different steps
    "step": {
        singular: false,
        required: true,
        values: {
            name: true,
            title: true,
            input: true,
            type: {
                required: true,
                type: Types.choice,
                choices: "render stylerender convert",
            },
            args: false,
            pipeline: {
                required: false,
                default: "document",
            },
            description: false,
            style_target: false,
            target: {
                required: true,
                type: Types.choice,
                choices: "primary nonprimary all",
            },
            filter: false,
            skip_if: false,
        },
    },
    */
};
//module.exports.exportprofile.settings = plugin_settings_schema.settings;
//module.exports.exportprofile.setting  = plugin_settings_schema.setting;

