var _ = require('underscore');
_.str = require('underscore.string');
var fsutils = require('../util/fsutils');
var fs      = require('fs');
var path    = require('path');
var ncp     = require('ncp').ncp;
var data_actions = require('ncp');
var render_cli   = require('../cli/render').main;
var Document     = require('../data/document');
var ScrollMarkdownParser = require('../markup/markdownparser').ScrollMarkdownParser;
var TreeParser = require('../../lib/markup/parser').TreeParser;
var Structure  = require('../../lib/markup/structure');
var _renderer  = require('../../lib/markup/renderer'),
    EditorRenderer = _renderer.EditorRenderer,
    StyleRenderer = _renderer.StyleRenderer;
var _style      = require('../../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;

var get_empty_style = function (callback) {
    var empty_style = new Style('fake', 'empty', { template: [] });
    var empty_structure = new Structure({structure: {ordering: {}, hierarchy: {}, classes: {}}});
    empty_style.compile(function () {
        empty_structure.prepare(function () {
            callback(empty_style, empty_structure);
        });
    });
};



var _mirror_paths = function (local_path) {
};

var _get_phantomjs = function (config, workspace, opts) {
    var phantomjs = config.get_phantomjs();

    if (workspace) {
        if (workspace.working_dir) {
            phantomjs.bake_opts({ cwd: workspace.working_dir, });
        } else {
            phantomjs.bake_opts({ cwd: workspace, });
        }
    }

    if (opts.silent) {
        phantomjs.bake_opts({ silent: true});
    }

    return phantomjs;
};

var _should_render = function (asset_info) {
    return !asset_info.path.match(/cfg$/i); // terrible rule
};

var _get_dest_path = function (workspace, destination, asset) {
    var path_suffix;
    if (asset.path.indexOf(workspace.working_dir) === 0) {
        // prefixed with working directory
        path_suffix = path.relative(workspace.working_dir, asset.path);
    } else if (asset.path.charAt(0) !== '/') {
        // relative to working dir
        path_suffix = asset.path
    } else {
        throw new Error("Invalid asset path, abs specified not " +
                        "in working dir: " + asset.path);
    }

    return path.join(destination, path_suffix);
};

exports.render_document = function (opts, callback) {
    opts = _.extend({
        tagloader: null,
        log: function () {},
        debug: function () {},
        asset: {},
        destination: false,
        use_editor_renderer: false,
    }, opts);


    // prep handle_result, source, destination
    var handle_result, source, destination;

    if (opts.stdin_getter) { source = '-';
    } else { source = opts.asset.path; }

    if (opts.destination) {
        destination = opts.destination;
        handle_result = function (data) {
            fs.writeFile(destination, data, function (err) {
                if (err) { throw new Error("Could not write: " + err); }
                callback();
            });
        };
    } else {
        handle_result = callback;
    }

    //var target = options.target || "editor";

    var do_stdin = function (parser, renderer) {
        opts.stdin_getter(function (text) {
            renderer.render_to_string(text, parser, function (result) {
                handle_result(result);
                //opts.cli.output(result);
            });
        });
    };

    var do_document = function (parser, renderer) {
        fs.readFile(source, function (err, data) {
            if (err) { throw new Error("Could not read doc: " + err); }
            renderer.render_to_string(data.toString(), parser, function (result) {
                handle_result(result);
                //helper.cli.output(result);
            });
        });
    };

    var next_action;
    var emit_opts = { emit_source: false };

    // check if we are rendering from stdin
    if (source === '-') {
        opts.debug("Getting via stdin...")
        next_action = do_stdin;
    } else {
        opts.debug("Getting via document...")
        next_action = do_document;
    }

    var next_ = function (parser, renderer) {
        var done = _.after(2, function () {
            next_action(parser, renderer); });
        parser.compile(done);
        renderer.compile(done);
    };

    if (opts.use_editor_renderer) {
        opts.debug("Using editor renderer")
        var parser = new ScrollMarkdownParser(opts.tagloader, emit_opts);
        var renderer = new EditorRenderer(opts.tagloader);
        next_(parser, renderer);
    } else {
        opts.debug("Using style renderer")
        var renderer_opts = {target: "default"};
        get_empty_style(function (style, structure) {
            opts.debug("Using generated empty style and structure " + style + " - " + structure)
            var parser = new TreeParser(opts.tagloader, structure);
            var renderer = new StyleRenderer(parser.tagloader, style, renderer_opts);
            next_(parser, renderer);
        });
    }
}

exports.render_asset = function (config, workspace, asset,
                                 destination, opts, callback) {
    if (asset.path.match(/\.md$/i)) {
        // markdown document, lets render
        var rel_p = asset.path.slice(0, asset.path.length-3) + ".html";
        var destination_path = path.join(path.dirname(destination), rel_p);
        var render_opts = _.extend({}, opts, {
            asset: asset,
            destination: destination_path,
            tagloader: workspace.tagloader,
        });
        exports.render_document(render_opts, callback);
    } else {
        fsutils.defaultfilecopy({
            source: asset.path,
            destination: destination,
            callback: callback,
            skipdirs: true,
        });
    }
};

exports.render_workspace = function (config, workspace, destination, opts, callback) {
    var render_all = function (assets, render_settings, cb) {
        // Renders all renderable assets (e.g. not ending with .cfg) in the
        // given list
        var renderable_assets = assets.filter(_should_render);
        var done = _.after(renderable_assets.length, cb);
        renderable_assets.forEach(function (asset_info) {
            var destpath = _get_dest_path(workspace, destination, asset_info);
            exports.render_asset(config, workspace,
                                asset_info, destpath,
                                opts, done);
        });
    };

    // Loop through every asset, render into mirror, copy over images
    workspace.get_all_assets(function (assets) {
        // First get settings asset
        //var settings = _.find(assets, {asset_type: 'settings'});

        // Now get out render options
        //var render_settings = settings[0] || null; // todo
        render_all(assets, null, callback);
    });
};


/*
exports.html = function (config, workspace, destination, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};

};

exports.web = function (config, workspace, destination, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};

};

exports.pdf = function (config, workspace, destination, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};
};

exports.ebook = function (config, workspace, destination, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};
};
*/


