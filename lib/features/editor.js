/*
 * Middleware for Editor UI, but runs in browser thread
 */
var ipc = require('ipc');
var cheerio = require('cheerio');
var markup = require('../markup/markdownparser');

var EditorUI = function (editor) {
    this.editor = editor;
    this.workspace = editor.workspace;
};


EditorUI.prototype.edit_mode_reply_data = function (arg) {
    var uuid = arg.uuid;
    var html_data = arg.html;
    var new_html = this.to_edit_mode(html_data);
    return {
        uuid: uuid,
        html: new_html,
        triggers: [
            { name: 'autogrow' },
            { name: 'autosplit' },
            { name: 'stylebar' },
        ],
    }
};

EditorUI.prototype.save_to_document = function (html_data, callback) {
    // Saves entire buffer, contained in data

    // Parse HTML and push data tags into a list
    var $ = cheerio.load(html_data);
    var data_list = [];
    $('bk').each(function () {
        data_list.push($(this).attr('data'));
    });

    // save, joining with block divider (xxx hardcoded markdown here)
    var data = data_list.join("\n\n");
    this.workspace.default_doc.write(data, callback);
};

EditorUI.prototype.to_edit_mode = function (html_data) {
    // todo: need to implement this using Renderer, for custom editors
    var $obj = cheerio.load(html_data)('bk');
    var $ = cheerio.load("<textarea></textarea>");
    var $ret = cheerio.load("<textarea></textarea>")('textarea');
    $('textarea').attr('class', $obj.attr('class'));
    $('textarea').text($obj.attr('data'));
    var result = $.html();
    return result;
};


EditorUI.prototype.to_normal_mode = function (cls, text_data, callback) {
    var doc_edit_info = this.workspace.default_doc.editor;
    doc_edit_info.renderer.render_to_string(
            text_data,
            doc_edit_info.parser,
            function (result) {
                callback(result);
            });
};

EditorUI.prototype.fill_editor = function (e, data) {
    var viewframe = {
        data: data,
        default_element: '<bk class="base_para" data=""></bk>',
    };

    e.sender.send("editor_ready_reply", viewframe)
};


EditorUI.prototype.hook_events = function () {
    var _this = this;
    ipc.on('edit', function (event, arg) {
        // Translate html data into other data
        event.sender.send("edit_reply",
            _this.edit_mode_reply_data(arg));
    });

    ipc.on('normal', function (event, opts) {
        _this.to_normal_mode(opts.cls, opts.data, function (new_html) {
            var data = { html: new_html };

            if (opts.switching) {
                var html_to_edit = null;

                if (opts.splitting) {
                    // pull out split HTML to enter into edit mode
                    var $ = cheerio.load(new_html);
                    var $edited_elem = $('bk').last();
                    html_to_edit = $.html($edited_elem);

                    // set up ID to indicate which gets focus
                    $edited_elem.attr('id', opts.id);
                    data.html = $.html();
                }

                // Switching directly to a new mode
                data.edit = _this.edit_mode_reply_data({
                    uuid: opts.uuid,
                    html: html_to_edit || opts.html
                });
            }

            // Simple switch from normal mode, translate HTML data into other data
            event.sender.send("normal_reply", data);
        });
    });


    ipc.on('quicksave', function (event, info) {
        //var chunk = info.chunk;
        var html_data = info.data;
        //console.log('quicksave!', html_data);
        _this.save_to_document(html_data, function () {
            event.sender.send("quicksave_reply");
        });
    });

    ipc.on('editor_ready', function (event, arg) {
        // Do initial loading now, and then send back
        _this.workspace.default_doc.read(function (data) {
            _this.to_normal_mode(null, data, function (edit_html) {
                _this.fill_editor(event, edit_html);
            });
        });
    });
};



var setup = function (editor) {
    var editor_ui = new EditorUI(editor);
    editor_ui.hook_events();
};

module.exports.setup = setup;
module.exports.run_once = true;

