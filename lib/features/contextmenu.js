
var Menu = require('menu');
var MenuItem = require('menu-item');
var BrowserWindow = require('browser-window');
var ipc = require('ipc');

var setup = function (editor) {
    // Visual mode menu (e.g. on selection)
    var v_menu = new Menu();
    v_menu.append(new MenuItem({ label: 'Copy', click: function() { console.log('VISUAL MODE COPY'); } }));
    v_menu.append(new MenuItem({ label: 'Cut', click: function() { console.log('VISUAL MODE CUT'); } }));

    // Normal mode menu (e.g. on nothing)
    var n_menu = new Menu();
    n_menu.append(new MenuItem({ label: 'Undo', click: function() { console.log('NORMAL MODE UNDO'); } }));
    n_menu.append(new MenuItem({ label: 'Redo', click: function() { console.log('NORMAL MODE REDO'); } }));
    n_menu.append(new MenuItem({ type: 'separator' }));
    n_menu.append(new MenuItem({ label: 'Paste', click: function() { console.log('NORMAL MODE PASTE'); } }));
    n_menu.append(new MenuItem({ type: 'separator' }));
    // some top level shortcut settings
    n_menu.append(new MenuItem({ label: 'Distraction-free mode', type: 'checkbox', checked: false }));
    n_menu.append(new MenuItem({ label: 'Full screen mode', type: 'checkbox', checked: false }));

    // Edit mode menu (e.g. while editing)
    var e_menu = new Menu();
    e_menu.append(new MenuItem({ label: 'Copy', click: function() { console.log('EDIT MODE COPY'); } }));
    e_menu.append(new MenuItem({ label: 'Cut', click: function() { console.log('EDIT MODE CUT'); } }));


    ipc.on('context_menu', function (event, info) {
        var window = BrowserWindow.getFocusedWindow();
        // pops up
        if (info.normal) {
            // normal mode
            n_menu.popup(window);
        } else if (info.edit) {
            e_menu.popup(window);
        } else if (info.visual) {
            v_menu.popup(window);
        }
    });
};

module.exports.setup = setup;
module.exports.run_once = true;

