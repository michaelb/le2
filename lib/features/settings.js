var _ = require('underscore');

var get_settings_context = function () {
};

// Handles pushing settings to and from front end

/*
// define settings
[settingsinfo]
namespace=core.ui.feed
header=Decor Settings

[>setting]
name=full_width
type=bool

LE2.publish('settings:full_width', function (value) {
    // Set value
});


*/

var types = {
    bool: function () {
    }
};

var setup = function (buff) {
    var ipc = require('ipc');
    ipc.on('open', function (event, template) {
    });

    ipc.on('ready', function (event, arg) {
        // Populates the files context obj, asynchronously, then sends back to client
        var context = { };
        event.sender.send("ready_reply", context)
    });
};

module.exports.setup = setup;
module.exports.run_once = true;

