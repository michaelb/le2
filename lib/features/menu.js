var _ = require('underscore');
var child_process = require('child_process');

var get_exe = function (args) {
    if (args[0].indexOf('electron') !== -1) {
        return args.slice(0, 2); // electron binary, ignore
    } else {
        return args.slice(0, 1); // single binary
    }
};

var run = function (my_args) {
    var exe_args = get_exe(process.argv);
    var cmd_all = exe_args.concat(my_args);
    var cmd = cmd_all[0];
    var cmd_args = cmd_all.slice(1);
    var opts = { detached: true, stdio: [ 'ignore', 'ignore', 'ignore' ] };
    //var opts = { detached: true };
    var child = child_process.spawn(cmd, cmd_args, opts);
    child.unref();
    process.exit(0); // perish

    child.stdout.on('data', function (data) {
        console.log('stdout: ' + data);
        if (data.indexOf("SPECIALCODE:ALLREADY") !== -1) {
            process.exit(0); // perish
        }
    });

    child.stderr.on('data', function (data) {
        console.log('stderr: ' + data);
    });

    child.on('close', function (code) {
        console.log('child process exited with code ' + code);
    });

};


var setup = function (buff) {
    var ipc = require('ipc');
    ipc.on('open', function (event, template) {
        console.log("RECEIVED OPEN!", template);
        // based on template
        run([template]);
    });

    ipc.on('load', function (event, opts) {
        // either recent, or from a dialog
    });

    ipc.on('delete', function (event, opts) {
    });

    ipc.on('menu_ready', function (event, arg) {
        // Populates the files context obj, asynchronously, then sends back to client
        var context = { };
        var done = _.after(2, function () {
            event.sender.send("menu_ready_reply", context)
        });

        buff.resources.get_all_template_metadata(function (template_list) {
            //context.template_confs = template_list;

            // Set up templates sorted by category
            var categories = [];
            var order = buff.get_setting('core.editor', 'template_categories');
            order.forEach(function (catname) {
                var catinfo = {
                    name: catname,
                    list: [],
                    is_empty: true,
                    color: {
                        simple: "indigo",
                        book: "cyan",
                        tech: "lime",
                    }[catname]
                };
                categories.push(catinfo);

                template_list.forEach(function (template_conf) {
                    var tcat = (template_conf.info[0] || {}).category
                    if (tcat === catname) {
                        var template = {
                            path: template_conf.path,
                            basename: template_conf.basename,
                            info: _.extend({}, template_conf.info[0]),
                        };

                        catinfo.list.push(template);
                        catinfo.is_empty = false;
                    }
                });
            });
            context.templates_by_category = categories;

            done();
        });

        buff.resources.get_all_recent_files(function (recent_list) {
            context.recent = recent_list;
            done();
        });
    });
};

module.exports.setup = setup;
module.exports.run_once = true;

