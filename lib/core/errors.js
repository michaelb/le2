

// simple exception system
var Error = function (message) {
    this.message = message;
    this.type = "Error";
};

Error.prototype.toString = function () {
    return this.type + ": " + this.message;
};


var FormatError = function (message) {
    this.message = message;
    this.type = "FormatError";
};

FormatError.prototype = new Error;


var IOError = function (message) {
    this.message = message;
    this.type = "IOError";
};

IOError.prototype = new Error;

module.exports.Error       = Error;
module.exports.FormatError = FormatError;
module.exports.IOError     = IOError;
