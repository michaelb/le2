var a = {
    para: {
        name: "Paragraph",
        // Icons idsplaying wrong
        //icon: "pilcrow",
        short_label: "&#182;",
        help: "A normal paragraph of text. This is the default: almost all text in your document should be contained in paragraphs."
    },
    list: {
        // Icons idsplaying wrong
        //icon: "pilcrow",
        short_label: "&#x2022;List",
        help: "A list of items. Can be an ordered list (1, 2, 3), or a un-ordered list (usually designated with bullet points)."
    },
    verse: {
        help: "Use verse to more precisely format songs or poems in your work."
    },
    blockquote: {
        short_label: '&ldquo; &rdquo;',
        name: 'Block quotation',
        help: "A block quotation  is a quotation in a written document, that is set off from the main text as a paragraph, or block of text, and typically distinguished visually using indentation and a different typeface. (This is in contrast to a setting it off with quotation marks in a run-in quote.) Block quotations are used for long quotations."
    },
    epistle: {
        name: 'Epistle (letter)',
        help: "An epistle, or letter, is used for precisely formatting a letter within a document."
    },
    story: {
        help: 'Writing a short story collection? Use this to designate an individual story for better results than a chapter.'
    },
    volume: {
        help: 'A volume, when printed, constitutes a single book. If you want to write a trilogy in one book, for example, use three volumes to designate the separate books.'
    },
    part: {
        help: 'Occasionally, books are divided into multiple parts, which in turn can be divided into multiple chapters'
    },
    dedication: {
        short_label: "&#10084;Dedication",
        help: 'A Dedication is the expression of friendly connection or thank by the author towards another person, or a group of persons. When published, it receives it\'s own page, and is displayed in center-aligned verse form. The dedication has its own place on the dedication page and is part of the front matter.'
    },
    foreword: {
        //icon: "text-dropcaps",
        help: 'Often, a foreword will tell of some interaction between the writer of the foreword and the story or the writer of the story. A foreword to later editions of a work often explains in what respects that edition differs from previous ones.'
    },
    preface: {
        //icon: "text-dropcaps",
        help: 'A preface generally covers the story of how the book came into being, or how the idea for the book was developed; this is often followed by thanks and acknowledgments to people who were helpful to the author during the time of writing.'
    },
    acknowledgment: {
        //icon: "text-dropcaps",
        help: 'Acknowledges those who contributed to the '+
            'creation of the book. This is different from a ' +
            'dedication, as it may consist of an arbitrarily large ' +
            'block of text like a chapter, whereas a dedication is ' +
            'only one verse-like text which is much shorter.'
    },
    introduction: {
        //icon: "text-dropcaps",
        help: 'A beginning section which states the purpose and goals of the following writing.'
    },
    prologue: {
        //icon: "text-dropcaps",
        help: 'A prologue is an opening to a story that establishes the setting and gives background details, often some earlier story that ties into the main one, and other miscellaneous information.'
    },
    chapter: {
        //icon: "text-dropcaps",
        help: 'A chapter is one of the main divisions of a piece of writing of relative length, such as a book of prose. Many novels of great length have chapters. Non-fiction books, especially those used for reference, almost always have chapters for ease of navigation. In these works, chapters are often subdivided into sections. The chapters of reference works are almost always listed in a table of contents. Novels sometimes use a table of contents, but not always.'
    },
    epilogue: {
        help: 'This piece of writing at the end of a work of literature or drama is usually used to bring closure to the work.'
    },
    authorsnote: {
        name: "Author's Note",
        help: "Author's Notes are front-matter written directly from the perspective the author, disclaiming,  excusing, or otherwise explaining some aspect of the work."
    },
    afterword: {
        help: 'An afterword generally covers the story of how the book came into being, or of how the idea for the book was developed.'
    },
    conclusion: {
        help: 'Most useful in non-fiction, a conclusion is backmatter that represents the conclusion or findings of an essay or report.'
    },
    postscript: {
        help: 'A postscript, abbreviated PS or P.S., is writing added after the main body text.'
    },
    appendix: {
        help: 'This supplemental addition to a given main work may correct errors, explain inconsistencies or otherwise detail or update the information found in the main work.'
    },
    glossary: {
        help: 'The glossary consists of a set of definitions of words of importance to the work. They are normally alphabetized. The entries may consist of places and characters, which is common for longer works of fiction.'
    },
    backmatter: {
        name: "Generic back-matter",
        help: "Use this element for a chapter-like section of back-matter that is not one of the already available back-matter types."
    },
    frontmatter: {
        name: "Generic front-matter",
        help: "Use this element for a chapter-like section of front-matter that is not one of the already available front-matter types."
    },
    bodymatter: {
        name: "Generic body-matter",
        help: "Use this element for a chapter-like section of body-matter that is not one of the already available body-matter types."
    },

    'line': {
        markup: "$\n",
        help: "A line, as in a line in a poem."
    },
    'emphasis': {
        keycode: "i",
        markup: "_$_",
        short_label: "<em>Em</em>",
        help: "Emphasized text, usually designated with italics."
    },
    'strong': {
        keycode: "b",
        markup: "*$*",
        short_label: "<strong>Str</strong>",
        help: "Strong text, usually designated with boldface."
    },
    'distinguish': {
        keycode: "u",
        markup: "u{$}",
        short_label: '<span style="text-decoration: underline">U</span>',
        help: "Distinguished text, usually designated with underline."
    },
    'dialogue': {
        keycode: "d",
        markup: [['"$"'], ["``$''", "precise", "Joanne said, ``He claimed, ``The enemy's gate is up!'' Don't believe him.'' She shrugged."]],
        short_label: '&ldquo; &rdquo;',
        help: "Dialogue, as in quoted exchanges between characters."
    },
    'internal_monologue': {
        markup: '==$==',
        short_label: '<em>Intern</em>',
        help: "An internal monologue is where a character's "+
            "thoughts are revealed almost like dialogue. "+
            "It is usually specified in fiction using "+
            "italic text."
    },
    'footnote': {
        markup: 'footnote{$}',
        keycode: "m",
        short_label: 'Ft<sup style="text-decoration: underline;  position: relative; top: -0.5em; font-size: 80%; ">3</sup>',
        help: "Footnotes are notes at the foot of the page.  The note can provide an author's comments on the main text or citations of a reference work in support of the text, or both. A footnote is normally flagged by a superscripted number immediately following that portion of the text the note is in reference to."
    },
    'endnote': {
        markup: 'endnote{$}',
        keycode: "l",
        short_label: 'E<span style="font-size: 80%; ">[5]</span>',
        help: "Endnotes are collected under a separate heading at the end of a chapter in a book or a document. Unlike footnotes, endnotes have the advantage of not affecting the image of the main text, but may cause inconvenience to readers who have to move back and forth between the main text and the endnotes."
    },
    'mistake': {
        markup: '~~$~~',
        short_label: '<span style="text-decoration: line-through">Mis<span>',
        help: "Text that is a mistake, usually designated with a strike through the center of the text (ie, stricken out)."
    }
};

var result = {};

for (var i in a) {
    result['base_' + i] = a[i];
};

module.exports = result;

