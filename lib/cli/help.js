
var _ = require('underscore');
_.str = require('underscore.string');

module.exports.main = function (args, options, helper, ACTIONS_HELP) {
    if (args.length < 1) {
        _(ACTIONS_HELP).each(function (help_msg, action_name) {
            var s = _.str.pad(action_name, 10, " ", "right");
            helper.out(helper.yellow(s));
            helper.out(help_msg + "\n");
        });
    } else {
        _(args).each(function (arg, i) {
            try {
                var command_info = require("./" + arg);
            } catch (err) {
                helper.cli.status(arg + " command does not exist", "error");
                return;
            }

            helper.out(helper.yellow(arg) + "\n");
            if (command_info.help) {
                command_info.help(helper);
            } else {
                helper.out(ACTIONS_HELP[arg] + "\n");
            }
        });
    }
};

module.exports.help = function (helper) {
    helper.usage('help', {
        arg: "CMD",
        intro: [
            "The help command can give in-depth help",
            "on a particular command, or a summary of",
            "all commands.",
        ].join(' '),
        examples: [
            ['', "Gets a summary of the functions of all commands"],
            ['init', "Gets the help on the 'init' command"],
        ],
    });
};

