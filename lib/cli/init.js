var actions = require('../data/actions');

module.exports.main = function (args, options, helper) {
    // Inits in current dire
    helper.log("Initializing", options.worktree);
    var worktree = options.worktree;
    var error = function () {
        helper.cli.fatal("File already exists!");
    };

    var completed = function () {
        helper.log("Directory is ready!");
        helper.callback();
    };
    actions.init(worktree, completed, error);
};


