var actions = require('../data/actions');

module.exports.main = function (args, options, helper) {
    // Lists all items in the stash
    var type = args.shift() || false;
    var error = function () {
        helper.cli.fatal("File already exists!");
    };

    var completed = function (data) {
        for (var key in data) {
            if (type && key !== type) { continue; }
            helper.out(helper.yellow(key));
            helper.out("\n");

            var namespaces = data[key];
            for (var namespace in namespaces) {
                helper.out(namespace + ": ");

                var items = namespaces[namespace];
                helper.out(items.join(" "));
                helper.out("\n");
            }
            helper.out("\n");
        }
        helper.callback();
    };

    helper.get_config(function (config) {
        actions.stash_list(config, completed, error);
    });
};


