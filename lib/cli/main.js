var CLIHelper = require("./helper");
var _ = require('underscore');
_.str = require('underscore.string');

var OPTS = {
    verbose:   ['v', 'Enable verbose logging to stdout'],
    worktree:  ['w', 'Work from the given path', 'path', '.'],
    namespace: ['n', 'Use given namespace for stash operation', 'namespace'],
    //file:      ['f', 'Load from the given scroll file', 'file', '.'],
    yes:       ['yes', 'Answer all yes/no questions with yes'],
    renderer:  ['renderer', 'Which renderer to use for the document', ["style", "editor"], 'style'],
    config:    ['config', 'Specify a specific config directory', 'path'],
};

var ACTIONS_HELP = {
    'new':     'new from template',
    init:      'initializes bare skeleton for scroll workspace',
    stashlist: 'lists all items in stash',
    stashload: 'loads a tag from stash',
    //stashsave: 'save a tag to stash',
    //export:    'full export to particular target, e.g. "publish"',
    render:    'render to particular target (e.g. render HTML, no packaging)',
    //find:      'find elements based on criteria',
    //config:    'save or check config properties (local or global)',
    //gitpull:   'clones or updates a git-controlled scroll into your home',
    //gitpush:   'pushes a git-controlled scroll back to a source',
    //gitcommit: 'auto-generates a commit for a git-controlled scroll (possibly branching)',
    //gitundo:   'undos the last commit in a git-controlled scroll',
    //gitredo:   'redos the last commit in a git-controlled scroll',
    //gitpurge:  'deletes ALL history in a git-controlleds scroll',
    //install:   'install tag or plug-in from web!',
    help:      'list this brief description of commands',
    editor:    'launch graphical editor', // not really existing
};

var ACTIONS = _.keys(ACTIONS_HELP);

module.exports.main = function (cli, args, options) {
    // Check first for "editor" command to start graphical editor
    if (cli.command === "editor") {
        var editor_module = require("../gui/main");
        editor_module.main(cli, args, options);
        return;
    }


    try {
        var command = require("./" + cli.command);
    } catch (err) {
        cli.status(cli.command + " not found", "fatal");
        return;
    }

    var helper = new CLIHelper(cli, args, options, OPTS);

    if (command.opts) {
        helper.set_options(_.clone(command.opts));
    }

    if (cli.command === "help") {
        command.main(args, options, helper, ACTIONS_HELP);
        return;
    }

    try {
        command.main(args, options, helper);
    } catch (err) {
        cli.status(err, "fatal");
        return;
    }
};

module.exports.OPTS = OPTS;
module.exports.ACTIONS = ACTIONS;
