var actions = require('../data/actions');
var _ = require('underscore');
_.str = require('underscore.string');

module.exports.main = function (args, options, helper) {
    // First inits the worktree directory, then loads the given template
    var namespace = args.shift();
    var name      = args.shift();
    if (!namespace || !name) {
        helper.fatal("Specify template like this: [namespace] [name]")
    }

    helper.log("Initializing ",  options.worktree, "with template", namespace, name);
    var worktree = options.worktree;
    // First, do an init operation
    helper.get_config(function (config) {
        var opts = {
            error: function (msg) { helper.fatal(msg); },
            debug: _.bind(helper.debug, helper),
        };

        var completed = function () {
            helper.cli.status("Template is loaded", "ok");
        };

        actions.template_init(config, worktree, namespace, name, completed, opts);
    }, {});
};


