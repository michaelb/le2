var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var Document = require('../data/document');
var ScrollMarkdownParser = require('../markup/markdownparser').ScrollMarkdownParser;
var TreeParser = require('../../lib/markup/parser').TreeParser;
var Structure = require('../../lib/markup/structure');
var _renderer = require('../../lib/markup/renderer'),
    EditorRenderer = _renderer.EditorRenderer,
    StyleRenderer = _renderer.StyleRenderer;
var _style = require('../../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;

module.exports.opts = [
    'worktree',
    'renderer',
];

var get_empty = function (callback) {
    var empty_style = new Style('fake', 'empty', { template: [] });
    var empty_structure = new Structure({structure: {ordering: {}, hierarchy: {}, classes: {}}});
    empty_style.compile(function () {
        empty_structure.prepare(function () {
            callback(empty_style, empty_structure);
        });
    });
};


module.exports.main = function (args, options, helper) {

    // Inits in current dir
    var source = options.source || "-"; // stdin
    //var target = options.target || "editor";

    var do_stdin = function (parser, renderer) {
        helper.cli.withStdin(function (text) {
            renderer.render_to_string(text, parser, function (result) {
                helper.cli.output(result);
            });
        });
    };

    var do_document = function (parser, renderer) {
        var doc = workspace.documents[source];
        doc.read(function (text) {
            renderer.render_to_string(text, parser, function (result) {
                helper.cli.output(result);
            });
        });
    };

    helper.get_workspace(function (workspace) {
        var next_action;
        var emit_opts = { emit_source: false };

        // check if we are rendering from stdin
        if (source === '-') {
            helper.debug("Getting via stdin...")
            next_action = do_stdin;
        } else if (workspace.documents[source]) {
            helper.debug("Getting via document...")
            next_action = do_document;
        } else {
            helper.cli.status("No document " + source, "fatal");
        }

        var next_ = function (parser, renderer) {
            var done = _.after(2, function () {
                next_action(parser, renderer); });
            parser.compile(done);
            renderer.compile(done);
        };

        if (options.renderer === 'editor') {
            helper.debug("Using editor renderer")
            var parser = new ScrollMarkdownParser(workspace.tagloader, emit_opts);
            var renderer = new EditorRenderer(workspace.tagloader);
            next_(parser, renderer);
        } else {
            helper.debug("Using style renderer")
            var opts = {target: "default"};
            get_empty(function (style, structure) {
                helper.debug("Using generated empty style and structure " + style + " - " + structure)
                var parser = new TreeParser(workspace.tagloader, structure);
                var renderer = new StyleRenderer(parser.tagloader, style, opts);
                next_(parser, renderer);
            });
        }
    });
};


module.exports.help = function (helper) {
    helper.usage('help', {
        arg: "CMD",
        intro: [
            "Renders a file",
            "on a particular command, or a summary of",
            "all commands.",
        ].join(' '),
        examples: [
            ['< /path/to/file.md', "Renders a particular file somewhere"],
        ],
    });
};

