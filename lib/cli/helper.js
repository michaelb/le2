var actions = require('../data/actions');
var userid = require('../util/userid');
var inquirer = require('inquirer');
var fs = require('fs');
var _ = require('underscore');
_.str = require('underscore.string');

// BUG: options.no_color is never set

var CLIHelper = function (cli, args, options, OPTS, callback) {
    this.args = args;
    this.options = options;
    this.cli = cli;
    this.OPTS = OPTS;
    this.callback = callback || function () {};
    this.relevant_opts = null;

    /*process.on('uncaughtException', function (err) {
        console.trace();
        cli.status("Runtime Error: " + err, "fatal");
    });*/
};

CLIHelper.prototype.set_options = function (opts_list) {
    this.relevant_opts = {};
    opts_list.push('yes');
    opts_list.push('timeout');
    opts_list.push('verbose');
    opts_list.push('config');
    for (var i in opts_list) {
        var v = opts_list[i];
        this.relevant_opts[v] = this.OPTS[v];
    }
};

CLIHelper.prototype.yesno = function (msg, yes, no) {
    if (this.options.yes) {
        yes();
        return;
    }

    var p = {type: "confirm", name: "yesno", message: msg, default: true }
    inquirer.prompt([p], function (answers) {
        if (answers.yesno) { yes(); }
        else { no(); }
    });
};

CLIHelper.prototype.get_config = function (cb, opts) {
    var _this = this;

    userid.get_conf_dir(function (dir, rcpath, info) {
        // Specifying custom location for the config path
        if (_this.options.config) {
            var Config = require('../data/config');
            _this.config = new Config(opts || {});
            var dir = _this.options.config;
            _this.config.load(dir, info, function () {
                _this.cli.debug("Config dir: " + _this.config.dir);
                cb(_this.config);
            });
            return;
        }

        // Specifying default location for config path
        fs.exists(dir, function (exists) {
            if (!exists) {
                // doesn't exist, offer to create
                _this.yesno("First time run: Config directory not created. Create one?", function () {
                    // try again
                    actions.init_conf(function () {
                        _this.get_config(function (config) {
                                _this.cli.ok("Created!");
                                cb(config);
                            }, opts);
                    }, function (err) {
                        _this.cli.fatal("Could not create config directory:" + err);
                    });
                }, function () {
                    _this.cli.fatal("Aborting.");
                });
            } else {
                // exists, next step
                var Config = require('../data/config');
                _this.config = new Config(opts || {});
                _this.config.load(dir, info, function () {
                    _this.cli.debug("Config dir: " + _this.config.dir);
                    cb(_this.config);
                });
            }
        });
    });
};


CLIHelper.prototype.get_workspace = function (cb) {
    var Workspace = require('../data/workspace');
    this.workspace = new Workspace(this.options.worktree);
    var _this = this;
    this.workspace.prepare(function () {
        cb(_this.workspace);
    });
};

CLIHelper.prototype.out = function (s) {
    process.stdout.write(s);
};

CLIHelper.prototype.log = function () {
    if (this.options.verbose) {
        console.log.apply(this, arguments);
    }
};

CLIHelper.prototype.debug = function (msg) {
    this.cli.status(msg, "debug");
};

CLIHelper.prototype.fatal = function (msg) {
    if (this.options.verbose) {
        console.trace();
    }

    this.cli.status(msg, "fatal");
};

CLIHelper.prototype.yellow = function (text) {
    if (this.options.no_color) { return text; }
    return ['\x1B[33m', text, '\x1B[0m:'].join('');
}

CLIHelper.prototype.yellow = function (text) {
    if (this.options.no_color) { return text; }
    return ['\x1B[33m', text, '\x1B[0m:'].join('');
}

CLIHelper.prototype.usage = function (cmdname, opts) {
    var cli = this.cli;
    var short, desc, optional, line, seen_opts = [],
        switch_pad = cli.option_width;

    var trunc_desc = function (pref, desc, len) {
        var pref_len = pref.length,
            desc_len = cli.width - pref_len,
            truncated = '';
        if (desc.length <= desc_len) {
            return desc;
        }
        var desc_words = (desc+'').split(' '), chars = 0, word;
        while (desc_words.length) {
            truncated += (word = desc_words.shift()) + ' ';
            chars += word.length;
            if (desc_words.length && chars + desc_words[0].length > desc_len) {
                truncated += '\n' + pad(pref_len);
                chars = 0;
            }
        }
        return truncated;
    };

    var usage = this.cli.app + ' ' + cmdname + ' [OPTIONS] ' + (opts.arg && ' [' +opts.arg+ ']');
    if (this.options.no_color) {
        console.error('Usage:\n  ' + usage);
    } else {
        console.error('\x1b[1mUsage\x1b[0m:\n  ' + usage);
    }

    if (opts.intro) {
        console.error('\n\x1b[1mDescription\x1b[0m: ');
        console.error(opts.intro);
    }

    if (this.options.no_color) {
        console.error('Options: ');
    } else {
        console.error('\n\x1b[1mOptions\x1b[0m: ');
    }

    var opt_list = opts.opt_list || this.relevant_opts || [];

    for (var opt in opt_list) {

        if (opt.length === 1) {
            long = opt_list[opt][0];
            short = opt;
        } else {
            long = opt;
            short = opt_list[opt][0];
        }

        //Parse opt_list
        desc = opt_list[opt][1].trim();
        type = opt_list[opt].length >= 3 ? opt_list[opt][2] : null;
        optional = opt_list[opt].length === 4 ? opt_list[opt][3] : null;

        //Build usage line
        if (short === long) {
            if (short.length === 1) {
                line = '  -' + short;
            } else {
                line = '      --' + long;
            }
        } else if (short) {
            line = '  -' + short + ', --' + long;
        } else {
            line = '      --' + long;
        }
        line += ' ';

        if (type) {
            if (type instanceof Array) {
                desc += '. VALUE must be either [' + type.join('|') + ']';
                type = 'VALUE';
            }
            if (type === true || type === 1) {
                type = long.toUpperCase();
            }
            type = type.toUpperCase();
            if (type === 'FLOAT' || type === 'INT') {
                type = 'NUMBER';
            }
            line += optional ? '[' + type + ']' : type;
        }
        line = pad(line, switch_pad);
        line += trunc_desc(line, desc);
        line += optional ? ' (Default is ' + optional + ')' : '';
        console.error(line.replace('%s', '%\0s'));

        seen_opts.push(short);
        seen_opts.push(long);
    }

    if (opts.examples) {
        console.error('\n\x1b[1mExamples\x1b[0m:');
        for (var i in opts.examples) {
            var einfo = opts.examples[i];
            console.error([this.cli.app, cmdname, einfo[0]].join(' '));
            console.error("    " + einfo[1]);
        }
    }

    //return cli.exit(code);
};


module.exports = CLIHelper;



