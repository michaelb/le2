/* #########################################
 * license: AGPL3.0
 * author: Michael Bethencourt
 * 
 * Classes and functions that pertain to going to and from SembookMarkup
 * #########################################
 */

var _ = require('underscore');
_.str = require('underscore.string');
var cheerio = require('cheerio');
var ajs = require('../util/ajs');
//var markup_help = require('./markup_help');

var spec =  {
    /*
    help: function (tag) {
        return _.extend({
            name: _.str.humanize(tag),
            help: false,
            short_label: _.str.humanize(tag)
        }, markup_help[tag]);
    },
    */
    contains_iter: {
        'base_emphasis': true,
        'base_strong': true,
        'base_distinguish': true,
        'base_dialogue': true,
        //'base_internal_monologue': true,
        'base_mistake': true,
        'base_footnote': true,
        'base_endnote': true,
    }
};

module.exports.spec = spec;


// Tag represents a single markup tag.
var Tag = function(markup, tag_or_html, name) {
    if (markup.indexOf('$')!=-1) {
        var s_tag = markup.split('$');
        this.css = tag_or_html;
        this.open_tag = s_tag[0];
        this.close_tag = s_tag[1];
        this.open_html = '<in class="'+tag_or_html+'">';
        this.close_html = '</in>';
    } else {
        // Is a self-closing tag
        this.css = name;
        this.open_html = tag_or_html;
        this.open_tag = markup;
        this.close_tag = false;
        this.close_html = false;
    }
};

// TagSet is a collection of tags.
var escape_for_regex = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

var TagSet = function (tags, extend, name) {
    var tokens = [];
    this.original_tags = tags;
    this.autoopen = null;
    this.open_tags = {};
    this.close_tags = {};
    this.self_closing_tags = {};
    this.name = name;

    if (extend) {
        this.original_tags =
            tags.concat(extend.original_tags);
    }

    tags = this.original_tags;

    for (var i in tags) {
        var tag = tags[i];
        if (tag.open_tag.length > 0) {
            // Open tags should never conflict
            tokens.push(escape_for_regex(tag.open_tag));
            this.open_tags[tag.open_tag] = tag;
        } else {
            // autoopen tag, don't add to open_tags
            this.autoopen = tag;
        }

        // Close tags can conflict, hence the use of arrays for those
        if (tag.close_tag !== false) {
            tokens.push(escape_for_regex(tag.close_tag));
            if (!this.close_tags[tag.close_tag]) {
                this.close_tags[tag.close_tag] = [];
            }
            this.close_tags[tag.close_tag].push(tag);
        } else {
            this.self_closing_tags[tag.open_tag] = tag.open_html;
        }
    }
    this.all_tokens = tokens;

    // Here we add all the tokens from the tags we just specified into a
    // new markup lexer
    this.lexer = new ajs.util.MarkupLexer(this.all_tokens);

    this.tags = tags;

    this.$find = function ($e) {
        // Find a tag withing this set that matches the jQuery HTML
        // element given.
        for (var i in tags) {
            var tag = tags[i];
            var classes = tag.css.split(' ');
            var found = classes.length > 0;
            for (var j in classes) {
                found = found && $e.hasClass(classes[j]);
            }

            if (found) {
                return tag;
            }
        }
        return null;
    };

    this.$find_custom = function($e, func) {
        // Find a tag withing this set that matches the given func
        if ($e.length && $e[0].nodeType === 3) {
            return null;
        };
        for (var i in tags) {
            var tag = tags[i];
            var found = func(tag, $e);
            if (found) {
                return tag;
            }
        }
        return null;
    };

    this.self_closing_html_to_markup = function(text) {
    };
};

// TODO need to figure out how to gracefully add `` '' etc as buttons
//var markup_tags = new TagSet([
//    new Tag("``$''", "dialogue en")
//    ,
//    new Tag('"$"', "dialogue")
//    ,
//    new Tag('_$_', "emphasis")
//    ,
//    new Tag('*$*', "strong")
//    ,
//    new Tag('~~$~~', "mistake")
//    ,
//    new Tag('==$==', "internal_dialogue")
//    ,
//    new Tag('footnote{$}', "footnote")
//    ,
//    new Tag('endnote{$}', "endnote")
//    ,
//    new Tag('--', "&#8212;", "em-dash")
//]);
//
//// Base symbols
//var symbols = [
//    ['--', "&#8212;", "em-dash"],
//    ['...', "&#8230;", "ellipsis"],
//    ['\\dagger', "&#8224;", "dagger"],
//    ['\\double_dagger', "&#8225;", "double_dagger"],
//    ['->', "&#8594;", "right_arrow"],
//    ['<-', "&#8592;", "left_arrow"],
//    ["<~", "&#8629;", "carriage_return_arrow"],
//    ["<>", "&#9674;", "lozenge"],
//    ["\\bullet", "&#8226;", "bullet"],
//    ['(TM)', "&#153;", "trademark"],
//    ['(C)', "&#169;", "copyright"],
//    ['(R)', "&#174;", "registered_trademark"]
//];
//
//

var unicode_symbols = {
    // TODO add non-unicode symbols, such as 
    // (CC) and (cl) (backwards c), (PD)
    // Do this via images. That way you can easily embed copyleftist
    // typography.

    8212: ['--', "em-dash"],
    8230: ['...', "ellipsis"],
    8224: ['\\dagger', "dagger"],
    8225: ['\\double_dagger', "double_dagger"],

    // Math
    215: ["\\times", "times"],
    247: ["\\div", "obelus"],
    8756: ["\\therefore", "therefore"],

    // Misc
    176: ["\\degrees", "degrees"],

    // Ligatures
    198: ['\\lig_AE', "ligature_ae_upper"],
    230: ['\\lig_ae', "ligature_ae_lower"],
    64257: ['\\lig_fi', "ligature_fi"],
    64258: ['\\lig_fl', "ligature_fl"],

    8470: ['n_o_', "numero_sign"],

    8594: ['->', "right_arrow"],
    8592: ['<-', "left_arrow"],
    8629: ["<~", "carriage_return_arrow"],
    9674: ["<>", "lozenge"],
    8226: ["\\bullet", "bullet"],
    8482: ['(TM)', "trademark"],
    //: '(TM)',
    169: ['(C)', "copyright"],
    8480: ['(SM)', "servicemark"],
    // For some reason, 128045 turns into 55357 when being read...
    // ???
    128045: ['>(LR)<', "leebre_rat"],
    55357: '>(LR)<',
    174: ['(R)', "registered_trademark"]
};


var symbols;

var unicode_to_edit_symbols;

var symbols = (function (a) {
    var lst = [];
    for (var number in a) {
        // ones that are not arrays only go one way
        if (_.isArray(a[number])) {
            lst.push([a[number][0], '&#'+number+';', a[number][1]]);
        }
    }
    return lst;
})(unicode_symbols);

// For conversions to edit mode
var unicode_to_edit_symbols = (function (a) {
    var lst = {};
    for (var number in a) {
        var row = a[number];
        lst[number] = _.isArray(row) ? row[0] : row;
    }
    return lst;
})(unicode_symbols);

var make_tagset = function (tagname) {
    var args = [];
    var iter;

    if (_.isString(tagname)) {
        //iter = spec.hierarchy[tagname].contains.iter();
        iter = spec.contains_iter;
    } else {
        iter = tagname.iter();
    }

    for (var tag in iter) {
        var info = spec.help(tag);
        if (info.markup) {
            // Check if there are multiple markups
            if (_.isArray(info.markup)) {
                // HACK: Go through in reverse so that
                // smu_-style tags get preferences over
                // plain ones when converting back
                var i = info.markup.length;
                while (i--) {
                    var pair = info.markup[i],
                        markup = pair[0],
                        name = pair[1] || false;
                    var new_tag = tag;
                    if (name) {
                        new_tag += ' smu_'+name;
                    }
                    args.push([markup, new_tag]);
                }
            } else {
                args.push([info.markup, tag])
            }
        }
    }

    // I guess symbols go after?
    args = args.concat(_.clone(symbols));

    var tags = [];
    // Now build the return value
    for (var i in args) {
        var a = args[i];
        tags.push(new Tag(a[0], a[1], a[2] || null));
    }

    return new TagSet(tags);
};

var _tagset_cache = {};
var get_tagset = function (tagname) {
    if (!_tagset_cache[tagname]) {
        _tagset_cache[tagname] = make_tagset(tagname);
    }
    return _tagset_cache[tagname];
};


var parse_markup_to_html = function (text, tagname) {
    var tagset = get_tagset(tagname || 'para');
    var lexer = tagset.lexer;
    var lexed = lexer.lex(text);
    var html = '';
    var tagstack = [];

    var open = function(tag) {
        // Only push it onto the stack if its not a self-closing tag.
        if (tag.close_html !== false) {
            tagstack.push(tag);
        }
        return tag.open_html;
    };

    var close = function() {
        var tag = tagstack.pop();
        return tag.close_html;
    };

    if (tagset.autoopen) {
        html += open(tagset.autoopen);
    }

    var open_if_autoopen = function (closetag) {
        if (tagset.autoopen && closetag == tagset.autoopen.close_tag) {
            // Re-open right away for the autotag
            var opentag = tagset.close_tags[closetag][0];
            html += open(opentag);
        };
    };

    for (var i in lexed) {
        var t = lexed[i], type = t[0], value = t[1];
        var top_ = tagstack.length > 0 ? tagstack[tagstack.length-1] : false;

        if (type === lexer.TEXT_NODE) {
            // Just append the value if it is a text node
            html = html + value;
        } 
        // Reverse look up to get Tag based on type
        // bug: backslashes not in type

        // First check if closing the topmost open tag
        else if (top_ && top_.close_tag == type) {
            // PROPER CLOSING TAG
            html += close();
            open_if_autoopen(top_.close_tag);
        } else {

            // TODO: comb through tag stack, check if there is tag
            // currently opened that could use this as a closer.
            var closed = false;

            for (var i=tagstack.length-2, opentag=null; opentag = tagstack[i]; i--) {
                // Walk backwards through tagstack
                if (opentag.close_tag == type && opentag.open_tag == type) {
                    // FOUND a closing tag for this one
                    //console.log("Malformed closing tag.", i);
                    // For symmetric tags, just
                    // automatically close all tags up to
                    // and including this one.
                    var close_count = tagstack.length - i;
                    for (var j=0; j<close_count; j++) {
                        html += close();
                    }
                    closed = true;
                    break;
                }
            }

            if (!closed) {
                // No tag is being, could be a legit opening
                // tag, or just an extraneous character
                var opentag = tagset.open_tags[type];
                if (opentag) {
                    // A legit opening tag.
                    html += open(opentag);
                } else {
                    // Cannot be opening a tag, just an
                    // extraneous character.  Insert it
                    // escaped.
                    //html += "\\" + type;

                    // Actually, might be better to
                    // leave be... need to consider
                    // this:
                    html += type;
                }
            }
        }
    }

    // Close all open tags.
    for (var i in tagstack) {
        html += close();
    }
    return html;
};

var parse_html_to_markup = function(html, tagname) {
    // NOTE: naive, could feasibly screw up since it is sequential and not at once.
    // NOTE 2: ^--- not sure if this applies any more
    var tagset = get_tagset(tagname || 'para');
    var $ = $.load('<div></div>');
    var wrap_children = function($e) {
        var output = '';
        $e.contents().each(function() {
            if (this.nodeType == 3) {
                // Text node, apply pre-processing to transform
                // HTML entities back to their text shortcuts.
                output += markup_preprocess(this.nodeValue);
            } else {
                var tag = tagset.$find($(this));
                // TODO EXPAND into other sneaky characters
                if (tag) {
                    if (tag.open_tag == "\n") {
                        output += "\n";
                    } else {
                        output += tag.open_tag;
                    }
                }
                output += wrap_children($(this));
                if (tag) {
                    if (tag.close_tag == "\n") {
                        output += "\n";
                    } else {
                        output += tag.close_tag;
                    }
                }
            }
        });
        return output;
    };
    var $outer = $('div');
    $outer.html(html);
    //console.info("This is initial", html);
    return wrap_children($outer);
};
module.exports.parse_html_to_markup = parse_html_to_markup;


var html_to_markup = function(html, a, b, c) {
    return parse_html_to_markup(html, a, b, c);
};


// TODO need to condense this with symbols
var unicode = (function(a){
        var ret = [];
        for (var unicode in a) {
            var c = String.fromCharCode(unicode);
            ret.push([c, a[unicode]]);
        }
        return ret;
    })(_.extend({
        // Some default unicodes and their replacements
        // These defaults are especially useful for pasted in
        // text when we want to handle unicode from that.
        // TODO: add more here Such as from:
        // http://www.fileformat.info/info/unicode/category/Ps/list.htm
        // http://www.fileformat.info/info/unicode/category/Pe/list.htm
        // and http://www.fileformat.info/info/unicode/category/Po/list.htm
        8208: '-' // hyphen
        ,
        8209: '-' // non-breaking hyphen
        ,
        8210: '-' // figure dash
        ,
        8211: '-' // en-dash
        ,
        8212: '--' // em-dash
        ,
        8213: '--' // "horizontal bar"
        ,
        8220: '``' // TODO add more quotes
        ,
        8221: "''"
    }, unicode_to_edit_symbols));

var markup_preprocess = function(text) {
    // Converts unicode to markup equivalents
    for (var i in unicode) {
        var pair = unicode[i];
        text = text.replace(new RegExp(pair[0], 'g'), pair[1]);
    }
    return text;
    //// Code for learning charCodes
    //var s = _.str.chars(text)
    //for (var i in s) {
    //    var c = s[i];
    //    if (!c.match(/[\w\s\d"',><:._=?!-]/)) {
    //        console.info('This is c', c, "and its code", c.charCodeAt(0));
    //    }
    //}
};

var markup_to_html = function(text, a, b, c) {
    text = markup_preprocess(text, a, b, c);
    return parse_markup_to_html(text, a, b, c);
};

module.exports.markup = {};

module.exports.markup_to_html = markup_to_html;
module.exports.parse_markup_to_html = parse_markup_to_html;
module.exports.markup_preprocess = markup_preprocess;
module.exports.html_to_markup = html_to_markup;

