/*
 * Contains the parser for the Scroll Markdown language, as
 * defined by tags in a given scroll workspace.
 *
 * This is a new version based on TagLex
 */


var _ = require('underscore');
_.str = require('underscore.string');
var taglex = require('taglex');
var util = require('util');
//var old_markup = require('./old_markup')

var XML_OPEN = /\r?\n\s*\r?\n\s*<[\w:-]+[ \t]*>/;

// Node types:
var TEXT        = 1; exports.TEXT       = TEXT;
var TAG         = 2; exports.TAG        = TAG;
var OPEN_TAG    = 3; exports.OPEN_TAG   = OPEN_TAG;
var CLOSE_TAG   = 4; exports.CLOSE_TAG  = CLOSE_TAG;
var NODE_ENTER  = 5; exports.NODE_ENTER = NODE_ENTER;
var NODE_EXIT   = 6; exports.NODE_EXIT  = NODE_EXIT;


// ScrollMarkdownParser sets up a parser using TagLex
// emit function:
// (type, tag, contents)
// E.G.:
// (TEXT,       null,         "Asdf")
// OR:
// (OPEN_TAG,   [Tag object], null)
// (CLOSE_TAG,  [Tag object], null)

var TagLexParser = function (tagloader, opts) {
    // One of these should be generated per document, and needs to be
    // re-compiled if the documents tagset changes
    var defs = {
        emit_source: false,
        trim_doc: true,
    };
    this.opts         = _.extend(defs, opts);
    this.tagloader    = tagloader;
    this.tags         = tagloader.all_tags;
    this.tag_contexts = {};

    // taglex stuff
    this.ruleset = null;
    this.classes = null;
};

TagLexParser.prototype.parse = function (text, emit, done) {
    if (this.opts.trim_doc) {
        text = _.str.trim(text);
    }

    var source = null; // holds the last source code we received
    var _bufferer = null;
    var parser = null;
    var _parser = this.ruleset.new_parser();

    if (this.opts.emit_source) {
        _bufferer = new taglex.SourceBufferer(_parser, taglex.ROOT);
        parser = _bufferer;
    } else {
        parser = _parser;
    }

    parser.on("source_buffer", function (s) {
        source = s;
    });

    parser.on('tag_open', function (payload) {
        emit(OPEN_TAG, payload, source);
        if (source) { source = null; } // consume source
    });

    parser.on('symbol', function (payload) {
        // just a hack, do open and close to simulate a symbol no-op
        emit(OPEN_TAG, payload, null);
        emit(CLOSE_TAG, payload, null);
    });

    parser.on('text_node', function (text) {
        emit(TEXT, null, text);
    });

    parser.on('tag_close', function (payload) {
        emit(CLOSE_TAG, payload, null);
    });

    _parser.write(text);
    parser.end();
    done();
};



var get_parser = function (type, tagloader, opts) {
    var TYPES = {
        md: ScrollMarkdownParser,
        markdown: ScrollMarkdownParser,
    };

    var parser_class = TYPES[type.toLowerCase()];

    if (parser_class) {
        return new parser_class(tagloader, opts);
    } else {
        throw new Error("Unknown file type: " + md);
    }
};


var ScrollMarkdownParser = function (tagloader, opts) {
    TagLexParser.call(this, tagloader, opts);
};

util.inherits(ScrollMarkdownParser, TagLexParser);

var MD_DEFAULT_OPEN = "%s{";
var MD_DEFAULT_CLOSE = "}";

var MD_CONTAINS = "mdc_";

var PARA_CLOSE = {
    token: "\n\n",                    // canonical token form
    re: "\\s*\\r?\\n\\s*\\r?\\n\\s*", // regular expression to match above
};

var BLOCK_DEFAULT = ["", PARA_CLOSE];

/*
Creates TagLex TagRuleSet for parsing this thing

*/
ScrollMarkdownParser.prototype.compile = function (callback) {
    this.ruleset = new taglex.TagRuleSet({
        normalizer: function (token) {
            return token.replace(/[ \t\r]/g, '')
                        .replace(/\n\s*\n[\s*\n]*/g, "\n\n");
        },
        root_ignore_text: true,
    });
    this.classes = new taglex.TagClassManager();

    // Compile all relevant lexers
    for (var i in this.tags) {
        // "c_name" is a unique ID for this tag's set of contained
        // tags w.r.t parsing markdown
        var tag = this.tags[i];
        var markdown = tag.info.markdown;
        var aliases = [];
        var parents = [];
        tag.containment_class.forEach(function (keyword) {
            parents = parents.concat(this.classes.get(MD_CONTAINS + keyword));
        }, this);

        /*if (tag_class === 'text') {
            parents = parents.concat(this.classes.get('root'));
        }*/

        var opts = {
            name: tag.tag_class,
            parents: parents,
            payload: tag,
            aliases: aliases,
        };

        if (tag.info.symbol && tag.info.symbol.tag) {
            // Not a normal tag, a symbol tag instead, and continue
            opts.symbol = tag.info.symbol.tag;
            this.ruleset.add_symbol(opts);
            continue;
        }

        if (markdown) {
            if (markdown.type === "block") {
                // block types can always go in root, for now!
                opts.parents = opts.parents.concat(this.classes.get('root'));
                opts.force_close = true;

                // Now we put in suffix stuff
                if (markdown.block_prefix) {
                    var open = markdown.block_prefix || '';
                    var close = PARA_CLOSE;
                    aliases.push([open, close]);
                } else if (markdown.block_default) {
                    aliases.push(BLOCK_DEFAULT);
                }

            } else if (markdown.markdown) {
                var open_close = markdown.markdown.split("$");
                aliases.push(open_close);
            }

            // Add to containment class
            markdown.contains.forEach(function (name) {
                this.classes.add(tag.tag_class, MD_CONTAINS + name);
            }, this);
        }

        // set up default, also
        if (aliases.length < 1) {
            // both short and fully qualified
            aliases.push([MD_DEFAULT_OPEN.replace("%s", tag.tag_class),
                          MD_DEFAULT_CLOSE]);
            aliases.push([MD_DEFAULT_OPEN.replace("%s", tag.name),
                          MD_DEFAULT_CLOSE]);
        }

        // Now set up "XML" alternative formats
        // TODO: turn these into regexp that collapse ' ' and '\t'
        aliases.push(["<" + tag.tag_class + ">\n",
                        "\n</" + tag.tag_class + ">"])
        aliases.push(["<" + tag.name + ">\n",
                        "\n</" + tag.name + ">"])

        this.ruleset.add_tag(opts);
    }

    // Mute text node output in root

    // Finally compile the ruleset:
    this.ruleset.compile();

    callback();
};

module.exports.ScrollMarkdownParser = ScrollMarkdownParser;
module.exports.get_parser = get_parser;

