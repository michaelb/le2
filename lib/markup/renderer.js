var TinyTiny = require('../tinytiny/tinytiny_extensions');
var _ = require('underscore');
_.str = require('underscore.string');

var TEXT = 1;
var TAG  = 2;
var OPEN_TAG  = 3;
var CLOSE_TAG = 4;

var escape_html = function (text) {
    return text.replace(/&/g, "&amp;")
               .replace(/</g, "&lt;")
               .replace(/>/g, "&gt;");
};

var escape_quotes = function (text) {
    return text.replace(/"/g, "&quot;");
};

var Renderer = function (opts) {
    var defs = {
        target: "editor",
        templater: TinyTiny,
        split_html: /\{%\s*contents\s*%\}/g,
        disable_custom_html: false,
    };
    this.opts = _.extend(defs, opts);
    this.tagloader = this.opts.tagloader;
    this.Template = this.opts.templater
};

Renderer._render_instance = 1234;
Renderer.prototype.new_render_instance = function (i) {
    Renderer._render_instance += (i || 1);
    this.render_instance_id = Renderer._render_instance;
};

Renderer.prototype.get_default_template = function (node, tagname) {
    ////// Fall through, and build template out of built in style
    var tag = node.tag;
    var targetted_style = tag.get("html", this.opts.target);
    var _this = this;
    if (!this.Template) {
        // No templating system, not yet supported
        throw "Not implemented yet."
        var s = targetted_style.split(this.opts.split_html);
        open_template = s[0] || '';
        close_template = s[1] || '';
    }

    var template_string = targetted_style;
    if (!template_string) {
        if (!tagname) { tagname = tag.is_block() ? 'div' : 'span'; }
        template_string = ['<', tagname,' class="',
                            escape_quotes(tag.tag_class), '">',
                            '{% contents %}', "</", tagname, ">"].join('');
    }

    if (!node.is_unranked) {
        template_string = template_string.replace(this.opts.split_html,
                            "{% head_contents %}") + '{% contents %}';
    }

    return this.Template(template_string);
};

Renderer.prototype.render_to_string = function (text, parser, callback) {
    // Do a simple clean, flat render to a string. This is used by the editor
    // to generate working copy representations of data, for example.
    var result = [];
    var tag_class_tack = [];
    var _this = this;
    var emit = _.bind(result.push, result);

    var on_token = function (type, tag, text_content) {
        if (type === OPEN_TAG) {
            _this._emit_open(emit, tag.tag_class, text_content);
        } else if (type === CLOSE_TAG) {
            _this._emit_close(emit, tag.tag_class, null);
        } else {
            emit(_this._get_text(text_content));
        }
    };

    var on_end = function () {
        callback(result.join(""));
    };
    parser.parse(text, on_token, on_end);
};


var EditorRenderer = function (tagloader, opts) {
    var defs = {
        //attach_source: true,
        tagloader: tagloader,
        normalize_source: true,
    };
    Renderer.call(this, _.extend(defs, opts));

    this.normalize_source = this.opts.normalize_source || null;
};

EditorRenderer.prototype = new Renderer;

EditorRenderer.prototype.compile = function (callback) {
    // Compile templates, parse out strings
    this.html_tag_open = {};
    this.html_tag_close = {};
    var tags = this.tagloader.all_tags;
    for (var i in tags) {
        var tag = tags[i];
        var targetted_style = tag.get("html", "editor");

        if (tag.is_symbol()) {
            // Symbol is a simple, special case, where the targeted style is
            // always used no matter the context
            if (!targetted_style) {
                throw new Error("Render style required for symbol: " + tag.info.symbol.tag);
            };
            this.html_tag_open[tag.tag_class] = ['', ''];
            this.html_tag_close[tag.tag_class] = targetted_style;
            continue;
        }

        var open_start = (tag.is_block() ? '<bk class="' : '<in class="')
                            + escape_quotes(tag.tag_class);
        var open_finish = '">';
        var templates = null;
        if (targetted_style) {
            templates = targetted_style.split(this.opts.split_html);
        }

        if (!this.opts.disable_custom_html && templates) {
            // Parse custom HTML
            open_finish += templates[0];
        }

        var close = tag.is_block() ? "</bk>" : "</in>";
        if (!this.opts.disable_custom_html && templates) {
            close = templates[1] + close;
        }

        // Keep split into start and finish
        this.html_tag_open[tag.tag_class] = [open_start, open_finish];
        this.html_tag_close[tag.tag_class] = close;
    }
    callback();
};

EditorRenderer.prototype._emit_open = function (emit, tag_class, text_content) {
    var tag_open = this.html_tag_open[tag_class];
    emit(tag_open[0]);
    if (text_content) {
        emit('" data="');
        if (this.normalize_source) {
            emit(_.str.clean(escape_quotes(text_content)));
        } else {
            emit(escape_quotes(text_content));
        }
    }
    emit(tag_open[1]);
};

EditorRenderer.prototype._emit_close = function (emit, tag_class, text_context) {
    emit(this.html_tag_close[tag_class]);
};

EditorRenderer.prototype._get_text = escape_html;


/*
 * StyleRenderer is a typical renderer used during an export operation
 */
var StyleRenderer = function (tagloader, style, opts) {
    var defs = {
        tagloader: tagloader,
        style: style,
        context: {}, // extra render context
    };

    this.tagloader = tagloader;
    this.style = style;
    this._cached_templates = {};
    Renderer.call(this, _.extend(defs, opts));
};

StyleRenderer.prototype = new Renderer;

StyleRenderer.prototype.compile = function (callback) {
    // Style renderer compiles its templates on the fly
    callback();
};

StyleRenderer.prototype.get_template = function (node) {
    // STYLE RULES PRECEDENCE (think CSS)
    // 1. Direct match from Style
    // 2. Direct match from parent Style
    // 3. Default tag-defined fallback

    // First, check if there is a direct match
    var tag = node.tag;

    if (tag.name === "root") {
        // Root tag, search for root in particular
        var root = this.style.get_root();
        if (root) {
            // Root specified, use
            var template = this.Template(root.template);
            return template;
        } else {
            // default to simple concatenating children for root
            return this.Template("{% contents %}");
        }
    }

    ////// Check style matches
    var style = this.style.get_style(tag);
    if (style && style.template) {
        var template = this.Template(style.template);
        return template;
    } else if (style && (style.open_template || style.close_template)) {
        // "Pre-split" style template
        return this.Template([style.open_template || '',
                    '{% contents %}', style.close_template || ''].join(''));
    }
    // style does not have necessary info for the tag

    ////// Check parent style matches
    // (look up style of .parent), and see if we have a child
    // match, e.g. _first_para or something)
    // TODO requires  "positional TagMatcher", for :first and
    // :nth-child type matching


    ////// Fall through, and build template out of built in style
    return this.get_default_template(node);
};


StyleRenderer.prototype._get_text = escape_html;

StyleRenderer.prototype.tree_to_string = function (treenode, opts) {
    opts = opts || {};
    if (_.isArray(treenode)) {
        // Render each, then join the result
        var render = _.bind(function (node) {
            return this.tree_to_string(node, opts);
        }, this);
        return treenode.map(render).join('');
    }

    if (treenode.rendered && treenode.rendered === this.render_instance_id) {
        return ''; // already rendered!
    }

    if (treenode.is_text) {
        // text node
        return this._get_text(treenode.text);
    } 

    var template = this.get_template(treenode);
    // Ctx is shallow copy of opts context
    var ctx = _.extend({}, this.opts.context, {
        node: treenode,
        tag: treenode.tag,
        renderer: this,
    });

    if (opts.mark) {
        node.rendered = this.render_instance_id;
    }

    return template(ctx);
};

StyleRenderer.prototype.render_to_string = function (text, parser, callback) {
    this.new_render_instance(text.length);
    // Top-down render to string
    var _this = this;
    parser.parse(text, function () {}, function (tree) {
        callback(_this.tree_to_string(tree));
    });
};

module.exports.StyleRenderer = StyleRenderer;
module.exports.EditorRenderer = EditorRenderer;

