var path = require('path');
var fs = require('fs');

var _ = require('underscore');
_.str = require('underscore.string');

var LoaderBase = require('../util/loaderbase');
var ajs = require('../util/ajs');

// Pull in tag schema
var SCHEMA = require('./schemas').tag;

var DEFAULT_CSS = 'editor';

/* ***************************************
 * Tag represents a single tag           */
var Tag = function (namespace, name, validated_info) {
    // Get info, like we would find in a tag_name.cfg file
    this.namespace = namespace;
    this.name = name;
    this.tag_class = this.namespace + '_' + this.name;
    this.info = validated_info;
    this.css_selector = "." + this.tag_class;
    this.prepared = false;

    this.containment_class = this.info.tag.class;
    if (this.info.markdown) {
        this.contains = this.info.markdown.contains;
    } else {
        this.contains = [];
    }
    this.meta = this.info.tag;
};

Tag.prototype.get_oldstyle_info = function () {
    return _.extend({
        name: _.str.humanize(this.name),
        help: false,
        short_label: _.str.humanize(this.name),
        markup: this.info.tag[0].markdown
    }, this.info.tag[0]);
};

Tag.prototype.is_block = function () {
    return this.info.markdown && this.info.markdown.type === 'block';
};


Tag.prototype.is_symbol = function () {
    return this.info.symbol && this.info.symbol.tag;
};

/* "Bakes" CSS and HTML into pre-rendered templates for fast
 * insertion / removal
 */
Tag.prototype.prepare = function (callback) {
    this.css  = {};
    this.html = {};
    for (var i in this.info.style) {
        var style = this.info.style[i];
        var css = this._replace_css_sheet(style.css);
        //var html = this._wrap_html(style.html);
        var html = style.html;

        for (var i in style.target) {
            var target_name = style.target[i];
            this.css[target_name] = css;
            this.html[target_name] = html;
        }
    };

    this.prepared = true;
    callback();
};


Tag.prototype._replace_css_sheet = function (css) {
    // Skip over empty CSS
    var s = css;
    if (!s || _.str.trim(s) === '') {
        return '';
    }

    s = css.replace(/CLASS/g, this.tag_class)
           .replace(/TAG/g, this.css_selector)
           .replace(/NS/g, this.namespace)
           .replace(/NAME/g, this.name);

    // remove comments:
    s = s.replace(/\/\*[^*]*\*+([^/*][^*]*\*+)*\//g, '');

    // now clean up, and prefix anything thats missing it with the appropriate
    // class:
    s = s.split("}").map(_.bind(function (declaration) {
        var decr = _.str.clean(declaration);
        if (decr === '') { return ''; }
        if (decr.indexOf(this.css_selector) !== 0) {
            decr = this.css_selector + ' ' + decr;
        }
        return decr;
    }, this)).join("} ");

    // NOTE: this is NOT secure, it is just convenient. It's very
    // easy to "escape" it, e.g. TAG, html { display: none; }

    return s;
};

Tag.prototype.get = function (type, targets, is_retry) {
    /* Gets a particular rendering for this tag */
    if (_.isString(targets)) { targets = [targets] }
    target = target || ['']; // to force default

    if (type === 'css') { obj = this.css; }
    else { obj = this.html }

    // Now we try a few possibilities:
    for (var i in targets) {
        var target = targets[i];
        if (target in obj) {
            return obj[target];
        }
    }

    if (is_retry) { throw new Error("Tag contains no defaults!"); }
    return this.get(type, ['editor', 'default'], true);
};

Tag.prepare_containment = function (all_tags, callback) {
    var contained_by = {};

    // First pass, create dicts based on containment keywords
    for (var i in all_tags) {
        var tag = all_tags[i];
        tag.containment_class.forEach(function (keyword) {
            if (!contained_by[keyword]) {
                contained_by[keyword] = [];
            }
            contained_by[keyword].push(tag);
        });
    }

    // Second pass, populate containment hierarchies for each 
    for (var i in all_tags) {
        var tag = all_tags[i];
        var list = [];

        // Extend list by all children
        tag.contains.forEach(function (keyword) {
            var children = contained_by[keyword] || [];
            list = list.concat(children);
        });

        // Attach list to tag
        tag.containment = new Containment(list, tag.contains);
    }

    callback();
};


/* ***************************************
 * Containment                           */
var Containment = function (contains, names) {
    //this.children = ajs.Set(contains);
    this.tags = contains;
    this.containment_name = names.sort().join(':');

    this.can_contain_blocks = false;
    for (var i in contains) {
        if (contains[i].is_block()) {
            this.can_contain_blocks = true;
            break;
        }
    }
};

Containment.prototype.has = function (namespace, name) {
    for (var i in this.tags) {
        if (this.tags[i].name === name &&
                this.tags[i].namespace === namespace) {
            return true;
        }
    }
    return false;
};

var TagLoader = function (tag_groups) {
    // {"paths": ["tag.cfg"], "namespace": "base"}
    // What to load
    this.tag_groups = tag_groups;
    var this_ = this;

    LoaderBase.call(this, {
        groups: tag_groups,
        Class: Tag,
        schema: SCHEMA,
        plural_name: 'tags',
        compile_method: 'prepare',
        finish_load: function (callback) {
            // Step 2, set up containment and post-tag processing
            Tag.prepare_containment(this_.all_tags, callback);
        },
    });
};

TagLoader.prototype = new LoaderBase;

TagLoader.prototype.render_css = function (target) {
    return this.all_tags.map(function (tag) { return tag.get('css', target); })
                        .reduce(function (a, b) { return a + b; }, '');
};

module.exports.Tag = Tag;
module.exports.TagLoader = TagLoader;

