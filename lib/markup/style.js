var LoaderBase  = require('../util/loaderbase');
var _ = require('underscore');
_.str = require('underscore.string');

// Pull in style schema
var SCHEMA = require('./schemas').style;

/* ***************************************
 * Style represents a single style           */
var Style = function (namespace, name, validated_info) {
    this.namespace = namespace;
    this.name = name;
    this.info = validated_info;
    this.templates = null;
    this.meta = this.info.style;
};

Style.prototype.compile = function (callback) {
    callback();
};

Style.prototype.get_root = function () {
    for (var i in this.info.template) {
        var tinfo = this.info.template[i];
        if (tinfo.match.matches_root()) {
            return tinfo;
        }
    }
};

Style.prototype.get_style = function (tag) {
    // Check for tag
    for (var i in this.info.template) {
        var tinfo = this.info.template[i];
        if (tinfo.match.match(tag)) {
            return tinfo;
        }
    }
    return null;
};

var StyleLoader = function (style_groups) {
    // {"paths": ["style.cfg"], "namespace": "base"}
    // What to load
    LoaderBase.call(this, {
        groups: style_groups,
        Class: Style,
        schema: SCHEMA,
        plural_name: 'styles',
        compile_method: 'compile',
    });
};

StyleLoader.prototype = new LoaderBase;


module.exports.StyleLoader = StyleLoader;
module.exports.Style = Style;
