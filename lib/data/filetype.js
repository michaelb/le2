var path = require('path');
var fs   = require('fs');

var schemaconf   = require('schemaconf');

var LoaderBase  = require('../util/loaderbase');
var _ = require('underscore');
_.str = require('underscore.string');

var SCHEMA = require('./schemas').filetype;

var CONF_HEADER_BYTES = 2048;

var FileType = function (namespace, name, validated_info) {
    this.namespace = namespace;
    this.name = name;
    this.info = validated_info;
};


FileType.prototype.prepare = function (callback) {
    // Just see if we need to do magic number sniffing or schemaconf checking
    this.read_data = false;
    this.extensions = {};
    for (var i in this.data.match) {
        // Check if we will need to read the data
        var match = this.data.match[i];
        if (match.schemaconf) {
            this.read_data = true;
            this.read_length = CONF_HEADER_BYTES;
        }
        
        //if (match.signature) {
        //    this.read_data = true;
        //    // Check position, length, etc
        //}

        // Build a set (dict) of extensions
        if (match.extension) {
            var split = match.extension.split(',');
            for (var j in split) {
                var ext = _.str.trim(split[j]);
                if (ext.length > 0) {
                    this.extensions[ext] = true;
                }
            }
        }
    }
    callback();
};

FileType.prototype._check_extensions = function (filepath) {
    var extension = _.str.trim(path.extname(filepath.toLowerCase()), ".");
    return extension in this.extensions;
};


FileType.prototype._check_file_contents = function (buffer) {
    if (this.match.schemaconf) { throw new Error("NIY"); }

    var obj = {};
    try {
        schemaconf.parse(buffer.toString(), obj);
    } catch (e) {
        // Swallow errors, since we only read the first bytes of file 
    }
    return this.match.schemaconf in obj;
};

FileType.prototype.check_file = function (filepath, buffer) {
    if (!this.read_data) {
        return this._check_extensions(filepath));
    }

    // Already been read, lets just re-use it
    return this._check_file_contents(buffer);
};



var FileTypeLoader = function (file_groups) {
    FileTypeBase.call(this, {
        groups: file_groups,
        Class: FileType,
        schema: SCHEMA,
        plural_name: 'filetypes',
        compile_method: 'prepare',
        /*
        finish_load: function (callback) { },
        */
    });
};

FileTypeLoader.prototype = new LoaderBase;

FileTypeLoader.prototype.get_data = function (filepath, callback) {
    // First, read first X bytes of data
    var buffer = new Buffer(CONF_HEADER_BYTES);
    fs.open(filepath, 'r', function (err, fd) {
        if (err) { throw err; }
        fs.read(fd, buffer, 0, CONF_HEADER_BYTES, 0, function (err, bytes_read) {
            callback(buffer);
        });
    });
};

FileTypeLoader.prototype.get_filetype = function (filepath, callback) {
    /*
      Checks given filetype against all of the FileType checkers
      loaded. Calls back with FileType if found, otherwise null;
    */
    this.get_data(_.bind(function (buffer) {
        for (var i in this.filetypes) {
            var filetype = this.filetypes[i];
            if (filetype.check_file(filepath, buffer)) {
                // Found match
                callback(filetype);
                return;
            }
        }

        // No match
        callback(null);
    }, this));
};

module.exports.FileTypeLoader = FileTypeLoader;
module.exports.FileType = FileType;
