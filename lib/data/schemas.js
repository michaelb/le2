var Types = require('schemaconf').Types;
var _ = require('underscore');

var path_asset_set = function () {
    return {
        singular: false,
        required: false,
        values: {
            namespace: true,
            paths: {
                type: Types.wordlist,
                required: true, 
                default: [], 
            },
        },
    };
};


module.exports.manifest = {
    meta: {
        singular: true,
        required: true,
        values: {
            author: true,
            name: false,
            description: false,
        }
    },

    tags: path_asset_set(),
    styles: path_asset_set(),
    exports: path_asset_set(),
    templates: path_asset_set(),

    document: {
        singular: false,
        required: true,
        values: {
            name: false,
            path: false,
            primary: {
                type: Types.bool,
                default: true,
                required: false, 
            },
            chunks: {
                type: Types.wordlist,
                required: false, 
                default: [], 
            },
        },
    },

    /*
    styles: {
        singular: false,
        required: false,
    },
    */
};

// template is a clone of manifest
module.exports.template = {
    meta: {
        singular: true,
        required: true,
        values: {
            author: false,
            name: false,
            description: false,
        }
    },

    tags: {
        singular: false,
        required: true,
        values: {
            namespace: true,
        },
    },

    styles: {
        singular: false,
        required: false,
        values: {
            namespace: true,
        },
    },
};


module.exports.scrollrc = {
    user: {
        singular: true,
        required: true,
        values: {
            name: true,
        },
    },
    program: {
        singular: false,
        required: false,
        values: {
            name: true,
            path: true,
            autodetected: {
                type: Types.bool,
                default: false,
                required: false, 
            },
        },
    },
};

module.exports.filetype = {
    filetype: {
        singular: true,
        required: true,
        values: {
            name: true,
            category: true,
            grouping: false,
            directory: false,
        },
    },

    match: {
        singular: true,
        required: false,
        values: {
            extension: false,
            schemaconf: false,
            //mimetype: false,
        }
    },
};


