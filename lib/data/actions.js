var fs = require('fs');
var glob = require('glob');
var path = require('path');

var schemaconf  = require('schemaconf');

var Workspace = require('../data/workspace');
var Config = require('../data/config');
var Subprogram = require('../data/subprogram');
var userid = require('../util/userid');
var fsutils = require('../util/fsutils');
var uuid = require('node-uuid');
var _ = require('../util/uscoreenh');

// High level actions, generally 1 to 1 with CLI commands, useful
// by both editor and CLI interface

///////////////////
/////////////////// init
///////////////////
var DEFAULT_MANIFEST = [
        "[meta]",
        "author=%s",
        "",
        //"[tags]",
        //"namespace=base",
        //"paths : EOF",
        //"    paragraph.cfg",
        //"EOF",
        "[document]",
        "path=document.md",
        "",
    ].join("\n");

var DEFAULT_DOCUMENT = "\n";

var DEFAULT_PARAGRAPH = [
        "[tag]",
        "name = Paragraph",
        "class = text",
        "",
        "[markdown]",
        "type = block",
        "block_default = true",
        "contains = style",
        "",
        "[style]",
        "target = default",
        "html = <p>{{ contents }}</p>",
    ].join("\n");


module.exports.write_uuid = function (path, callback) {
    var data = uuid.v4();
    fs.writeFile(path, data + "\n", function (err) {
        if (err) { throw "ioerror, could not write uuid: " + uuid; }
        callback();
    });
};


module.exports.init = function (worktree, callback, error) {
    var COUNT = 3;
    var manifest_path  = path.join(worktree, "manifest.cfg");
    var document_path  = path.join(worktree, "document.md");
    //var paragraph_path = path.join(worktree, "paragraph.cfg");
    var uuid_path      = path.join(worktree, ".scrollid");

    var check_exists = function () {
        var done = _.after(COUNT-1, do_it);
        var check = function (exists) {
            if (exists) { error(); }
            else { done(); }
        };
        fs.exists(manifest_path,  check);
        fs.exists(document_path,  check);
        //fs.exists(paragraph_path, check);
    };

    var do_it = function () {
        var done = _.after(COUNT, callback);

        userid.get_user_info(function (info) {
            // Take the best guess at the persons username
            var name = info.bestname;

            // Take the best guess at the persons username
            var manifest_text = DEFAULT_MANIFEST.replace("%s", name);
            fs.writeFile(manifest_path,  manifest_text, done);
            fs.writeFile(document_path,  DEFAULT_DOCUMENT, done);
            //fs.writeFile(paragraph_path, DEFAULT_PARAGRAPH, done);

            // writing UUID is optional
            fs.exists(uuid_path, function (exists) {
                if (!exists) {
                    module.exports.write_uuid(uuid_path, done);
                } else { done(); }
            });
        });
    };

    check_exists();
};

///////////////////
/////////////////// init conf
///////////////////

var DEFAULT_SCROLLRC = [
        "[user]",
        "name=%s",
        "",
    ].join("\n");


exports.init_conf = function (callback, on_error, options) {
    var opts = _.extend({
        datadir: path.resolve(__dirname, "..", "..", "data", "conf"),
    }, options);
    /* ***************************************
     * Step 1, configure self, and make relevant dirs
     * *************************************** */
    var makedirs = function () {
        userid.get_conf_dir(function (dir, info) {
            makerc(dir, info.username);
        });
    };

    /* ***************************************
     * Step 2, create default RC file
     * *************************************** */
    var makerc = function (dir, username) {
        var next = function () { copyfiles(dir); };

        // Get default applications, and autogenerate initial data
        Subprogram.get_defaults(function (subprograms) {
            var rc = DEFAULT_SCROLLRC.replace("%s", username);
            rc += schemaconf.unparse({"program": subprograms});
            fsutils.defaultfile(path.join(dir, "scrollrc"), rc, next);
        });
    };

    /* ***************************************
     * Step 3, copy over default stash
     * *************************************** */
    var copyfiles = function (dir) {
        // "data/conf" -> where we keep defaults --- possibly later put in a zip
        glob(path.join(opts.datadir, "**"), function (err, paths) {
            // check for empty case
            if (paths.length < 1) { return finish(); }

            var done = _.after(paths.length, finish);
            paths.forEach(function (filepath) {
                if (opts.datadir === filepath) {
                    return done(); // matches base dir, also
                }
                var relpath = path.relative(opts.datadir, filepath);
                var destpath = path.join(dir, relpath);
                fsutils.defaultfilecopy({
                    source: filepath,
                    destination: destpath,
                    callback: done,
                    skipdirs: true,
                });
            });
        });
    };

    var finish = function () {
        callback();
    };
    makedirs();
};


exports.get_or_init_conf = function (callback, on_error, options) {
    userid.get_conf_dir(function (dir, rcpath, info) {
        var get_config = function () {
            // Exists at this point, load it!
            var config = new Config(options || {});
            config.load(dir, info, function () {
                callback(config);
            });
        };

        fs.exists(dir, function (exists) {
            if (!exists) {
                // init it first
                exports.init_conf(get_config, function (err) {
                    _this.cli.fatal("Could not create config directory:" + err);
                }, on_error, options);
            } else {
                // Already exists, just get it right away
                get_config();
            }
        });
    });
};


///////////////////
/////////////////// stash list
///////////////////

module.exports.stash_list = function (config, callback, error) {
    // Prepares a nice obj listing all items in the stash
    var o = {};
    var add_to_list = function (datatype) {
        if (!config.stash[datatype]) { return; }
        o[datatype] = {};
        config.stash[datatype].list
            .filter(function (item) {
                return !item.meta || !item.meta.hide;
            })
            .forEach(function (item) {
                if (!o[datatype][item.namespace]) {
                    o[datatype][item.namespace] = [];
                }
                o[datatype][item.namespace].push(_.extend({
                    filename: item.name,
                    namespace: item.namespace,
                }, item.meta || item.info.meta));
            });
    };
    add_to_list('styles');
    add_to_list('templates');
    add_to_list('tags');
    add_to_list('exports');

    callback(o);
};


///////////////////
/////////////////// stash load
///////////////////

var NEW_MANIFEST_ITEM = [
        "",
        "[%type%s]",
        "namespace = %namespace%",
        "paths     = %reldir%",
        "",
    ].join("\n");


module.exports.stash_load = function (config, workspace, type, namespace, name, callback, opts) {
    var error = opts.error || function (msg) { throw msg; };
    var debug = opts.debug || function () {};
    var skip_check = !!opts.skip_check;
    debug = debug || function () {};
    // "Mixes in" a stash item to an expected place in the working dir
    var names = [];
    var stash_loader = config.stash[type+"s"];


    /////////////////////////////////////////////////////
    // First, prepare and check the arguments and get set up
    var prepare = function () {
        if (!stash_loader) {
            return error("Stash " + type + " doesn't exist.");
        }

        var stash_ns = stash_loader.by_namespace[namespace];
        if (!stash_ns) {
            return error("Namespace " + namespace + " doesn't exist.");
        }

        if (name === null) {
            // name is null, load entire namespace
            debug("Loading entire namespace");
            names = _.keys(stash_ns);
        } else if (_.isString(name)) {
            names = [name];
        } else {
            names = name;
        }


        for (var i in names) { 
            if (!stash_ns[names[i]]) {
                return error(type + " " + name[i]+ " not found in "
                                + namespace + " namespace.");
            }
        };
        start();
    };

    var start = function () {

        /////////////////////////////////////////////////////
        // Second, check if we have a loader for this namespace:
        var loader_ns = workspace.loaders[type].by_namespace[namespace];
        if (loader_ns) {
            debug("Workspace already has namespace " + namespace);
            var example_path = _.values(workspace.loaders[type].paths[namespace])[0];
            debug("Trying to put new " + type + " alongside its sibling " + example_path);
            var target_dir = path.dirname(example_path);
            do_copy(target_dir);
        } else {
            // Easy-peasy, does not exist, lets put it in an expected
            // place and add the loader to the manifest
            debug("Workspace does not have namespace " + namespace);
            var target_dir = path.join(workspace.working_dir, type + "s", namespace);
            var reldir = path.join(path.relative(workspace.working_dir, target_dir), "*.cfg");
            var text = NEW_MANIFEST_ITEM
                        .replace(/%type%/g, type)
                        .replace(/%namespace%/g, namespace)
                        .replace(/%reldir%/g, reldir);
            workspace.append_to_manifest(text, function () {
                debug("Added '" + text + "' to manifest");
                do_copy(target_dir);
            });
        }
    };

    /////////////////////////////////////////////////////
    // Third, do the actual copy operations
    var do_copy = function (target_dir) {
        debug("Copying file to " + target_dir);
        var done = _.after(names.length, check);
        names.forEach(function (name) {
            stash_loader.copy_to(namespace, name, target_dir,
                function (err, was_copied) {
                    if (err) { error("Error copying: " + err); }
                    else if (!was_copied) { error("File already exists: " + name); }
                    else { done(); }
                });
        });
    };

    /////////////////////////////////////////////////////
    // Forth, make sure this tag gets loaded by reloading the workspace
    var check = function () {
        if (skip_check) {
            callback(null); // no new workspace
            return;
        }

        workspace.reload(function (new_workspace) {
            var new_ns = new_workspace.loaders[type].by_namespace[namespace];
            names.forEach(function (name) {
                if (name in new_ns) {
                    debug("Successfully loaded " + name);
                } else {
                    debug("Warning: " + name + " was copied, but it's not available");
                    // Make sure we have the target
                }
            });
            callback(new_workspace);
        });
    };

    prepare();
};


///////////////////
/////////////////// template_load
///////////////////


// "Mixes in" a template into the current workspace
// Generally intended for after a clean init, not for mid-work
// (which individual stash loads are better suited for)
module.exports.template_load = function (config, workspace, template, callback, opts) {
    var error = opts.error || function (msg) { throw msg; };
    var debug = opts.debug || function () {};
    var TYPES = ['tag', 'style'];

    /////////////////////////////////////////////////////
    // Loop through and get each
    var prepare = function () {
        var done = _.after(TYPES.length, do_reload);
        TYPES.forEach(function (typename) {
            var ns_names = _.pluck(template.info[typename+"s"] || [], "namespace");
            if (ns_names.length < 1) {
                done(); // is empty, skip
            } else {
                do_namespaces(typename, ns_names, done);
            }
        });
    };
    var stashloadopts = _.extend({}, opts, {skip_check: true})

    /////////////////////////////////////////////////////
    // Second, for each type and each namespace, load them all
    var do_namespaces = function (typename, ns_names, done) {
        var finished = _.after(ns_names.length, done);
        ns_names.forEach(function (nsname) {
            debug("Loading " + nsname);
            exports.stash_load(config, workspace, typename, nsname, null,
                               finished, stashloadopts);
        });
    };

    /////////////////////////////////////////////////////
    // Third, make sure this tag gets loaded by reloading the workspace
    var do_reload = function () {
        workspace.reload(function (new_workspace) {
            debug("Finished, reloading workspace!");
            callback(new_workspace);
        });
    };

    prepare();
};


///////////////////
/////////////////// template_init
///////////////////


// Simply a combo of init + template_load
module.exports.template_init = function (config, worktree, template_ns, template_name, callback, opts) {
    var error = opts.error || function (msg) { throw msg; };

    var template_namespace = config.stash.templates.by_namespace[template_ns];
    if (!template_namespace) {
        return error("Could not find template namespace '" + template_ns + "'");
    }

    var template = template_namespace[template_name];

    var init_completed = function () {
        var workspace = new Workspace(worktree);
        workspace.prepare(function () {
            exports.template_load(config, workspace, template, callback, opts);
        });
    };
    exports.init(worktree, init_completed, error);
};

