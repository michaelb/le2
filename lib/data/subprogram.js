var child_process = require('child_process');
var _ = require('underscore');
_.str = require('underscore.string');

var SUBPROGRAMS = ['git', 'libreoffice', 'ebook-convert', 'phantomjs', 'tar'];

var Subprogram = function (info) {
    this.info = info;
    this.process = null;
    this.baked_opts = {};
};

Subprogram.autodetermine = function (name, callback) {
    // Guesses where things are
    // (would need to be changed for full win support)
    child_process.exec("which " + name, function (error, stdout, stderr) {
        if (error !== null) {
            callback(null);
        } else {
            callback({
                path: _.str.trim(stdout),
                name: name,
                autodetected: true,
            });
        }
    });
};


Subprogram.get_defaults = function (callback) {
    var results = [];
    var done = _.after(SUBPROGRAMS.length, function () { callback(results); });
    SUBPROGRAMS.forEach(function (name) {
        Subprogram.autodetermine(name, function (result) {
            if (result !== null) {
                results.push(result);
            }
            done();
        })
    });
};

Subprogram.prototype.bake_opts = function (opts) {
    _.extend(this.baked_opts, opts);
};

Subprogram.prototype.run = function () {
    // shortcut for "spawn"
    var args = Array.prototype.slice.call(arguments);
    var callback = args.pop();
    this.spawn({
        args: args,
    }, callback);
};


Subprogram.prototype.run_output = function () {
    // shortcut for "spawn", with output collected as a string
    var args = Array.prototype.slice.call(arguments);
    var callback = args.pop();
    var result = [];
    var push_data = function (data) { result.push(data + ''); };
    this.spawn({
            args: args,
            on_stdout_data: push_data,
            _result: result,
        }, function () {
            callback(result.join(''));
        });
};

Subprogram.prototype.spawn = function (options, callback) {
    if (this.process) { throw "Already running!"; }

    var opts = _.extend({
        passthrough: true,
        progress: false,
        on_stdout_data: null,
        on_stderr_data: null,
        on_error: null,
        ignore_error: false,
        silent: false,
        collect_output: false,
        env: null,
        args: [],
    }, this.baked_opts, options);
    var _this = this;

    var process_opts = {};

    // cwd and env are just passed on
    if (opts.cwd) { process_opts.cwd = opts.cwd}
    if (opts.env) { process_opts.env = opts.env}

    // shortcut to add in output collecting system
    if (opts.collect_output) {
        var result = [];
        var push_data = function (data) { result.push(data + ''); };
        opts.on_stdout_data = push_data;
        opts._result = result;
        var old_callback = callback;
        callback = function () {
            old_callback(result.join(''));
        };
    }

    // various shortcut interfaces for a nicer data handling
    if (opts.on_stdout_data || opts.on_stderr_data) { /* pass */ }
    else if (opts.stdio) { process_opts.stdio = opts.stdio; }
    else if (opts.silent) { process_opts.stdio = "ignore";  }
    else if (opts.passthrough) { process_opts.stdio = "inherit"; }

    this.process = child_process.spawn(this.info.path, opts.args, process_opts);

    if (opts.on_stdout_data) {
        this.process.stdout.on('data', function (data) {
            opts.on_stdout_data(data);
        });
    }

    if (opts.on_stderr_data) {
        this.process.stderr.on('data', function (data) {
            opts.on_stderr_data(data);
        });
    }
    this.process.on('close', function (code) {
        _this.process = null;
        if (code !== 0) {
            if (opts.on_error) {
                opts.on_error(code);
            } else if (opts.ignore_error) {
                // pass
            } else {
                var output = (opts._result || []).join("");
                if (output.length > 0) {
                    output = "\nOUT: " + output + "\n";
                }
                throw [opts.args.join(" "), "- errorcode:", code, output].join(" ");
            }
        }
        callback(code);
    });
};

var yellow = function (text) {
    return ['\x1B[33m', text, '\x1B[0m:'].join('');
};




module.exports = Subprogram;

