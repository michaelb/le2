var fs = require('fs');
var fsutils = require('../util/fsutils');
var Subprogram = require('../data/subprogram');
var schemaconf = require('schemaconf');

var mkdirp = require('mkdirp');
var glob = require('glob');
var path = require('path');
var userid = require('../util/userid');
var inquirer = require("inquirer");
var _ = require('underscore');
var _tag = require('../markup/tag'),
    TagLoader = _tag.TagLoader;
var _style = require('../markup/style'),
    StyleLoader = _style.StyleLoader;
var _template = require('../data/template'),
    TemplateLoader = _template.TemplateLoader;

var SCHEMA = require('./schemas').scrollrc;
var GIT_TEMPLATE_DIR_SUFFIX = path.join("misc", "gittemplate");

/*
Handles:
    * Global settings
    * Stash operations
    * Templates
    * Workspaces
    e.g., the .config/scroll in the user's home directory
*/

var Config = function (opts) {
    opts = opts || {};

    // Allows us to selectively load items, to make CLI faster
    this.opts = _.extend({
        load: _.extend({
            stashtags: true,
            stashstyles: true,
            stashtemplates: true,
            //stashexports: true,
            // v-- give an ID of the workspace we want to load
            workspace: false,
        }, opts.load),
    }, opts);

    this.rc = null;
    this.os_userinfo = null;
    this.dir = null;
    this.programs = {};
};

Config.prototype.load = function (dir, userinfo, callback) {
    var _this = this;

    // v--- single arg mode
    if (!callback) { callback = dir; dir = null; userinfo = null; }

    /* ***************************************
     * Step 1 (optional), configure self: determine config dir
     * *************************************** */
    var configure_self = function () {
        userid.get_conf_dir(function (dir, info, rcpath) {
            fs.exists(dir, function (exists) {
                if (!exists) {
                    // doesn't exist, raise error
                    throw "Config: dir does not exist, needs to init first.";
                }

                schemaconf.fs.load({path: rcpath, schema: SCHEMA}, function (rc) {
                    _this.rc = rc;
                    rc.program.forEach(function (info) {
                       _this.programs[info.name] = new Subprogram(info);
                    });
                    // exists, next step
                    load_stash(dir, info);
                });
            });
        });
    };


    /* ***************************************
     * Step 2, load the stash
     * *************************************** */
    var load_stash = function (dir, info) {
        _this.os_userinfo = info; // assign user info since we got it
        _this.dir = dir;
        _this.GIT_TEMPLATE_DIR = path.join(dir, GIT_TEMPLATE_DIR_SUFFIX);

        _this.stash = {};
        // Filter through which things need to be loaded according to the opts
        // we were given
        var loaders = {
            stashtags: ["tags", TagLoader],
            stashstyles: ["styles", StyleLoader],
            stashtemplates: ["templates", TemplateLoader],
            //stashexports: ExportProfileLoader,
        };
        var stash_dir = path.join(_this.dir, "stash");
        var is_enabled = function (v, key) { return _this.opts.load[key]; }
        var enabled_loaders = _(_(loaders).pick(is_enabled)).values();

        // skip ahead if we have nothing to load
        if (enabled_loaders.length < 1) { return load_workspaces(); }

        var done = _.after(enabled_loaders.length, load_workspaces);

        // For each enabled loader, load the required stuff into our .stash property
        enabled_loaders.forEach(function (_loader) {
            var name  = _loader[0];
            var Class = _loader[1];
            fsutils.autoloader(Class, path.join(stash_dir, name), function (loader) {
                _this.stash[name] = loader;
                done();
            });
        });
    };

    /* ***************************************
     * Step 3, load work spaces
     * *************************************** */
    var load_workspaces = function () {
        callback();
    };

    if (dir) {
        load_stash(dir, userinfo);
    } else {
        configure_self();
    }

};

Config.prototype.path_for = function (type, name) {
    var type_dir = {
            document: "documents",
            tag: path.join('stash', "tags", name),
            style: path.join('stash', "styles", name),
            template: path.join('stash', "templates", name),
            export: path.join('stash', "exports", name),
        }[type];
    if (!type_dir) { throw "Unknown type: " + type; }
    return path.join(this.dir, type_dir, name);
};

Config.prototype.get_git = function (type, name) {
    /* 
     * Gets a sub program, properly configured to use git as the scroll user
     */
    if (!("git" in this.programs)) {
        throw new Error("git is not installed or configured properly!");
    }
    var git = this.programs.git;
    var name = this.rc.user.name;
    var email = this.rc.user.email || (name + '@mysterious');
    git.bake_opts({
        env: {
            GIT_CONFIG: '/tmp/NONEXISTENTFILE',
            GIT_AUTHOR_NAME: name,
            GIT_AUTHOR_EMAIL: email,
            GIT_COMMITTER_NAME: name,
            GIT_COMMITTER_EMAIL: email,
        },
    });
    return git;
};

Config.prototype.get_phantomjs = function () {
    /* 
     * Gets a sub program, properly configured to use git as the scroll user
     */
    if (!("phantomjs" in this.programs)) {
        throw "phantomjs is not installed or configured properly!";
    }
    var phantomjs = this.programs.phantomjs;
    return phantomjs;
};



module.exports = Config;

