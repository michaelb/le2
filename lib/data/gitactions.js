///// High level functions for git
// Presently, wraps CLI binary version of GIT. Later, need to implement a
// work-alike library in JavaScript, which will be used by default, and
// maintain both to verify compatibility (e.g. have same test suite run on both
// libs).

var _ = require('underscore');
_.str = require('underscore.string');
var fsutils = require('../util/fsutils');
var fs   = require('fs');
var path   = require('path');
var ncp = require('ncp').ncp;

var _get_git = function (config, workspace, opts) {
    var git = config.get_git();

    if (workspace) {
        if (workspace.working_dir) {
            git.bake_opts({ cwd: workspace.working_dir, });
        } else {
            git.bake_opts({ cwd: workspace, });
        }
    }

    if (opts.silent) {
        git.bake_opts({ silent: true});
    }

    return git;
};

var _scrollid_regexp = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-'+
                             '[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$', 'i');

var DELIM = ":5<r0l1@!"; // this is used as a hacky delimiter when parsing git log
var DELIM_REGEXP = new RegExp(DELIM, 'g');
var SCROLL_ACTION = "scroll-action:";
var BARE_REPO_NAME = "scroll.git";

var SCROLL_MAGIC = fsutils.consts.SCROLL_MAGIC;

var SCROLLFILE_LIST = [
    "manifest.cfg",
    ".scrollid",
];

var _checkscrollid = function (s) {
    if (!s.match(_scrollid_regexp)) {
        throw "Invalid Scroll, invalid UUID: " + s;
    }
};

exports._read_scrollid = function (uri, scrollid_is_sibling, callback) {
    if (uri.match(/^file:\/\//)) {
        var path_suffix = uri.slice('file://'.length);
        var scrollid_path;
        if (scrollid_is_sibling) {
            // Sibling file to git repo (e.g., bare repo)
            scrollid_path = path.join(path.dirname(path_suffix), ".scrollid");
        } else {
            // Existant in git repo (e.g., not bare repo)
            scrollid_path = path.join(path_suffix, ".scrollid");
        }

        // is an absolute path to a directory
        fs.readFile(scrollid_path, function (err, data) {
            if (err) { throw "Invalid Scroll, missing UUID: " + err + ' - ' + uri; }
            var scrollid = _.str.trim(data + '');
            _checkscrollid(scrollid);
            callback(scrollid);
        });

    } else if (uri.match(/^http:\/\//)) {
        // xxx: v--- this does not work, more just pseudocode
        httputils.get(function (err, data) {
            var scrollid = _.str.trim(data + '');
            if (err) { throw "Invalid Scroll, missing UUID: " + uri; }
            _checkscrollid(scrollid);
            callback(scrollid);
        });
    } else {
        // need to later support http, etc
        throw "Unsupported URI: " + uri;
    }
};


exports._process_uri = function (config, original_uri, callback) {

    var preprocess_scroll_file = function (uri) {
        // is an absolute path to a scroll file
        var path_suffix = uri.slice('scrollfile://'.length);

        // Get info about it
        fsutils.check_scroll_file(path_suffix, function (is_valid) {
            if (!is_valid) {
                throw "Invalid Scroll file: " + original_uri;
            }

            // Tar file, decompress to tmp directory
            //fsutils.mkdirtmp({subdirname: SCROLL_MAGIC}, function (tmpdir) {
            fsutils.mkdirtmp({}, function (tmpdir) {
                var full_tmp_dir = path.join(tmpdir, SCROLL_MAGIC);
                var new_uri = "scrolldir://" + full_tmp_dir;
                fsutils.tar.extract(path_suffix, tmpdir, function () {
                    preprocess_scroll_dir(new_uri);
                });
            });
        });
    };

    var preprocess_scroll_dir = function (uri) {
        var path_suffix = uri.slice('scrolldir://'.length);
        var new_uri = "file://" + path.join(path_suffix, BARE_REPO_NAME);

        fs.stat(path_suffix, function (err, stat) {
            // ensure that it is a folder
            if (err) { throw "Invalid Scroll, cannot read scroll dir: " + uri + err; }
            else if (stat.isFile()) {  throw "Invalid Scroll, no scroll dir: " + uri;}
            process_uri(new_uri, true);
        });
    };

    var process_uri = function (uri, scrollid_is_sibling) {
        // Now load a workspace here to get the scrollid
        exports._read_scrollid(uri, scrollid_is_sibling, function (scrollid) {
            callback(scrollid, uri);
        });
    };

    // First, lets check the path for "scrollfile"
    if (original_uri.match(/^scrollfile:\/\//)) {
        preprocess_scroll_file(original_uri);
    } else if (original_uri.match(/^scrolldir:\/\//)) {
        // Now lets check for scrolldir, which is the path to a dir that
        // contains a scroll.git directory (bare repo)
        preprocess_scroll_dir(original_uri);
    } else {
        process_uri(original_uri, false);
    }
};

var _get_head_state = function (git, callback) {
    git.spawn({
        args: ['symbolic-ref', '-q', 'HEAD'],
        collect_output: true,
        ignore_error: true,
    }, function (output) {
        var cleaned = _.str.trim(output);
        if (cleaned.length > 0) {
            callback(cleaned);
        } else {
            callback(null); // detached
        }
    });
};

exports.gitpull = function (config, uri, opts, callback) {
    //gitpull:   'clones or updates a git-controlled scroll into your home',
    // Requires a config, the source URI
    // opts can optionally contain {"dir": "..." } which is the target dir
    // callback is called with the resulting dir and the scrollid
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};

    var git = _get_git(config, null, opts);

    exports._process_uri(config, uri, function (scrollid, processed_uri) {
        opts.log("Found Scroll ID: " + scrollid);

        if (!opts.dir) {
            opts.dir = config.path_for("document", scrollid);
            opts.log("Using default location: " + opts.dir);
        }

        var done = function () { callback(opts.dir, scrollid); };
        fs.exists(opts.dir, function (fexists) {
            // local dir already exists
            if (fexists) {
                opts.log("Pulling from " + processed_uri + " into " + opts.dir);
                git.run("pull", "--work-tree", opts.dir, processed_uri, done);
            } else {
                opts.log("Cloning from " + processed_uri + " into " + opts.dir);
                git.run("clone", processed_uri, opts.dir, done);
            }
        });
    });
};

exports.gitinit = function (config, workspace, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    var git = _get_git(config, workspace, opts);
    git.run("init", "--template=" + config.GIT_TEMPLATE_DIR, callback);
};

exports.gitstage = function (config, workspace, opts, callback) {
    // Adds all relevant files to the staging area
    if (!callback) { callback = opts; opts = {}; }
    var git = _get_git(config, workspace, opts);
    workspace.get_all_paths(function (paths) {
        git.spawn({ args: ["add"].concat(paths), }, callback);
    });
};

exports.gitcommit = function (config, workspace, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    //if (!config) { throw "Argument Error: Config undefined"; }
    opts.log = opts.log || function () {};
    opts.message = opts.message || "nocomment";

    var git = _get_git(config, workspace, opts);
    //opts.filelist = opts.filelist || null;
    //var file_list_args = "-a" if

    if (opts.message.indexOf(DELIM) !== -1) {
        throw "Invalid message: contains sacred symbol.";
    }

    // STEP 1, check for detached head state
    var check_for_detached_head = function (head_state) {
        if (head_state) {
            // NOT DETACHED STATE, now just make commit
            make_commit();
        } else {
            // DETACHED HEAD, first checkout to new branch
            //opts.log("Detatched head detected! First creating an redo-history branch...");
            opts.log("Detatched head detected! Saving this state...");
            exports.gitundorevert(config, workspace, opts, function (){
                make_commit();
            });
        }
    };

    // STEP 2, make the actual commit, of anything staged
    var make_commit = function () {
        git.run("commit", "-m", opts.message, function () {
            callback();
        });
    };

    // start: if head_state is already computed, use that for logic, otherwise
    // fetch ourself
    if ('head_state' in opts) {
        check_for_detached_head(opts.head_state);
    } else {
        _get_head_state(git, check_for_detached_head)
    }
};

exports.gitcommitaction = function (config, workspace, verb, object, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    var git = _get_git(config, workspace, opts);

    _get_head_state(git, function (head_state) {
        opts.head_state = head_state;
        opts.message = [SCROLL_ACTION, verb, object].join(' ');
        if (head_state !== null) {
            // Not a detached head, e.g. on master or some other branch
            exports.gitstage(config, workspace, opts, function () {
                exports.gitcommit(config, workspace, opts, function () {
                    callback();
                });
            });
        } else {
            // A detached head state, go right to the commit logic
            exports.gitcommit(config, workspace, opts, function () {
                callback();
            });
        }
    });
};

exports.gitgethash = function (config, workspace, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};
    var git = _get_git(config, workspace, opts);
    git.run_output("rev-parse", "HEAD", function (hash) {
        callback(_.str.trim(hash));
    });
};

exports.gitundorevert = function (config, workspace, opts, callback) {
    // Non-destructive "undo" operation, that adds an "undo"
    // commit to the history
    if (!callback) { callback = opts; opts = {}; }
    var log = opts.log || function () {};
    var git = _get_git(config, workspace, opts);

    ///// STEP 1: gets hash of HEAD
    var get_hash = function () {
        exports.gitgethash(config, workspace, opts, back_to_tip);
    };

    ///// STEP 2: stashes working copy changes, goes to master
    var back_to_tip = function (to_hash) {
        log("gitundorevert: reverting to " + to_hash);
        // first, stash changes
        git.spawn({ args: ['stash'], ignore_error: true}, function (code) {
            log("gitundorevert: stashed any changes (code: " + code + ")");
            // then, checkout master
            git.run('checkout', 'master', function () {
                log("gitundorevert: checkout master, ready to revert");
                do_revert(to_hash);
            });
        });
    };

    ///// STEP 3: Reverts, does action commit and finally
    ///// re-applies changes (without committing)
    var do_revert = function (to_hash) {
        // Then, revert to old commit
        git.run('revert', '--no-commit', to_hash + "..HEAD", function () {
            // Make new actioncommit
            log("gitundorevert: revert success");
            var message = [SCROLL_ACTION, "reverted", to_hash].join(' ');
            git.run("commit", "-m", message, function () {
                // Now, pop changes
                git.spawn({ args: ['stash', 'pop'], ignore_error: true}, function () {
                    // and callback, only after adding everything
                    log("gitundorevert: stash popped, working area re-added");
                    exports.gitstage(config, workspace, opts, function () {
                        callback();
                    });
                });
            });
        });
    };

   get_hash();
};

exports.gitundofork = function (config, workspace, opts, callback) {
    if (!callback) { callback = opts; opts = {}; }
    opts.log = opts.log || function () {};
    var git = _get_git(config, workspace, opts);
    exports.gitlog(config, workspace, 0, 1, opts, function (log) {
        var branchname = "oldredo-" + hash;
        opts.log("Renaming master to " + branchname);
        git.run("branch", "-m", "master", branchname, function () {
            opts.log("Checking out at detached head state to master");
            git.run("checkout", "-b", "master", function () {
                callback();
            });
        });
    });
};

exports.gitundo = function (config, workspace, opts, callback) {
    //gitundo:   'undos the last commit in a git-controlled scroll',
    if (!callback) { callback = opts; opts = {}; }
    opts = _.extend({
        hash: null,
        head_commit: null,
        log: function () {},
    }, opts);
    var git = _get_git(config, workspace, opts);
    var head_commit;

    // STEP 1: get relevant hashes for this undo operation
    var get_hashes = function (hash) {
        // get last two hashes, use git log
        exports.gitlog(config, workspace, 0, 2, function (log) {
            if (log.length < 2) {
                throw "Nothing to undo!";
            }

            if (opts.head_commit === null) {
                opts.head_commit = log[0].hash;
            }

            if (opts.hash === null) {
                opts.hash = log[1].hash;
            }

            /*
            if (opts.hash === opts.head_commit) {
                // Attempt to checkout the head / master commit,
                // lets just out master instead
                opts.hash = "master";
            }
            */

            undo_to_hash(opts.hash);
        });
    };

    /*
    // STEP 2: create a "_detached_head" file
    var write_head_commit = function () {
        if (opts.hash === opts.head_commit) {
            // we are undoing to the HEAD commit, simply delete if
            // it exists, since we will no longer be in a detached
            // head state
            fs.unlink(path.join(dir, "_detached_head"), function (err) {
                if (err) { throw "error unlinking _detached_head flag"; }
                undo_to_hash(opts.hash);
            });
        } else {
            fs.writeFile(path.join(dir, "_detached_head"), opts.head_commit, function (err) {
                if (err) { throw "error writing _detached_head flag"; }
                undo_to_hash(opts.hash);
            });
        }
    };
    */

    // STEP 3: checkout the relevant commit
    var undo_to_hash = function (hash) {
        git.run("checkout", hash, function () {
            callback();
        });
    };

    get_hashes();
};


// inspired by SO #4600445
var pretty_format_flag = ['--pretty=format:{%n',
    'hash', ':', '%H', ',%n',
    'author', ':', '%an', ',%n',
    'date', ':', '%ad', ',%n',
    'email', ':', '%aE', ',%n',
    'message', ':', '%s', ',%n',
    'commitDate', ':', '%ai', ',%n',
    'age', ':', '%cr', '},'].join(DELIM);


var add_action_info = function (logentry) {
    // Parses a git commit message for Scroll action data
    if (logentry.message.match(/^scroll-action:/)) {
        var action = _.str.trim(logentry.message.split(':')[1]);
        var verb = action.split(' ')[0];
        var object = action.slice(verb.length);
        logentry.action = {
            subject: logentry.author,
            verb: _.str.trim(verb),
            object: _.str.trim(object),
        };
    } else {
        logentry.action = null;
    }
};

exports.gitlog = function (config, workspace, start_at, page_size, opts, callback) {
    //gitlog:   'shows ',
    if (!callback) { callback = opts; opts = {}; }

    var git = _get_git(config, workspace, opts);
    var args = ["log",
            "--max-count=" + page_size, "--skip=" + start_at,
            "--branches=*", pretty_format_flag];
    var result = [];
    var push_data = function (data) { result.push(data + ''); };
    var finish = function () {
        var out = result.join('');
        if (out.lastIndexOf(',') === out.length-1) {
            out = out.slice(0, out.length-1); }
        out = out.replace(/"/g, '\\"').replace(DELIM_REGEXP, '"');
        out = ['[', out, ']'].join('');
        var parsed = JSON.parse(out);
        _.each(parsed, add_action_info);
        callback(parsed);
    };
    git.spawn({ args: args, on_stdout_data: push_data }, finish);
};


exports.gitpurge = function (config, workspace, opts, callback) {
    //gitlog:   'shows ',
    if (!callback) { callback = opts; opts = {}; }

    //  reads in from .git
};


exports.gitsavescroll = function (config, workspace, destination, opts, callback) {
    // Saves a bare git repository as a '.scroll' file, with dir structure like:
    // scroll.git/     (bare repo)
    // .scrollid       (so it can be easily peaked at / identified)
    // manifest.cfg    (ditto)

    if (!callback) { callback = opts; opts = {}; }
    var git = _get_git(config, workspace, opts);
    opts = _.extend({
        log: function () {},
        tmpdir: null,
        nuked: false,
    }, opts);

    // todo: need to re-think tmpdir. It should be as follows:
    // /tmp/_scrollfiles/<file-hash + uuid>/
    // That way it can preserve state between instances
    // until then, tmpdir should be a disabled feature

    // 4 possibilities
    //         existing file   |  new file
    // ----------------------------------------
    // tmp    | -> push -> tar | -> clone -> tar
    // ----------------------------------------
    // no-tmp | ^--- untar     | ^--- mktmpdir
    // ----------------------------------------

    var do_prepare_directory = function () {
        fs.exists(destination, function (fexists) {
            if (opts.tmpdir) {
                //do_start_operation(fexists, opts.tmpdir);
                throw "NotImplementedYet: tmpdir";
            } else if (fexists) {
                fsutils.mkdirtmp(function (tmpdir) {
                    var extracted_dir = path.join(tmpdir, SCROLL_MAGIC);
                    fsutils.tar.extract(destination, tmpdir, function () {
                        do_start_operation(fexists, extracted_dir);
                    });
                });
            } else {
                fsutils.mkdirtmp({subdirname: SCROLL_MAGIC}, function (tmpdir) {
                    do_start_operation(fexists, tmpdir);
                });
            }
        });
    };

    var do_start_operation = function (fexists, tmpdir) {
        if (fexists) {
            do_save(true, tmpdir);
        } else {
            do_save(false, tmpdir);
        }
    };

    var do_save = function (is_push, tmpdir) {
        if (opts.nuked) {
            // "nuked" is just the working dir, no history / git stuff
            throw "Nuked file: not implemented yet";
        } else {
            var done = _.after(2, function () { do_tar(tmpdir); });
            if (is_push) {
                do_push(tmpdir, done);
            } else {
                do_clone(tmpdir, done);
            }
            fsutils.copyfilelist(SCROLLFILE_LIST, workspace.working_dir, tmpdir, done);
        }
    };

    var do_push = function (tmpdir, cb) {
        var target_dir = path.join(tmpdir, BARE_REPO_NAME);
        opts.log("Pushing to tmp dir: " + target_dir);
        git.run("push", target_dir, cb);
    };

    var do_clone = function (tmpdir, cb) {
        var target_dir = path.join(tmpdir, BARE_REPO_NAME);
        opts.log("Cloning to tmp bare repo: " + target_dir);
        var git = _get_git(config, null, opts);
        git.run('clone',  "--template=" + config.GIT_TEMPLATE_DIR, '--bare', workspace.working_dir, target_dir, function () {
            var git = _get_git(config, target_dir, opts); // get a new git within target dir

            var cmd = ['remote', 'remove', 'origin'] // remove bogus origin
            git.spawn({args: cmd, ignore_error: true}, cb);
        });
    };

    //var do_nuked = function (tmpdir, cb) {
    //    // xxx stub
    //    var opts = { filter: function (a) {
    //            return path.basename(a).charAt(0) !== '.'; }, };
    //    ncp(src, to, opts, function (err) { cb(); });
    //};

    var do_tar = function (tmpdir) {
        opts.log("Starting packing from " + tmpdir + " to " + destination);
        fsutils.tar.pack(tmpdir, destination, function () {
            opts.log("Finished packing!");
            callback();
        });
    };

    do_prepare_directory();
};

//gitpush:   'pushes a git-controlled scroll back to a source',
//gitcommit: 'auto-generates a commit for a git-controlled scroll (possibly branching)',
//gitundo:   'undos the last commit in a git-controlled scroll',
//gitredo:   'redos the last commit in a git-controlled scroll',
//gitpurge:  'deletes ALL history in a git-controlleds scroll',

