/*
The Document class encapsulates a single Document. A scroll workspace (e.g.
".scroll" file), may have more than one Document.
*/

var ScrollMarkdownParser = require('../markup/markdownparser').ScrollMarkdownParser;
var TreeParser = require('../../lib/markup/parser').TreeParser;
var _renderer = require('../../lib/markup/renderer'),
    EditorRenderer = _renderer.EditorRenderer,
    StyleRenderer = _renderer.StyleRenderer;
var _style = require('../../lib/markup/style'),
    StyleLoader = _style.StyleLoader,
    Style       = _style.Style;
var fs = require('fs');
var _ = require('underscore');

var Document = function (info, options) {
    this.opts = _.extend({
        prep_editor: true,
    }, options);
    this.info = info;
    if (info.path) {
        this.path = info.path;
    } else {
        this.chunk_paths = info.paths;
        // prep chunk names based on basename
    }
    this.parser = null;
    this.editor = {}; // houses editor parser and editor renderer
};

Document.prototype.prepare = function (tagloader, structure, callback) {
    /* ***************************************
     * Step 1, compile document parsers
     * *************************************** */
    // note: to add more types, e.g. XML, it would be here, based on some sort
    // of "document type" field

    // Compile parsers for the documents
    this.structure = structure;
    this.parser = new TreeParser(tagloader, this.structure);
    if (this.opts.prep_editor) {
        var emit_opts = { emit_source: true };
        this.editor.parser = new ScrollMarkdownParser(tagloader, emit_opts);
        this.editor.renderer = new EditorRenderer(tagloader);
    }

    var after = _.after(3, function () {
        callback();
    });

    this.parser.compile(after);
    this.editor.parser.compile(after);
    this.editor.renderer.compile(after);
};

Document.prototype.read = function (chunks, callback) {
    if (!callback) { callback = chunks; chunks = false; }

    if (chunks) {
        // for now, don't support chunks
        throw "chunks not sup yet";
    } 

    fs.readFile(this.path, function (err, data) {
        if (err) { throw errors.IOError(p + ": document read err " + err); }
        callback(data + '');
    });
};

Document.prototype.write = function (data, callback) {
    if (_.isObject(data)) {
        throw "chunks not sup yet";
        // todo: loop through, assuming its { "chunk_name": "chunk data" }
    }

    fs.writeFile(this.path, data, function (err, data) {
        if (err) { throw errors.IOError(p + ": document read err " + err); }
        callback(data + '');
    });
};

module.exports = Document;

