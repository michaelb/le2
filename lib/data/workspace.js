/*
The Workspace class encapsulates a single workspace, or "untarred .scroll
file". This is assumed to contain a manifest.cfg file.
*/

var glob = require('glob');
var path = require('path');
var fs   = require('fs');
var errors = require('../core/errors');
var schemaconf = require('schemaconf');
var fsutils = require('../util/fsutils');
var EditorRenderer = require('../markup/renderer').EditorRenderer;
var ScrollMarkdownParser = require('../markup/markdownparser').ScrollMarkdownParser;
var Document = require('./document');

var _tag = require('../markup/tag'),
    TagLoader = _tag.TagLoader;
var _style = require('../markup/style'),
    StyleLoader = _style.StyleLoader;
var _template = require('../data/template'),
    TemplateLoader = _template.TemplateLoader;

var _ = require('underscore');
_.str = require('underscore.string');

var schemas = require('./schemas');

var MANIFEST_FILE = "manifest.cfg";


/*
 * Workspace represents a loaded workspace, e.g. an open directory (file)
 */
var Workspace = function (working_dir, source_uri) {
    this.working_dir = path.resolve(working_dir);
    this.source_uri  = source_uri || null;
    this.tagloader   = null;
    this.manifest    = null;
    this.loaders     = {};
    this.documents   = {};
};

Workspace.prototype.reload = function (callback) {
    // essentially, make a duplicate of this workspace, but reloading everything
    var new_workspace = new Workspace(this.working_dir);
    new_workspace.prepare(function () {
        callback(new_workspace);
    });
};

Workspace.get_loaders = function (parsed_manifest, working_dir, callback) {
    // Fetches all the loaders for the given parsed manfiest (useful, for
    // example, for loading from templates)
    var loaders = {
        tag:      TagLoader,
        style:    StyleLoader,
        template: TemplateLoader,
        //export: StyleLoader,
    };

    var result = {};
    var done = _.after(_.size(loaders), function () { callback(result); });

    _.each(loaders, function (LoaderClass, loader_name) {
        var loader_name_plural = loader_name + "s";
        var list = parsed_manifest[loader_name_plural] || [];
        fsutils.loader_from_globlist(LoaderClass, working_dir, list,
            function (loader) {
                result[loader_name] = loader;
                done();
            });
    })
};

Workspace.prototype.prepare = function (callback) {
    /* ***************************************
     * Step 1, load manifest
     * *************************************** */
     var _this = this;
     var load_manifest = function () {
        var manifest_path = path.join(_this.working_dir, "manifest.cfg");
        _this.manifest_path = manifest_path;
        fs.exists(manifest_path, function (manifest_exists) {
            if (!manifest_exists) {
                throw new errors.FormatError("Invalid Scroll: Could not find manifest");
            }

            // it does exist, load it:
            schemaconf.fs.load({ path: manifest_path, schema: schemas.manifest },
                function (data) {
                    _this.manifest = data;
                    load_tags();
                });
        });
     };

    /* ***************************************
     * Step 2, setup loaders
     * *************************************** */
    var load_tags = function () {
        Workspace.get_loaders(_this.manifest, _this.working_dir, function (loaders) {
            _this.loaders = loaders;
            _this.tagloader = loaders.tag;
            prepare_documents()
        });
    };


    /* ***************************************
     * Step 3, compile document parsers
     * *************************************** */
    var prepare_documents = function () {
        // note: to add more types, e.g. XML, it would be here, based on some
        // sort of "document type" field
        var finish = _.after(_this.manifest.document.length, callback);
        for (var i in _this.manifest.document) {
            var document_info = _this.manifest.document[i];
            if (!document_info.path && !document_info.chunks) {
                throw new errors.FormatError("Invalid document: Needs either chunks or path");
            }

            if (_this.manifest.document.length > 1 && !document_info.name) {
                throw new errors.FormatError("Invalid document: Needs name if plural");
            }

            var doc_info = _.clone(document_info);
            doc_info.name = document_info.name || "document";
            if (doc_info.path) {
                doc_info.path = path.resolve(_this.working_dir, doc_info.path);
            } else {
                // chunks xxx worry about later
                throw "chunk not sup";
            }

            // Create new doc, and compile parsers for it
            var doc = new Document(doc_info);
            _this.documents[doc_info.name] = doc;
            _this.default_doc = doc;
            doc.prepare(_this.tagloader, _this.structure, finish);
        };
    };
    load_manifest();
};

Workspace.prototype.append_to_manifest = function (data, callback) {
    if (_.isObject(data)) {
        data = schemaconf.unparse(data);
    }

    fs.appendFile(this.manifest_path, data, function (err) {
        if (err) {
            throw new errors.IOError("Could not append to manifest:" + err);
        }
        callback();
    });
};


Workspace.prototype.get_all_paths = function (callback) {
    // loader.TAG.paths.NAMESPACE.TAGNAME = value;
    //        val pluck  val       val
    var loaded_list = _.flatten(_.pluck(_.values(this.loaders), 'paths_list'));
    loaded_list.push("manifest.cfg");
    loaded_list.push("document.md");
    loaded_list.push(".scrollid");
    callback(loaded_list);
};


Workspace.prototype.get_all_assets = function (callback) {
    // loader.TAG.paths.NAMESPACE.TAGNAME = value;
    //        val pluck  val       val
    var loaded_list = _.flatten(_.pluck(_.values(this.loaders), 'paths_list'));
    loaded_list.push("document.md");
    var asset_list = loaded_list.map(function (path) { return { path: path, }; });
    callback(asset_list);
};

module.exports = Workspace;

