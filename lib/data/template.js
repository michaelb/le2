var LoaderBase  = require('../util/loaderbase');
var _ = require('underscore');
_.str = require('underscore.string');

// Pull in style schema
var SCHEMA = require('./schemas').template;

/* ***************************************
 * Template represents a single style           */
var Template = function (namespace, name, validated_info) {
    this.namespace = namespace;
    this.name = name;
    this.info = validated_info;
    this.templates = null;
    this.meta = this.info.style;
};

Template.prototype.prepare = function (callback) {
    callback();
};

var TemplateLoader = function (template_groups) {
    // {"paths": ["style.cfg"], "namespace": "base"}
    // What to load
    LoaderBase.call(this, {
        groups: template_groups,
        Class: Template,
        schema: SCHEMA,
        plural_name: 'templates',
        compile_method: 'prepare',
    });
};

TemplateLoader.prototype = new LoaderBase;


module.exports.TemplateLoader = TemplateLoader;
module.exports.Template = Template;
