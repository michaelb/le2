# Assuming: any sort of npm is installed (can even be a very old version)
VERSION="5.1"

# Bootstrap newest version of node
npm install n > /dev/null 2> /dev/null
TARGET="`pwd`./node_modules/__n_node/"
mkdir -p $TARGET
N_PREFIX=$TARGET ./node_modules/n/bin/n $VERSION > /dev/null 2> /dev/null
npm remove n  > /dev/null 2> /dev/null # Clean up n
export PATH=$TARGET/bin:$PATH

# Check types
node --version
npm --version

# Install
npm -d install > /dev/null 2> /dev/null

# Run tests
npm test
