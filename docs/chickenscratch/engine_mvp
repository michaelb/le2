Engine MVP notes
================

Rational: Other MVP notes are for the editor, now that I've switched focus to
the engine, "libscroll" or something, I need a new set of MVP, before I can
even start working on implementing the Scroll editor

Criteria: These features need to be completed and have unit-tests using test
data (no need for real data)

key: ( ) is nice-to-have, [ ] is req

Core features
-------------
[x] tag loading + parsing
[x] markdown parsing
[x] basic rendering
[x] style rendering
[x] loader mechanism
[ ] multiple documents
( ) generalized loader inheritance
( ) custom doc loading / chunking


Export
------
[ ] Export profiles   (high level export "Ebook", support options)
[ ] Export processors (determine paths to get to desired format, can use
                       ebook-convert, libreoffice, etc, gradually replace
                       these with pure-JS versions)


Init
----
[x] init
[x] new from template


Utility
-----------
[ ] find (search through documents based on criteria)


Stash
-----
[x] stash list
[x] stash load
( ) stash save


Git-actions
-----------
[x] commit, undo, redo
[x] scroll file



CLI MVP notes
================


Rational: This stage will involve developing the CLI-only version of scroll.
This means developing the default set of templates, etc.

Criteria: Each feature needs to be complete and documented in a "Advanced Usage
(CLI)" web page, written as a scroll file in a separate repo. The web page
should have functional tests built into its documentation, programmatically
generated into/from bats tests.

key: ( ) is nice-to-have, [ ] is req

CLI
---
[x] new  (new from template)
[x] init (creates ./manifest.cfg and ./document.md, bare skeleton)
[x] stashlist (lists all items in stash)
[ ] stashload (loads a tag from stash)
( ) stashsave (save a tag to stash)
[ ] export (full export to particular target, e.g. "publish")
[ ] render (render to particular target (render HTML, no packaging))
[ ] config (save or check config properties (local or global))
[ ] find (find elements based on criteria)
[ ] gitpull (clones or updates a git-controlled scroll into your home)
[ ] gitpush (pushes a git-controlled scroll back to a source)
[ ] gitcommit (auto-generates a commit for a git-controlled scroll (possibly branching))
[ ] gitundo (undos the last commit in a git-controlled scroll)
[ ] gitredo (redos the last commit in a git-controlled scroll)
( ) gitpurge (deletes ALL history in a git-controlleds scroll)
[ ] help (list this brief description of commands)


Templates
---------
[ ] Unstructured
[ ] Fiction
[ ] Website


Styles
------
[ ] Bootstrap website example
[ ] Scroll website style (maintained in separate repo)


Specific tags
---------
Note: In addition to implied tags from Templates.
[ ] text_include (includes text from a given path during rendering, can do some
                  simple manipulations / filtering too)


