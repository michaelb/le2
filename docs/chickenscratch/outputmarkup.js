var markup_help = require('../lib/core/markup_help');
var fs = require('fs');
var _ = require('underscore');
_.str = require('underscore.string');


for (var tagname in markup_help) {
    var s = '';
    var info = markup_help[tagname];
    var tagname = tagname.slice(5);
    s += '[tag]\n';
    s += 'name = ' + _.str.humanize(tagname) + "\n";
    if (info.markup) {
        s += 'markdown = ' + info.markup + "\n";
    }
    if (info.keycode) {
        s += 'keycode = ' + info.keycode + "\n";
    }
    if (info.short_label) {
        s += 'short_label = ' + info.short_label + "\n";
    }
    if (info.help) {
        s += 'help : EOF\n' + info.help + "\nEOF\n";
    }

    console.log("----");
    console.log(s);
    fs.writeFileSync(tagname + '.cfg', s);
}




